<?php
//phpinfo();
include("s/config.php");
$page->do_debug=0;
$language->get_lang("global");
$user_detail = $session->get_user_details_session($session->get_session_id());
@$page->get_parameter ( $user_detail->challenge );

if ($page->get_operation("act","logout") == true ) {
    
    $template->basicheader($site_title);

    $page->do_logout(@$_SESSION[$session_id_name]);
    $page->redirect( 1 ,"/index/p/0/" ,$str_label["LBL_REDIRECTING"] );
    
    $template->basicfooter();

}
else if ($page->get_operation("act","login") == true ) {
    
    $template->basicheader($site_title);

	// Login Process
	foreach ($_POST as $k => $v) {
		$$k = $v;
	}

	$password = $session->get_decrypted_password($password);
	$page->debug("password:$password");

	$page->do_login($username, $password, $str_label);
    
    $template->basicfooter();

}
else if ($session->check_session_id() == true) {
	
	$page->check_access();
	
	$page->debug($user_detail);

	include "view/home_page.php";

}
else {

	// Login Page
    
	$key = $session->app_password_challenge();
	$iv = $session->app_password_challenge();

	$_SESSION['key'] = $key;
	$_SESSION['iv'] = $iv;
    
	include "view/login_page.php";
}

?>
