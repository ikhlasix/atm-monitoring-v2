<?php

// Include Classes and Basic Settings
include $_SERVER['DOCUMENT_ROOT']."/s/config.php";

// Get Label
$language->get_lang("global");







// ===================================================================================================================
// CRUD Setting Section: Start 
// ===================================================================================================================

// Include Process Class
include "m/menu.php";
$process = new menu( $db, $page, $form, $session, $log, $date, $str_label );

// Page Settings
$process->do_debug = true;
$config['script'] = "/modules/utilities/menu";
$config['page_title'] = "Menu Management";

// Table Settings
$config['table'] = "app_menu";
$config['primary_key'] = "men_id";
$config['primary_key2'] = "";
$config['primary_key3'] = "";
$config['primary_key_prefix'] = "MEN";

// Data Tables Settings
$config['data_tables_columns'] = array("men_id","menu_level","reference","title");
$config['data_tables_columns_label'] = array("MEN ID","Level","Reference","Title");
$config['data_tables_default_sort'] = "weight";
$config['data_tables_default_order'] = "asc";
$config['data_tables_default_sort_column'] = array_search ($config['data_tables_default_sort'], $config['data_tables_columns']); // No need to edit this

function form_settings( $values, $form, $page ) {
	foreach ($values as $k => $v) $$k = $v;
	
	// Disable inputs for View
	$input_disabled = "";
	if ($page->get_operation("act","view")) {
		$input_disabled = "disabled";
	}

// Form Settings
$settings = 
	array(
		array(
			"field_name" => "Level",
			"field_variable" => "menu_level",
			"field_value" => @$menu_level,
			"input_type" => "text", 				// text,password,email,date,multidate,numeric,currency
			"custom_input_type" => $form->select_list_array("menu_level",array("1"=>"1","2"=>"2","3"=>"3","4"=>"4"),@$menu_level," class=\"form-control\" $input_disabled style=\"width:100px;\" "),
			"validation" => "mandatory",
			"disabled" => $input_disabled,
		),
		array(
			"field_name" => "Reference",
			"field_variable" => "reference",
			"field_value" => @$reference,
			"input_type" => "text",
			"custom_input_type" => $form->select_list("reference","app_menu","men_id","title",@$reference," class=\"form-control\" $input_disabled ","","select men_id, men_id+' '+title title from app_menu"),
			"validation" => "",
			"disabled" => $input_disabled,
		),
		array(
			"field_name" => "Title",
			"field_variable" => "title",
			"field_value" => @$title,
			"input_type" => "text",
			"validation" => "mandatory",
			"disabled" => $input_disabled,
		),
		array(
			"field_name" => "URL",
			"field_variable" => "url",
			"field_value" => @$url,
			"input_type" => "text",
			"validation" => "",
			"disabled" => $input_disabled,
		),
		array(
			"field_name" => "Remark",
			"field_variable" => "remark",
			"field_value" => @$remark,
			"input_type" => "text",
			"validation" => "",
			"disabled" => $input_disabled,
		),
		array(
			"field_name" => "Target",
			"field_variable" => "target",
			"field_value" => @$target,
			"input_type" => "text",
			"validation" => "",
			"disabled" => $input_disabled,
		),
		array(
			"field_name" => "Image",
			"field_variable" => "image",
			"field_value" => @$image,
			"input_type" => "text",
			"validation" => "",
			"disabled" => $input_disabled,
		),
		array(
			"field_name" => "Weight",
			"field_variable" => "weight",
			"field_value" => @$weight,
			"input_type" => "text",
			"validation" => "mandatory",
			"disabled" => $input_disabled,
		),
		array(
			"field_name" => "Show",
			"field_variable" => "show",
			"field_value" => @$show,
			"input_type" => "text",
			"custom_input_type" => $form->select_list_array("show",array("1"=>"Yes","0"=>"No"),@$show," class=\"form-control\" $input_disabled style=\"width:100px;\" "),
			"validation" => "mandatory",
			"disabled" => $input_disabled,
		)	
		
		
	);
	return $settings;
}

// ===================================================================================================================
// CRUD Setting Section: End 
// ===================================================================================================================








// Check Access Privilege
if (!$page->get_function_access("read",$config['script'])) {
	$template->box($str_label["LBL_ILLEGAL_ACCESS"], $str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"], array(array("back","Back")), "warning");
	die();
}

// Logged in user Details
$user_detail = $session->get_user_details_session($session->get_session_id());
$config['user_detail'] = $user_detail;

// Get all encrypted parameters
$page->get_parameter ( $user_detail->challenge, $str_label );

// Debug Requests
if ($page->get_operation("act","add") || $page->get_operation("act","update")) $page->debug($_GET);
if ($page->get_operation("act","do_add") || $page->get_operation("act","do_update")) $page->debug($_POST);



// ===================================================
// Process Delete Data
// ===================================================
if ($page->get_operation("act","delete")) {
	
	// Check Access Privileges
	if (!$page->get_function_access("delete",$config['script'])) {
		$template->box($str_label["LBL_ILLEGAL_ACCESS"], $str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"], array(array("back","Back")), "warning");
		die();
	}
	
	// Delete Process
	$process->delete( $config );

	// Redirecting page
	$page->redirect(1 ,$config['script']."/p/0/" ,"Data Successfully Deleted");
	
	die();
	
}
// ===================================================
// Process Do Add/Do Update Data
// ===================================================
elseif ($page->get_operation("act","do_add") || $page->get_operation("act","do_update")) {

	if(!empty($_POST[$config['primary_key']])) $act="do_update";
	
	// Check Access Privileges
	if ( 	(!$page->get_function_access("add",$config['script']) && $page->get_operation("act","do_add")) || 
			(!$page->get_function_access("edit",$config['script']) && $page->get_operation("act","do_update"))
		) {
		$template->box($str_label["LBL_ILLEGAL_ACCESS"], $str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"], "", "warning");
		die();
	}
	
	// Prepare fields
	$values = array();
	$config["form_fields"] = form_settings( $values, $form, $page );
	
	//Form Validation
	$process->validate( $config );
	
	// Save Form
	$process->set_data( $config );	
	
	die();

}
// ===================================================
// Process Get List
// ===================================================
elseif ($page->get_operation("act","list")) {
	
	$process->get_list( $config );
	
	die();

}
// ===================================================
// Process Add/Update Form
// ===================================================
else if ($page->get_operation("act","add") || $page->get_operation("act","update")  || $page->get_operation("act","view")) {

	// Check Access Privilege
	if ( 	(!$page->get_function_access("add",$config['script']) && $page->get_operation("act","add")) || 
			(!$page->get_function_access("edit",$config['script']) && $page->get_operation("act","update"))
		) {
		$template->box($str_label["LBL_ILLEGAL_ACCESS"], $str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"], array(array("back","Back")), "warning");
		die();
	}
	    
	// Process Get Data
	$result = $process->get_data( $config );
	$values = array();
	foreach ($result as $key => $value) {
		$values[$key] = $value;
	}
	
	// Additional Process to give default values on add process
	if (isset($_GET['reference'])) $values['reference'] = $_GET['reference'];
	if (isset($_GET['menu_level'])) $values['menu_level'] = $_GET['menu_level'];
	if (isset($_GET['weight'])) $values['weight'] = $_GET['weight'];
	
	// Prepare Form Setting 
	$config["form_fields"] = form_settings( $values, $form, $page );
	
	// Generate Form
	$main_content = $process->generate_form ( $config );
	
}
// ===================================================
// Process List View
// ===================================================
else {
	
	$add_button = "";
	if ($page->get_function_access("add",$config['script'])) {
		$parameter_add = $page->set_parameter ( array("act"=>"add"), $config['user_detail']->challenge );
		$add_button = "<button type=\"button\" class=\"btn btn-primary btn-xs button-add\" onClick=\"window.location.href='".$config['script']."/p/".$parameter_add."/';\"><i class=\"glyphicon glyphicon-plus\"></i> ".$str_label["LBL_ADD"]."</button>";
	}

$main_content = <<<EOD
	<table id="tt" class="easyui-treegrid fixedtable"
        data-options="url:'{$config['script']}/p/0/?act=list',idField:'id',treeField:'title'" width="100%">
    <thead>
        <tr>
            <th field="title" width="50%">Menu</th>
            <th field="weight" width="10%">Weight</th>
			<th field="show" width="10%">Show</th>
			<th field="functions" width="30%">Functions</th>
        </tr>
    </thead>
	</table>
	<button class="btn btn-primary btn-xs" onClick="$('.easyui-treegrid').treegrid('expandAll');">Expand All</button> <button class="btn btn-primary btn-xs" onClick="$('.easyui-treegrid').treegrid('collapseAll');">Collapse All</button>	
EOD;
	
}

//Page Template
include $_SERVER["DOCUMENT_ROOT"]."/view/basic_page_easyui.php";
?>