<?php

// Include Classes and Basic Settings
include $_SERVER['DOCUMENT_ROOT']."/s/config.php";

// Get Label
$language->get_lang("global");



// ===================================================================================================================
// CRUD Setting Section: Start 
// ===================================================================================================================

// Include Process Class
include "m/function_access.php";
$process = new function_access( $db, $page, $form, $session, $log, $date, $str_label );

// Page Settings
$process->do_debug = true;
$config['script'] = "/modules/utilities/function_access";
$config['page_title'] = "Function Access Management";

// Table Settings
$config['table'] = "app_function_access";
$config['primary_key'] = "name";
$config['primary_key2'] = "";
$config['primary_key3'] = "";
$config['primary_key_prefix'] = "FUN";

// Data Tables Settings
$config['data_tables_columns'] = array("name");
$config['data_tables_columns_label'] = array("Name");
$config['data_tables_default_sort'] = "name";
$config['data_tables_default_order'] = "asc";
$config['data_tables_default_sort_column'] = array_search ($config['data_tables_default_sort'], $config['data_tables_columns']); // No need to edit this

function form_settings( $values, $form, $page, $process, $config ) {
	foreach ($values as $k => $v) $$k = $v;
	
	// Disable inputs for View
	$input_disabled = "";
	if ($page->get_operation("act","view")) {
		$input_disabled = "disabled";
	}

// Form Settings
$settings = 
	array(
		array(
			"field_name" => "Name",
			"field_variable" => "name",
			"field_value" => @$name,
			"input_type" => "text", 				// text/password/email/date/multidate/numeric/currency
			"validation" => "mandatory",			// mandatory,valid_email,valid_number,valid_date
			"disabled" => $input_disabled,
		),
		array(
			"field_name" => "Access",
			"field_variable" => "access",
			"field_value" => @$access,
			"input_type" => "text",
			"validation" => "",
			"disabled" => $input_disabled,
			"custom_input_type" => $process->generate_function_access_form(@$config, @$name)
		)	
	);
	return $settings;
}

// ===================================================================================================================
// CRUD Setting Section: End 
// ===================================================================================================================








// Check Access Privilege
if (!$page->get_function_access("read",$config['script'])) {
	$template->box($str_label["LBL_ILLEGAL_ACCESS"], $str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"], array(array("back","Back")), "warning");
	die();
}

// Logged in user Details
$user_detail = $session->get_user_details_session($session->get_session_id());
$config['user_detail'] = $user_detail;

// Get all encrypted parameters
$page->get_parameter ( $user_detail->challenge, $str_label );

// Debug Requests
if ($page->get_operation("act","add") || $page->get_operation("act","update")) $page->debug($_GET);
if ($page->get_operation("act","do_add") || $page->get_operation("act","do_update")) $page->debug($_POST);



// ===================================================
// Process Delete Data
// ===================================================
if ($page->get_operation("act","delete")) {
	
	// Check Access Privileges
	if (!$page->get_function_access("delete",$config['script'])) {
		$template->box($str_label["LBL_ILLEGAL_ACCESS"], $str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"], array(array("back","Back")), "warning");
		die();
	}
	
	// Delete Process
	$process->delete( $config );

	// Redirecting page
	$page->redirect(1 ,$config['script']."/p/0/" ,"Data Successfully Deleted");
	
	die();
	
}
// ===================================================
// Process Do Add/Do Update Data
// ===================================================
elseif ($page->get_operation("act","do_add") || $page->get_operation("act","do_update")) {

	if(!empty($_POST[$config['primary_key']])) $act="do_update";
	
	// Check Access Privileges
	if ( 	(!$page->get_function_access("add",$config['script']) && $page->get_operation("act","do_add")) || 
			(!$page->get_function_access("edit",$config['script']) && $page->get_operation("act","do_update"))
		) {
		$template->box($str_label["LBL_ILLEGAL_ACCESS"], $str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"], "", "warning");
		die();
	}
	
	// Prepare fields
	$values = array();
	$config["form_fields"] = form_settings( $values, $form, $page, $process, $config );
	
	//Form Validation
	$process->validate( $config );
	
	// Save Form
	$process->set_data( $config );	
	
	die();

}
// ===================================================
// Process Do Duplicate Data
// ===================================================
elseif ($page->get_operation("act","do_duplicate")) {

	// Check Access Privileges
	if (!$page->get_function_access("add",$config['script'])) {
		$template->box($str_label["LBL_ILLEGAL_ACCESS"], $str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"], "", "warning");
		die();
	}
	
	// Save Form
	$process->set_duplicate( $config );	
	
	die();

}
// ===================================================
// Process Get List
// ===================================================
elseif ($page->get_operation("act","list")) {
	
	// Check Access Privileges
	$edit_privilege = false;
	$delete_privilege = false;
	if ($page->get_function_access("edit",$config['script'])) $config['edit_privilege'] = true;
	if ($page->get_function_access("delete",$config['script'])) $config['delete_privilege'] = true;  
	
	// Process Get List
	$process->get_list( $config );
	
	die();

}
// ===================================================
// Process Get TreeList
// ===================================================
elseif ($page->get_operation("act","treelist")) {
	
	$process->get_treelist ( $config );
	
	die();

}
// ===================================================
// Process Add/Update Form
// ===================================================
else if ($page->get_operation("act","add") || $page->get_operation("act","update")  || $page->get_operation("act","view")) {

	// Check Access Privilege
	if ( 	(!$page->get_function_access("add",$config['script']) && $page->get_operation("act","add")) || 
			(!$page->get_function_access("edit",$config['script']) && $page->get_operation("act","update"))
		) {
		$template->box($str_label["LBL_ILLEGAL_ACCESS"], $str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"], array(array("back","Back")), "warning");
		die();
	}
	    
	// Process Get Data
	$result = $process->get_data( $config );
	$values = array();
	foreach ($result as $key => $value) {
		$values[$key] = $value;
	}
	
	// Prepare Form Setting 
	$config["form_fields"] = form_settings( $values, $form, $page, $process, $config );
	
	// Generate Form
	$main_content = $process->generate_form ( $config );
	
}
// ===================================================
// Process Duplicate Form
// ===================================================
else if ($page->get_operation("act","duplicate")) {
	
	// Check Access Privilege
	if ( !$page->get_function_access("add",$config['script'])
		) {
		$template->box($str_label["LBL_ILLEGAL_ACCESS"], $str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"], array(array("back","Back")), "warning");
		die();
	}
	    
	$submit_button = "<button type=\"button\" class=\"btn btn-primary button-submit\" onClick=\"submitForm();return false\" style=\"width:100px\">".$str_label["LBL_DUPLICATE"]."</button>";
	
	$output = "
            
	<div class=\"form-group\">
		<label class=\"control-label col-sm-2\" for=\"name\">Name *</label>
		<div class=\"col-lg-6 col-md-8 col-sm-10\">
		".$form->text_input ( "name", "text", "", "" )."                
		</div>
	</div>
	
	";
	
$main_content = <<<EOD
            <form id="f1" enctype="multipart/form-data" class="form-horizontal" role="form">	
            <input type="hidden" name="act" value="do_duplicate">
            <input type="hidden" name="duplicate_from" value="{$_GET['name']}">
            
            {$output}
            
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                <button type="button" class="btn btn-primary button-back" onClick="window.location.href='{$config['script']}/p/0/'; return false" style="width:100px"><i class="fa fa-arrow-left" style="margin-right:10px"></i> {$str_label["LBL_BACK"]}</button>
                {$submit_button}
                </div>
            </div>
            </form>
            {$str_label["MSG_FIELDS_MANDATORY"]}
EOD;
	
	
}
// ===================================================
// Process List View
// ===================================================
else {

	$main_content = $process->generate_list ( $config );

}

//Page Template
include $_SERVER["DOCUMENT_ROOT"]."/view/easyui_utilities_function_access.php";
?> 