<?php

// Include Classes and Basic Settings
include $_SERVER['DOCUMENT_ROOT']."/s/config.php";

// Get Label
$language->get_lang("global");




// ===================================================================================================================
// CRUD Setting Section: Start 
// ===================================================================================================================

// Include Process Class
include "m/user.php";
$process = new user( $db, $page, $form, $session, $log, $date, $str_label );

// Page Settings
$process->do_debug = true;
$config['script'] = "/modules/utilities/user";
$config['page_title'] = "User Management";

// Table Settings
$config['table'] = "app_user";
$config['primary_key'] = "usr_id";
$config['primary_key2'] = "";
$config['primary_key3'] = "";
$config['primary_key_prefix'] = "USR";

// Data Tables Settings
$config['data_tables_columns'] = array("usr_id","username","function_access","inquiry_access");
$config['data_tables_columns_label'] = array("USR ID","Username","Function Access","Inquiry Access");
$config['data_tables_default_sort'] = "usr_id";
$config['data_tables_default_order'] = "asc";
$config['data_tables_default_sort_column'] = array_search ($config['data_tables_default_sort'], $config['data_tables_columns']); // No need to edit this

function form_settings( $values, $form, $page ) {
	foreach ($values as $k => $v) $$k = $v;
	
	// Additional Notification
	$password_notification = "";
	$password_mandatory = "mandatory";
	if ($page->get_operation("act","update") || $page->get_operation("act","do_update")) {
		$password_notification = "<i>Fill in password only if you want to change it</i>";
		$password_mandatory = "";
	}
	
	// Disable inputs for View
	$input_disabled = "";
	if ($page->get_operation("act","view")) {
		$input_disabled = "disabled";
	}

// Form Settings
$settings = 
	array(
		array(
			"field_name" => "First Name",
			"field_variable" => "first_name",
			"field_value" => @$first_name,
			"input_type" => "text", 				// text/password/email/date/multidate/numeric/currency
			"validation" => "mandatory",			// mandatory,valid_email,valid_number,valid_date
			"disabled" => $input_disabled,
		),
		array(
			"field_name" => "Last Name",
			"field_variable" => "last_name",
			"field_value" => @$last_name,
			"input_type" => "text",
			"validation" => "mandatory",
			"disabled" => $input_disabled,
		),
		array(
			"field_name" => "Username",
			"field_variable" => "username",
			"field_value" => @$username,
			"input_type" => "text",
			"validation" => "mandatory",
			"disabled" => $input_disabled,
		),
		array(
			"field_name" => "Password",
			"field_variable" => "password",
			"field_value" => @$password,
			"input_type" => "password",
			"validation" => "$password_mandatory",
			"notification" => $password_notification,
			"disabled" => $input_disabled,
		),
		array(
			"field_name" => "Password Verification",
			"field_variable" => "verify",
			"field_value" => @$verify,
			"input_type" => "password",
			"validation" => "$password_mandatory",
			"disabled" => $input_disabled,
		),
		array(
			"field_name" => "Email",
			"field_variable" => "email",
			"field_value" => @$email,
			"input_type" => "text",
			"validation" => "mandatory,valid_email",
			"disabled" => $input_disabled,
		),
		array(
			"field_name" => "Function Access",
			"field_variable" => "function_access",
			"field_value" => @$function_access,
			"input_type" => "text",
			"validation" => "mandatory",
			"disabled" => $input_disabled,
			"custom_input_type" => $form->select_list("function_access","app_function_access","name","name",@$function_access," class=\"form-control\" $input_disabled ","","select name from app_function_access group by name") 
		),
		array(
			"field_name" => "Inquiry Access",
			"field_variable" => "inquiry_access",
			"field_value" => @$inquiry_access,
			"input_type" => "text",
			"validation" => "mandatory",
			"disabled" => $input_disabled,
			"custom_input_type" => $form->select_list("inquiry_access","app_inquiry_access","access_name","access_name",@$inquiry_access," class=\"form-control\" $input_disabled ","select access_name from app_inquiry_access group by access_name") 
		)	
		
		
	);
	return $settings;
}

// ===================================================================================================================
// CRUD Setting Section: End 
// ===================================================================================================================








// Check Access Privilege
if (!$page->get_function_access("read",$config['script'])) {
	$template->box($str_label["LBL_ILLEGAL_ACCESS"], $str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"], array(array("back","Back")), "warning");
	die();
}

// Logged in user Details
$user_detail = $session->get_user_details_session($session->get_session_id());
$config['user_detail'] = $user_detail;

// Get all encrypted parameters
$page->get_parameter ( $user_detail->challenge, $str_label );

// Debug Requests
if ($page->get_operation("act","add") || $page->get_operation("act","update")) $page->debug($_GET);
if ($page->get_operation("act","do_add") || $page->get_operation("act","do_update")) $page->debug($_POST);



// ===================================================
// Process Delete Data
// ===================================================
if ($page->get_operation("act","delete")) {
	
	// Check Access Privileges
	if (!$page->get_function_access("delete",$config['script'])) {
		$template->box($str_label["LBL_ILLEGAL_ACCESS"], $str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"], array(array("back","Back")), "warning");
		die();
	}
	
	// Delete Process
	$process->delete( $config );

	// Redirecting page
	$page->redirect(1 ,$config['script']."/p/0/" ,"Data Successfully Deleted");
	
	die();
	
}
// ===================================================
// Process Do Add/Do Update Data
// ===================================================
elseif ($page->get_operation("act","do_add") || $page->get_operation("act","do_update")) {

	if(!empty($_POST[$config['primary_key']])) $act="do_update";
	
	// Check Access Privileges
	if ( 	(!$page->get_function_access("add",$config['script']) && $page->get_operation("act","do_add")) || 
			(!$page->get_function_access("edit",$config['script']) && $page->get_operation("act","do_update"))
		) {
		$template->box($str_label["LBL_ILLEGAL_ACCESS"], $str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"], "", "warning");
		die();
	}
	
	// Prepare fields
	$values = array();
	$config["form_fields"] = form_settings( $values, $form, $page );
	
	//Form Validation
	$process->validate( $config );
	
	// Save Form
	$process->set_data( $config );	
	
	die();

}
// ===================================================
// Process Get List
// ===================================================
elseif ($page->get_operation("act","list")) {
	
	// Check Access Privileges
	$edit_privilege = false;
	$delete_privilege = false;
	if ($page->get_function_access("edit",$config['script'])) $config['edit_privilege'] = true;
	if ($page->get_function_access("delete",$config['script'])) $config['delete_privilege'] = true;  
	
	// Process Get List
	$process->get_list( $config );
	
	die();

}
// ===================================================
// Process Add/Update Form
// ===================================================
else if ($page->get_operation("act","add") || $page->get_operation("act","update")  || $page->get_operation("act","view")) {

	// Check Access Privilege
	if ( 	(!$page->get_function_access("add",$config['script']) && $page->get_operation("act","add")) || 
			(!$page->get_function_access("edit",$config['script']) && $page->get_operation("act","update"))
		) {
		$template->box($str_label["LBL_ILLEGAL_ACCESS"], $str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"], array(array("back","Back")), "warning");
		die();
	}
	    
	// Process Get Data
	$result = $process->get_data( $config );
	$values = array();
	foreach ($result as $key => $value) {
		$values[$key] = $value;
	}
	
	// Prepare Form Setting 
	$config["form_fields"] = form_settings( $values, $form, $page );
	
	// Generate Form
	$main_content = $process->generate_form ( $config );
	
}
// ===================================================
// Process List View
// ===================================================
else {

	$main_content = $process->generate_list ( $config );

}

//Page Template
include $_SERVER["DOCUMENT_ROOT"]."/view/basic_page.php";
?> 