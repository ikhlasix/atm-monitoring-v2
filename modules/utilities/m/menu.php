<?php

class menu {
    
    private $page;
    private $form;
    private $session;
    private $log;
    private $str_label;
    private $date;
    private $db;
    
    public function __construct( $db, classes\application\page $page, classes\application\form $form, classes\application\session $session, classes\application\log $log, classes\application\date $date, $str_label ) {
        $this->page = $page;
        $this->form = $form;
        $this->session = $session;
        $this->log = $log;
        $this->str_label = $str_label;
        $this->date = $date;
        $this->db = $db;
        
        // Set Debug
        $this->page->do_debug = 0;
    }
    
    public function get_list( $config ) {
        
        // Check Access Privileges
        $edit_privilege = false;
        $delete_privilege = false;
        if ($this->page->get_function_access("edit",$config['script'])) $config['edit_privilege'] = true;
        if ($this->page->get_function_access("delete",$config['script'])) $config['delete_privilege'] = true;
        
        $result = array();
        
        if (!isset($_POST['id'])) $cond = "(reference is null or reference='')";
        else $cond = "reference='".$_POST['id']."'";
            
        $sql="select * from ".$config['table']." where $cond and isnull(is_delete,0)!='1' order by ".$config['data_tables_default_sort']." ".$config['data_tables_default_order']."";
        $this->page->debug($sql);
        $res_menu=$this->db->Execute($sql);
        if (!$res_menu) $this->page->debug_query(1);
        while($row_menu=$res_menu->FetchRow()) {
            if (is_array($row_menu)){ foreach($row_menu as $key=>$val) {
                $key=strtolower($key);
                $$key=$val;
            }}
        
            // Check child
            $child = false;
            $sql="select count(*) as num from ".$config['table']." where reference='".$men_id."' and isnull(is_delete,0)!='1'";
            $this->page->debug($sql);
            $num=$this->db->GetOne($sql);
            $child =  ($num > 0) ? "closed" : "open";	   
            
            // Generate defailt value for child addition
            $new_menu_level = $menu_level + 1;
            $new_reference = $men_id;
            $sql="select max(weight) last_weight from ".$config['table']." where reference='".$men_id."' and isnull(is_delete,0)!='1'";
            $this->page->debug($sql);
            $last_weight=$this->db->GetOne($sql);
            if (!isset($last_weight)) $last_weight = 0;
            $new_weight = $last_weight + 10;
            
            $parameter_add = $this->page->set_parameter ( array("reference"=>"$new_reference","menu_level"=>"$new_menu_level","weight"=>"$new_weight","act"=>"add"), $config['user_detail']->challenge );
            $parameter_update = $this->page->set_parameter ( array("men_id"=>"$men_id","act"=>"update"), $config['user_detail']->challenge );
            $parameter_view = $this->page->set_parameter ( array("men_id"=>"$men_id","act"=>"view"), $config['user_detail']->challenge );
            $parameter_delete = $this->page->set_parameter ( array("men_id"=>"$men_id","act"=>"delete"), $config['user_detail']->challenge );
            
            $links = "";
            // Add Link
            if ($this->page->get_function_access("add",$config['script'])) $links .=  "<button type=\"button\" class=\"btn btn-primary btn-xs button-edit\" onClick=\"window.location.href='".$config['script']."/p/".$parameter_add."/';\"><i class=\"glyphicon glyphicon-plus\"></i> ".$this->str_label["LBL_ADD"]."</button> ";
            
            // Edit or View link
            if ($this->page->get_function_access("edit",$config['script'])) $links .=  "<button type=\"button\" class=\"btn btn-primary btn-xs button-edit\" onClick=\"window.location.href='".$config['script']."/p/".$parameter_update."/';\"><i class=\"glyphicon glyphicon-edit\"></i> ".$this->str_label["LBL_EDIT"]."</button>  ";
            else $links .=  "<button type=\"button\" class=\"btn btn-primary btn-xs button-view\" onClick=\"window.location.href='".$config['script']."/p/".$parameter_view."/';\"><i class=\"glyphicon glyphicon-edit\"></i> ".$this->str_label["LBL_VIEW"]."</button> ";
            
            // Delete link            
            if ($this->page->get_function_access("delete",$config['script'])) $links .=  "<button type=\"button\" class=\"btn btn-primary btn-xs button-delete\" onClick=\"if (confirm('".$this->str_label["MSG_ARE_YOU_SURE_TO_DELETE_THIS_DATA"]."')) window.location.href='".$config['script']."/p/".$parameter_delete."/';\"><i class=\"glyphicon glyphicon-remove\"></i> ".$this->str_label["LBL_DELETE"]."</button> ";
            
            $node = array();
            $node['id'] = $men_id;
            $node['title'] = $title;
            $node['state'] = $child;
            $node['weight'] = $weight;
            $node['show'] = ($show == 1) ? "Yes" : "No";
            $node['functions'] = "
                $links
                ";
            array_push($result,$node);
        
        }
            
        echo json_encode($result);     
        
    }     
    
    
    
    public function get_data ( $config ) {
        
        $result = array();
        
        if(!empty($_GET[$config['primary_key']])){
		
            $cond = "";
            if (isset($_GET[$config['primary_key']])) $cond = $config['primary_key']."='".$_GET[$config['primary_key']]."' and ";
            if (isset($_GET[$config['primary_key2']])) $cond = $config['primary_key2']."='".$_GET[$config['primary_key2']]."' and ";
            if (isset($_GET[$config['primary_key3']])) $cond = $config['primary_key3']."='".$_GET[$config['primary_key3']]."' and ";
            $cond = substr($cond,0,-5);
            
            $sql="select * from ".$config['table']." where $cond ";
            $this->page->debug($sql);
            $res_data=$this->db->Execute($sql);
            if(!$res_data) $this->page->debug_query(1);
            $row=$res_data->FetchRow();
            foreach($row as $key=>$val){
                $key=strtolower($key);
                $result[$key]=$val;
            }
            
            // Reset password and verify variable
            $form_variable_arr = array("verify","password");
            foreach ($form_variable_arr as $key => $value) {
                $result[$value] = "";
            }
        }
        
        return $result;
        
    }
    
    
    public function delete( $config ) {
        
        $cond = "";
        if (isset($_GET[$config['primary_key']])) $cond = $config['primary_key']."='".$_GET[$config['primary_key']]."' and ";
        if (isset($_GET[$config['primary_key2']])) $cond = $config['primary_key2']."='".$_GET[$config['primary_key2']]."' and ";
        if (isset($_GET[$config['primary_key3']])) $cond = $config['primary_key3']."='".$_GET[$config['primary_key3']]."' and ";
        $cond = substr($cond,0,-5);
        
        $list = "";
        $list .= "is_delete='1',";
        $list .= "d_time=".$this->date->datetime_to_db(date("Y-m-d H:i:s")).",";
        $list .= "d_user='".$config['user_detail']->username."',";
        $list = preg_replace("/,$/","",$list);
        
        $sql="update ".$config['table']." set $list where $cond";
        $this->page->debug($sql);
        $res_del=$this->db->Execute($sql);
        if (!$res_del) $this->page->debug_query(1);
        
        $this->log->insert_log("DELETE ".$config['table']." ".$config['primary_key'].": ".$_GET[$config['primary_key']]);
        
    }
    
    
    public function validate( $config ) {
        
        $error = "";
        $error = $this->form->validate( $config );
        
        // Return Error
        if($error){
            echo '{ "status": "error", "message": "<b>Invalid Input</b><hr><ul>'.$error.'</ul>" }';
            die();
        }
        
    }
    
    
    public function set_data( $config ) {
        
        $columns = "";
        $values = "";
        $list = "";
        $cond = "";
        
        $act = $_POST['act'];

        foreach($_POST as $key=>$val){

            if($act=='do_add' && !preg_match("/^(act)$/i",$key)){
                if ($key==$config['primary_key']) {
                    $this->page->debug($key."=".$val);
                    $val = $this->form->generate_row_id($config['table'],$config['primary_key'],$config['primary_key_prefix']);
                    $_POST[$config['primary_key']] = $val;
                    $columns .="$key,";
                    $values .="'$val',";
                }
                else if ($key==$config['primary_key2']) {
                    $columns .="$key,";
                    $values .="'$val',";
                }
                else if ($key==$config['primary_key3']) {
                    $columns .="$key,";
                    $values .="'$val',";
                }
                else {
                    $columns .="$key,";
                    $values .="'$val',";
                }
            }elseif($act=='do_update' && !preg_match("/^(act)$/i",$key)){
                if($key==$config['primary_key']){
                    $cond="$key='$val' and ";
                }
                else if($key==$config['primary_key2']){
                    $cond="$key='$val' and ";
                }
                else if($key==$config['primary_key3']){
                    $cond="$key='$val' and ";
                }
                else{
                    $list .="$key='$val',";
                }
            }
            
            $$key = $val;
        }

        // Additional Columns
        if ($act == 'do_add') {
            
            $columns .= "c_time,";
            $values .= $this->date->datetime_to_db(date("Y-m-d H:i:s")).",";
            
            $columns .= "c_user,";
            $values .= "'".$config['user_detail']->username."',"; 
        }
        else if ($act == 'do_update') {
            
            $list .= "m_time=".$this->date->datetime_to_db(date("Y-m-d H:i:s")).",";
            $list .= "m_user='".$config['user_detail']->username."',";
        }
        

        $columns = preg_replace("/,$/","",$columns);
        $values	 = preg_replace("/,$/","",$values);
        $list	 = preg_replace("/,$/","",$list);
        $cond 	 = substr($cond,0,-5);
        
        if($act=="do_update"){
            $sql="update ".$config['table']." set $list where $cond";
            $this->page->debug($sql);
            $result=$this->db->Execute($sql);
            if (!$result){
                $this->page->debug_query(1);
            }
            $this->log->insert_log("UPDATE ".$config['table']." ".$config['primary_key'].": ".$_POST[$config['primary_key']]);
            
        }else{

            $sql="insert into ".$config['table']." ($columns) values ($values)";
            $this->page->debug($sql);
            $result=$this->db->Execute($sql);
            if (!$result){
                $this->page->debug_query(1);
            }	
            $this->log->insert_log("INSERT ".$config['table']." ".$config['primary_key'].": ".$_POST[$config['primary_key']]);
        }
        
        echo '{ "status": "success", "message": "Data has been successfully saved" }';
        
    }
    
    
    public function generate_form ( $config ) {
        
        $output = "";
        foreach ($config["form_fields"] as $key => $form_group) {
            
            $input_disabled = $form_group["disabled"];
            
            $mandatory_sign = "";
            if (preg_match("/mandatory/i",$form_group["validation"])) {
                $mandatory_sign = "*";
            }
            
            $input = "";
            if (isset($form_group["custom_input_type"])) {
                $input = $form_group["custom_input_type"];
            }
            else {
                // Text, Password, Email
                if (preg_match("/^(text|password|email)$/i",$form_group["input_type"])) {
                    $input = $this->form->text_input ( $form_group["field_variable"], $form_group["input_type"], $form_group["field_value"], $input_disabled );
                }
                // Date
                else if (preg_match("/^(date)$/i",$form_group["input_type"])) {
                    $input = $this->form->date_input ( $form_group["field_variable"], $form_group["field_value"], $input_disabled );
                }
                // Multi Date
                else if (preg_match("/^(multidate)$/i",$form_group["input_type"])) {
                    $input = $this->form->multidate_input ( $form_group["field_variable"], $form_group["field_value"], $input_disabled );
                }
                // Numeric
                else if (preg_match("/^(numeric)$/i",$form_group["input_type"])) {
                    $input = $this->form->numeric_input ( $form_group["field_variable"], $form_group["field_value"], $input_disabled );
                }
                // Currency
                else if (preg_match("/^(currency)$/i",$form_group["input_type"])) {
                    $input = $this->form->currency_input ( $form_group["field_variable"], $form_group["field_value"], $input_disabled );
                }
            }
            
            if (isset($form_group["notification"])) {
                $input .= $form_group["notification"];
            }
            
            $output .= "
            
            <div class=\"form-group\">
                <label class=\"control-label col-sm-2\" for=\"".$form_group["field_variable"]."\">".$form_group["field_name"]." ".$mandatory_sign."</label>
                <div class=\"col-lg-6 col-md-8 col-sm-10\">
                ".$input."                
                </div>
            </div>
            
            ";
            
            // Button Label
            $button_label = (($this->page->get_operation("act","add"))?$this->str_label["LBL_ADD"]:$this->str_label["LBL_EDIT"]);
            
            $submit_button = "";
            if ($this->page->get_operation("act","add") || $this->page->get_operation("act","update"))
                $submit_button = "<input type=\"submit\" class=\"btn btn-primary button-submit\" value=\"".$button_label."\" style=\"width:100px;\" />";
            
            // Set variable inside main content
            $hidden_pk1_var = "";
            $hidden_pk2_var = "";
            $hidden_pk3_var = "";
            $notification_password = "";
            if ($config['primary_key']) $hidden_pk1_var = "<input type=\"hidden\" name=\"".$config['primary_key']."\" value=\"".@$_GET[$config['primary_key']]."\">";
            if ($config['primary_key2']) $hidden_pk2_var = "<input type=\"hidden\" name=\"".$config['primary_key2']."\" value=\"".@$_GET[$config['primary_key2']]."\">";
            if ($config['primary_key3']) $hidden_pk3_var = "<input type=\"hidden\" name=\"".$config['primary_key3']."\" value=\"".@$_GET[$config['primary_key3']]."\">"; 
            
            $_act=(!empty($_GET[$config['primary_key']]))?"do_update":"do_add";
            

$main_content = <<<EOD
            <form id="f1"  class="form-horizontal" role="form" method="post" action="{$config['script']}/p/0/" enctype="multipart/form-data">	
            <input type="hidden" name="act" value="$_act">
            {$hidden_pk1_var}
            {$hidden_pk2_var}
            {$hidden_pk3_var}
            
            {$output}
            
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                <button type="button" class="btn btn-primary button-back" onClick="window.location.href='{$config['script']}/p/0/'; return false" style="width:100px"><i class="fa fa-arrow-left" style="margin-right:10px"></i> {$this->str_label["LBL_BACK"]}</button>
                {$submit_button}
                </div>
            </div>
            </form>
            {$this->str_label["MSG_FIELDS_MANDATORY"]}
EOD;
            
        }
        
        return $main_content;
        
    }
    
    
    
    public function generate_list( $config ) {
        
        // Check Button Privileges
        $add_button = "";
        if ($this->page->get_function_access("add",$config['script'])) {
            $parameter_add = $this->page->set_parameter ( array("act"=>"add"), $config['user_detail']->challenge );
            $add_button = "<button type=\"button\" class=\"btn btn-primary button-add\" style=\"margin-bottom: 10px\" onClick=\"window.location.href='".$config['script']."/p/".$parameter_add."/';\"><i class=\"glyphicon glyphicon-plus\"></i> ".$this->str_label["LBL_ADD"]."</button>";
        }           

        $column_label = "";
        foreach ($config['data_tables_columns_label'] as $key => $value) {
            $column_label .= "<th>".$value."</th>"; 
        }

$main_content = <<<EOD
	{$add_button}
	<table class="table table-striped table-hover" id="dataTables">
		<thead>
			<tr>
				{$column_label}
				<th>Functions</th>
			</tr>
		</thead>
	</table>
EOD;

    return $main_content;
        
    }
   
}

?>