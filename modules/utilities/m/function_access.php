<?php

class function_access {
    
    private $page;
    private $form;
    private $session;
    private $log;
    private $str_label;
    private $date;
    private $db;
    
    public function __construct( $db, classes\application\page $page, classes\application\form $form, classes\application\session $session, classes\application\log $log, classes\application\date $date, $str_label ) {
        $this->page = $page;
        $this->form = $form;
        $this->session = $session;
        $this->log = $log;
        $this->str_label = $str_label;
        $this->date = $date;
        $this->db = $db;
        
        // Set Debug
        $this->page->do_debug = 0;
    }
    
    public function get_list( $config ) {
        
        // Sorting Setting
        $_order = $config['data_tables_columns'][$_GET['order'][0]['column']];
        $_sort = $_GET['order'][0]['dir'];
        $result = array();
        
        // Condition Setting
        $cond = " and isnull(is_delete,0)!='1' ";
        
        // Search Setting
        $_query = $_GET['search']['value'];
        $cond_search = "";
        if ($_query) {
            foreach ($config['data_tables_columns'] as $key => $value) {
                $cond_search .= " $value like '%$_query%' or ";
            }
            $cond_search = " and (".substr($cond_search,0,-4).") ";
        }
        
        //Get Total Records
        $sql="select count(name) from ".$config['table']." where 1=1 $cond group by name";
        $this->page->debug($sql);
        $records_total=$this->db->GetOne($sql);

        //Get Data
        $sql="select name from ".$config['table']." where 1=1 $cond $cond_search group by name order by $_order $_sort";
        $this->page->debug($sql);
        $res_select=$this->db->Execute($sql);
        $records_filtered = $res_select->RecordCount();
        
        $res_select=$this->db->SelectLimit($sql,$_GET['length'],$_GET['start']);
        if (!$res_select) $this->page->debug_query(1);
        while($row_select=$res_select->FetchRow()) {
            if (is_array($row_select)){ foreach($row_select as $key=>$val) {
                $key=strtolower($key);
                $$key=$val;
            }}
            
            $parameter_array = array();
            if ($config['primary_key']) $parameter_array[$config['primary_key']] = $$config['primary_key'];
            if ($config['primary_key2']) $parameter_array[$config['primary_key2']] = $$config['primary_key2'];
            if ($config['primary_key3']) $parameter_array[$config['primary_key3']] = $$config['primary_key3'];
            
            $parameter_update = $this->page->set_parameter ( array_merge($parameter_array,array("act"=>"update")), $config['user_detail']->challenge );
            $parameter_view = $this->page->set_parameter ( array_merge($parameter_array,array("act"=>"view")), $config['user_detail']->challenge );
            $parameter_delete = $this->page->set_parameter ( array_merge($parameter_array,array("act"=>"delete")), $config['user_detail']->challenge );
            $parameter_duplicate = $this->page->set_parameter ( array_merge($parameter_array,array("act"=>"duplicate")), $config['user_detail']->challenge );
            
            $column_data = array();
            foreach ($config['data_tables_columns'] as $key => $value) {
                $column_data[] = $$value;
            }
            
            $links = "";
            // Edit or View link
            if ($this->page->get_function_access("edit",$config['script'])) $links .=  "<button type=\"button\" class=\"btn btn-primary btn-xs button-edit\" onClick=\"window.location.href='".$config['script']."/p/".$parameter_update."/';\"><i class=\"glyphicon glyphicon-edit\"></i> ".$this->str_label["LBL_EDIT"]."</button> ";
            else $links .=  "<button type=\"button\" class=\"btn btn-primary btn-xs button-view\" onClick=\"window.location.href='".$config['script']."/p/".$parameter_view."/';\"><i class=\"glyphicon glyphicon-edit\"></i> ".$this->str_label["LBL_VIEW"]."</button> ";
            
            // Delete link            
            if ($this->page->get_function_access("delete",$config['script'])) $links .=  "<button type=\"button\" class=\"btn btn-primary btn-xs button-delete\" onClick=\"if (confirm('".$this->str_label["MSG_ARE_YOU_SURE_TO_DELETE_THIS_DATA"]."')) window.location.href='".$config['script']."/p/".$parameter_delete."/';\"><i class=\"glyphicon glyphicon-remove\"></i> ".$this->str_label["LBL_DELETE"]."</button> ";
            
            // Duplicate link            
            if ($this->page->get_function_access("add",$config['script'])) $links .=  "<button type=\"button\" class=\"btn btn-primary btn-xs button-delete\" onClick=\"window.location.href='".$config['script']."/p/".$parameter_duplicate."/';\"><i class=\"glyphicon glyphicon-chevron-right\"></i> ".$this->str_label["LBL_DUPLICATE"]."</button> ";
            
            
            array_push($column_data, $links);
            
            $result[] = $column_data;
            
        }
        
        $output = array("draw"=>$_GET['draw'], "recordsTotal"=>$records_total, "recordsFiltered"=>$records_filtered, "data"=>$result);
        
        // Format to json
        echo json_encode($output);        
        
    }     
    
    
    
    public function get_data ( $config ) {
        
        $result = array();
        
        if(!empty($_GET[$config['primary_key']])){
		
            $cond = "";
            if (isset($_GET[$config['primary_key']])) $cond = $config['primary_key']."='".$_GET[$config['primary_key']]."' and ";
            if (isset($_GET[$config['primary_key2']])) $cond = $config['primary_key2']."='".$_GET[$config['primary_key2']]."' and ";
            if (isset($_GET[$config['primary_key3']])) $cond = $config['primary_key3']."='".$_GET[$config['primary_key3']]."' and ";
            $cond = substr($cond,0,-5);
            
            $sql="select * from ".$config['table']." where $cond ";
            $this->page->debug($sql);
            $res_data=$this->db->Execute($sql);
            if(!$res_data) $this->page->debug_query(1);
            $row=$res_data->FetchRow();
            foreach($row as $key=>$val){
                $key=strtolower($key);
                $result[$key]=$val;
            }
            
            // Reset password and verify variable
            $form_variable_arr = array("verify","password");
            foreach ($form_variable_arr as $key => $value) {
                $result[$value] = "";
            }
        }
        
        return $result;
        
    }
    
    
    public function delete( $config ) {
        
        $this->page->do_debug = 0;
        $this->page->debug($_GET);
        
        $cond = "";
        if (isset($_GET[$config['primary_key']])) $cond = $config['primary_key']."='".$_GET[$config['primary_key']]."' and ";
        if (isset($_GET[$config['primary_key2']])) $cond = $config['primary_key2']."='".$_GET[$config['primary_key2']]."' and ";
        if (isset($_GET[$config['primary_key3']])) $cond = $config['primary_key3']."='".$_GET[$config['primary_key3']]."' and ";
        $cond = substr($cond,0,-5);
        
        $sql="delete from ".$config['table']." where $cond";
        $this->page->debug($sql,1);
        $res_del=$this->db->Execute($sql);
        if (!$res_del) $this->page->debug_query(1);
        
        $this->log->insert_log("DELETE ".$config['table']." ".$config['primary_key'].": ".$_GET[$config['primary_key']]);
        
    }
    
    
    public function validate( $config ) {
        
        $error = "";
        $error = $this->form->validate( $config );
        
        // Return Error
        if($error){
            echo '{ "status": "error", "message": "<b>Invalid Input</b><hr><ul>'.$error.'</ul>" }';
            die();
        }
        
    }
    
    
    public function set_data( $config ) {
        
        $this->page->do_debug = 0;
        
        $columns = "";
        $values = "";
        $list = "";
        $cond = "";
        
        $act = $_POST['act'];
        
        $str_in = "";
        foreach ($_POST['show'] as $key => $value) {
            $str_in .= "'$value',";
        }
        $str_in = substr($str_in, 0, -1);

        $sql = "delete from ".$config['table']." where name='".$_POST['name']."' and men_id in (".$str_in.")";
        $this->page->debug($sql);
        $result=$this->db->Execute($sql);
        if(!$result) $this->page->debug_query(1);

        foreach( $_POST as $key=>$val ) {
            if( preg_match("/read|edit|add|delete/i", $key )) {
                
                foreach($val as $key1 => $val1){
                    
                    // Check if already exist
                    $sql="select men_id as men_id_exist from ".$config['table']." where men_id='".$val1."' and name='".$_POST['name']."'";
                    $this->page->debug($sql);
                    $result=$this->db->Execute($sql);
                    if(!$result) $this->page->debug_query(1);
                    $row=$result->FetchRow();
                    $men_id_exist=$row['men_id_exist'];
                    
                    if(!empty($men_id_exist)){
                        
                        $sql="update ".$config['table']." set ".$key."_priv='1' where men_id='".$val1."' and name='".$_POST['name']."'";
                        $this->page->debug($sql);
                        $result=$this->db->Execute($sql);
                        if(!$result) $this->page->debug_query(1);
                    }else{
                        
                        $sql="insert into ".$config['table']." (".$key."_priv,men_id,name) values ('1','".$val1."','".$_POST['name']."') ";
                        $this->page->debug($sql);
                        $result=$this->db->Execute($sql);
                        if(!$result) $this->page->debug_query(1);
                    }
                }
            }

        }
        
        $this->log->insert_log("INSERT ".$config['table']." ".$config['primary_key'].": ".$_POST[$config['primary_key']]);
        
        echo '{ "status": "success", "message": "Data has been successfully saved" }';
        
    }
    
    
    public function set_duplicate( $config ) {
        
        $this->page->do_debug = 0;
        $this->page->debug($_POST);
        
        $sql = "insert into ".$config['table']." (name, read_priv, edit_priv, delete_priv, add_priv, men_id) 
                select  '".$_POST['name']."', read_priv, edit_priv, delete_priv, add_priv, men_id from ".$config['table']." where name='".$_POST['duplicate_from']."' 
                ";
        $this->page->debug($sql);
        $res_duplicate=$this->db->Execute($sql);
        if (!$res_duplicate) $this->page->debug_query(1);
        
        $this->log->insert_log("INSERT ".$config['table']." ".$config['primary_key'].": ".$_POST[$config['primary_key']]);
        
        echo '{ "status": "success", "message": "Data has been successfully saved" }';
        
    }
    
    
    public function generate_form ( $config ) {
        
        $output = "";
        foreach ($config["form_fields"] as $key => $form_group) {
            
            $input_disabled = $form_group["disabled"];
            
            $mandatory_sign = "";
            if (preg_match("/mandatory/i",$form_group["validation"])) {
                $mandatory_sign = "*";
            }
            
            $input = "";
            if (isset($form_group["custom_input_type"])) {
                $input = $form_group["custom_input_type"];
            }
            else {
                $input = $this->form->generate_standard_input ( $form_group, $input_disabled, $config, $this->str_label );
            }
            
            if (isset($form_group["notification"])) {
                $input .= $form_group["notification"];
            }
            
            $output .= "
            
            <div class=\"form-group\">
                <label class=\"control-label col-sm-2\" for=\"".$form_group["field_variable"]."\">".$form_group["field_name"]." ".$mandatory_sign."</label>
                <div class=\"col-lg-6 col-md-8 col-sm-10\">
                ".$input."                
                </div>
            </div>
            
            ";
            
            // Button Label
            $button_label = (($this->page->get_operation("act","add"))?$this->str_label["LBL_ADD"]:$this->str_label["LBL_EDIT"]);
            
            $submit_button = "";
            if ($this->page->get_operation("act","add") || $this->page->get_operation("act","update"))
                $submit_button = "<input type=\"submit\" class=\"btn btn-primary button-submit\" value=\"".$button_label."\" style=\"width:100px;\" />";
            
            // Set variable inside main content
            $hidden_pk1_var = "";
            $hidden_pk2_var = "";
            $hidden_pk3_var = "";
            $notification_password = "";
            if ($config['primary_key']) $hidden_pk1_var = "<input type=\"hidden\" name=\"".$config['primary_key']."\" value=\"".@$_GET[$config['primary_key']]."\">";
            if ($config['primary_key2']) $hidden_pk2_var = "<input type=\"hidden\" name=\"".$config['primary_key2']."\" value=\"".@$_GET[$config['primary_key2']]."\">";
            if ($config['primary_key3']) $hidden_pk3_var = "<input type=\"hidden\" name=\"".$config['primary_key3']."\" value=\"".@$_GET[$config['primary_key3']]."\">"; 
            
            $_act=(!empty($_GET[$config['primary_key']]))?"do_update":"do_add";
            

$main_content = <<<EOD
            <form id="f1"  class="form-horizontal" role="form" method="post" action="{$config['script']}/p/0/" enctype="multipart/form-data">	
            <input type="hidden" name="act" value="$_act">
            {$hidden_pk1_var}
            {$hidden_pk2_var}
            {$hidden_pk3_var}
            
            {$output}
            
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                <button type="button" class="btn btn-primary button-back" onClick="window.location.href='{$config['script']}/p/0/'; return false" style="width:100px"><i class="fa fa-arrow-left" style="margin-right:10px"></i> {$this->str_label["LBL_BACK"]}</button>
                {$submit_button}
                </div>
            </div>
            </form>
            {$this->str_label["MSG_FIELDS_MANDATORY"]}
EOD;
            
        }
        
        return $main_content;
        
    }
    
    
    
    public function generate_list( $config ) {
        
        global $add_button;
        
        // Check Button Privileges
        $add_button = "";
        if ($this->page->get_function_access("add",$config['script'])) {
            $parameter_add = $this->page->set_parameter ( array("act"=>"add"), $config['user_detail']->challenge );
            $add_button = "<button type=\"button\" class=\"btn btn-primary btn-xs button-add\" onClick=\"window.location.href='".$config['script']."/p/".$parameter_add."/';\"><i class=\"glyphicon glyphicon-plus\"></i> ".$this->str_label["LBL_ADD"]."</button>";
        }           

        $column_label = "";
        foreach ($config['data_tables_columns_label'] as $key => $value) {
            $column_label .= "<th>".$value."</th>"; 
        }

$main_content = <<<EOD
	<table class="table table-striped table-hover" id="dataTables">
		<thead>
			<tr>
				{$column_label}
				<th width="250">Functions</th>
			</tr>
		</thead>
	</table>
EOD;

        return $main_content;
        
    }
    
    
    public function generate_function_access_form( $config, $name ) {
        
        $main_content = "
            <table id=\"tt\" class=\"easyui-treegrid fixedtable\"
                data-options=\"url:'".$config['script']."/p/0/?name=$name&act=treelist',idField:'id',treeField:'title'\" width=\"100%\">
            <thead>
                <tr>
                    <th field=\"title\" width=\"50%\">Menu</th>
                    <th field=\"read\" width=\"10%\">Read</th>
                    <th field=\"add\" width=\"10%\">Add</th>
                    <th field=\"edit\" width=\"10%\">Edit</th>
                    <th field=\"delete\" width=\"10%\">Delete</th>
                    <th field=\"checkrow\" width=\"5%\"> </th>
                    <th field=\"uncheckrow\" width=\"5%\"> </th>
                    <th field=\"show\" width=\"0%\"> </th>
                </tr>
            </thead>
            </table>	
            <button class=\"btn btn-primary btn-xs\" onClick=\"$('.easyui-treegrid').treegrid('expandAll');return false;\">Expand All</button> <button class=\"btn btn-primary btn-xs\" onClick=\"$('.easyui-treegrid').treegrid('collapseAll');return false;\">Collapse All</button>
        ";        
        
        return $main_content;
        
    }
    
    
    public function get_treelist ( $config ) {
        
        $this->page->do_debug = 0;
        
        $result = array();
	
        if (!isset($_POST['id'])) $cond = "(reference is null or reference='')";
        else $cond = "reference='".$_POST['id']."'";
            
        $sql="select * from app_menu where $cond and isnull(is_delete,0)!='1' order by weight asc";
        $this->page->debug($sql);
        $res_menu=$this->db->Execute($sql);
        if (!$res_menu) $this->page->debug_query(1);
        while($row_menu=$res_menu->FetchRow()) {
            if (is_array($row_menu)){ foreach($row_menu as $key=>$val) {
                $key=strtolower($key);
                $$key=$val;
            }}
            
            $read_priv = 0;
            $add_priv = 0;
            $edit_priv = 0;
            $delete_priv = 0;
            
            // Get Privilege Data
            $sql="select read_priv, edit_priv, delete_priv, add_priv from ".$config['table']." where ".$config['primary_key']."='".$_GET[$config['primary_key']]."' and men_id='".$men_id."'";
            $this->page->debug($sql);
            $row=$this->db->GetRow($sql);
            if (is_array($row)){ foreach($row as $key=>$val) {
                $key=strtolower($key);
                $$key=$val;
            }}
        
            // Check Child
            $child = false;
            $sql="select count(*) as num from app_menu where reference='".$men_id."' and isnull(is_delete,0)!='1'";
            $this->page->debug($sql);
            $num=$this->db->GetOne($sql);
            $child =  ($num > 0) ? "closed" : "open";	   
            
            // Generate default value for child addition
            $new_menu_level = $menu_level + 1;
            $new_reference = $men_id;
            $sql="select max(weight) last_weight from app_menu where reference='".$men_id."' and isnull(is_delete,0)!='1'";
            $this->page->debug($sql);
            $last_weight=$this->db->GetOne($sql);
            if (!isset($last_weight)) $last_weight = 0;
            $new_weight = $last_weight + 10;
            
            $read = "<input id=\"read_".$men_id."\" type=\"checkbox\" name=\"read[]\" value=\"$men_id\" ".($read_priv=='1'?"checked":"").">";
            $add = "<input id=\"add_".$men_id."\" type=\"checkbox\" name=\"add[]\" value=\"$men_id\" ".($add_priv=='1'?"checked":"").">";
            $edit = "<input id=\"edit_".$men_id."\" type=\"checkbox\" name=\"edit[]\" value=\"$men_id\" ".($edit_priv=='1'?"checked":"").">";
            $delete = "<input id=\"delete_".$men_id."\" type=\"checkbox\" name=\"delete[]\" value=\"$men_id\" ".($delete_priv=='1'?"checked":"").">";
            $show = "<input type=\"hidden\" name=show[] value=\"$men_id\">";
            $checkrow = "<a href=javascript:; onClick=\"check_row('".$men_id."')\"><i class=\"fa fa-check\"></i></a>";
            $uncheckrow = "<a href=javascript:; onClick=\"uncheck_row('".$men_id."')\"><i class=\"fa fa-times\"></i></a>";
            
            $node = array();
            $node['id'] = $men_id;
            $node['title'] = $title;
            $node['state'] = $child;
            $node['read'] = $read;
            $node['add'] = $add;
            $node['edit'] = $edit;
            $node['delete'] = $delete;
            $node['checkrow'] = $checkrow;
            $node['uncheckrow'] = $uncheckrow;
            $node['show'] = $show;
            
            array_push($result,$node);
        
        }
            
        echo json_encode($result);
        
    }
    
}

?>