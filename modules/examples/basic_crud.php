<?php

// Include Classes and Basic Settings
include $_SERVER['DOCUMENT_ROOT']."/s/config.php";

// Get Label
$language->get_lang("global");
$language->get_lang("examples");




// ===================================================================================================================
// CRUD Setting Section: Start 
// ===================================================================================================================

// Include Process Class
include "m/basic_crud.php";
$process = new basic_crud( $db, $page, $form, $session, $log, $date, $str_label );

// Page Settings
$process->do_debug = true;
$config['script'] = "/modules/examples/basic_crud";
$config['page_title'] = "Basic CRUD";
$config['tabs'] = array();

// Table Settings
$config['table'] = "exp_basic_crud";
$config['primary_key'] = "fra_id";
$config['primary_key2'] = "";
$config['primary_key3'] = "";
$config['primary_key_prefix'] = "FRA";

// Data Tables Settings
$config['data_tables_columns'] = array("fra_id","name","masked_input","numeric","currency");
$config['data_tables_columns_label'] = array("FRA ID",$str_label['LBL_NAME'],$str_label['LBL_MASKED_INPUT'],$str_label['LBL_NUMERIC'],$str_label['LBL_CURRENCY']);
$config['data_tables_default_sort'] = "fra_id";
$config['data_tables_default_order'] = "asc";
$config['data_tables_default_sort_column'] = array_search ($config['data_tables_default_sort'], $config['data_tables_columns']); // No need to edit this

function form_settings( $values, $form, $page, $str_label ) {
	foreach ($values as $k => $v) $$k = $v;
	
	// Disable inputs for View
	$input_disabled = "";
	if ($page->get_operation("act","view")) {
		$input_disabled = "disabled";
	}

// Form Settings
$settings = 
	array(
		array(
			"field_name" => $str_label['LBL_NAME'],
			"field_variable" => "name",
			"field_value" => @$name,
			"input_type" => "text", 				// text/password/email/date/multidate/numeric/currency/file/masked
			"validation" => "mandatory",			// mandatory,valid_email,valid_number,valid_date
			"disabled" => $input_disabled,
		),
		array(
			"field_name" => $str_label['LBL_MASKED_INPUT'],
			"field_variable" => "masked_input",
			"field_value" => @$masked_input,
			"input_type" => "masked",
			"input_config" => array(
				"format" => "99.999.999.9-999.999"	// a - Represents an alpha character (A-Z,a-z), 9 - Represents a numeric character (0-9), * - Represents an alphanumeric character (A-Z,a-z,0-9)

			),
			"validation" => "mandatory",
			"disabled" => $input_disabled,
		),
		array(
			"field_name" => $str_label['LBL_NUMERIC'],
			"field_variable" => "numeric",
			"field_value" => @$numeric,
			"input_type" => "numeric",
			"validation" => "mandatory",
			"disabled" => $input_disabled,
		),
		array(
			"field_name" => $str_label['LBL_CURRENCY'],
			"field_variable" => "currency",
			"field_value" => @$currency,
			"input_type" => "currency",
			"validation" => "mandatory",
			"disabled" => $input_disabled,
		),
		array(
			"field_name" => $str_label['LBL_TEXT'],
			"field_variable" => "text",
			"field_value" => @$text,
			"input_type" => "textarea",
			"validation" => "mandatory",
			"disabled" => $input_disabled,
		),
		array(
			"field_name" => $str_label['LBL_HTML'],
			"field_variable" => "html",
			"field_value" => @$html,
			"input_type" => "wysiwyg",
			"validation" => "mandatory",
			"disabled" => $input_disabled,
		),
		array(
			"field_name" => $str_label['LBL_FILE'],
			"field_variable" => "photo",
			"field_value" => @$photo,
			"input_type" => "file",
			"input_config" =>array(
				"type" => "single", 						// single/multiple
				"target_dir" => "/i/photo/",
				"extensions" => "jpg,png"			// allowed file extensions
			),	
			"validation" => "",
			"disabled" => $input_disabled,
		),
		array(
			"field_name" => $str_label['LBL_MULTI_FILES'],
			"field_variable" => "attachment",
			"field_value" => @$attachment,
			"input_type" => "file",
			"input_config" =>array(
				"type" => "multiple", 						// single/multiple
				"target_dir" => "/i/upload/",
				"extensions" => "doc,xls,pdf"			// allowed file extensions
			),	
			"validation" => "",
			"disabled" => $input_disabled,
		)
		
	);
	return $settings;
}

// ===================================================================================================================
// CRUD Setting Section: End 
// ===================================================================================================================








// Check Access Privilege
if (!$page->get_function_access("read",$config['script'])) {
	$template->box($str_label["LBL_ILLEGAL_ACCESS"], $str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"], array(array("back","Back")), "warning");
	die();
}

// Logged in user Details
$user_detail = $session->get_user_details_session($session->get_session_id());
$config['user_detail'] = $user_detail;

// Get all encrypted parameters
$page->get_parameter ( $user_detail->challenge, $str_label );

// Debug Requests
if ($page->get_operation("act","add") || $page->get_operation("act","update")) $page->debug($_GET);
if ($page->get_operation("act","do_add") || $page->get_operation("act","do_update")) $page->debug($_POST);
if ($page->get_operation("act","do_add") || $page->get_operation("act","do_update")) $page->debug($_FILES);

// Process Tabs
$tabs = $page->generate_tabs( $config );


// ===================================================
// Process Delete Data
// ===================================================
if ($page->get_operation("act","delete")) {
	
	// Check Access Privileges
	if (!$page->get_function_access("delete",$config['script'])) {
		$template->box($str_label["LBL_ILLEGAL_ACCESS"], $str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"], array(array("back","Back")), "warning");
		die();
	}
	
	// Delete Process
	$process->delete( $config );

}
// ===================================================
// Process Do Add/Do Update Data
// ===================================================
elseif ($page->get_operation("act","do_add") || $page->get_operation("act","do_update")) {

	if(!empty($_POST[$config['primary_key']])) $act="do_update";
	
	// Check Access Privileges
	if ( 	(!$page->get_function_access("add",$config['script']) && $page->get_operation("act","do_add")) || 
			(!$page->get_function_access("edit",$config['script']) && $page->get_operation("act","do_update"))
		) {
		$template->box($str_label["LBL_ILLEGAL_ACCESS"], $str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"], "", "warning");
		die();
	}
	
	// Prepare fields
	$values = array();
	$config["form_fields"] = form_settings( $values, $form, $page, $str_label );
	
	//Form Validation
	$process->validate( $config );
	
	// Save Form
	$process->set_data( $config );	
	
	die();

}
// ===================================================
// Process Get List
// ===================================================
elseif ($page->get_operation("act","list")) {
	
	// Check Access Privileges
	$edit_privilege = false;
	$delete_privilege = false;
	if ($page->get_function_access("edit",$config['script'])) $config['edit_privilege'] = true;
	if ($page->get_function_access("delete",$config['script'])) $config['delete_privilege'] = true;  
	
	// Process Get List
	$process->get_list( $config );
	
	die();

}
// ===================================================
// Process Add/Update Form
// ===================================================
else if ($page->get_operation("act","add") || $page->get_operation("act","update")  || $page->get_operation("act","view")) {

	// Check Access Privilege
	if ( 	(!$page->get_function_access("add",$config['script']) && $page->get_operation("act","add")) || 
			(!$page->get_function_access("edit",$config['script']) && $page->get_operation("act","update"))
		) {
		$template->box($str_label["LBL_ILLEGAL_ACCESS"], $str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"], array(array("back","Back")), "warning");
		die();
	}
	    
	// Process Get Data
	$result = $process->get_data( $config );
	$values = array();
	foreach ($result as $key => $value) {
		$values[$key] = $value;
	}
	
	// Prepare Form Setting 
	$config["form_fields"] = form_settings( $values, $form, $page, $str_label );
	
	// Generate Form
	$main_content = $process->generate_form ( $config );
	
}
// ===================================================
// Process List View
// ===================================================
else {

	$main_content = $process->generate_list ( $config );

}

//Page Template
include $_SERVER["DOCUMENT_ROOT"]."/view/basic_page.php";
?> 