<?php

// Include Classes and Basic Settings
include $_SERVER['DOCUMENT_ROOT']."/s/config.php";

// Get Label
$language->get_lang("global");
$language->get_lang("examples");




// ===================================================================================================================
// CRUD Setting Section: Start 
// ===================================================================================================================

// Include Process Class
include "m/basic_report.php";
$process = new basic_report( $db, $page, $form, $session, $log, $date, $str_label );

// Page Settings
$process->do_debug = true;
$config['script'] = "/modules/examples/basic_report";
$config['page_title'] = "Basic Report";
$config['tabs'] = array();

// Table Settings
$config['table'] = "exp_basic_crud";
$config['primary_key'] = "fra_id";
$config['primary_key2'] = "";
$config['primary_key3'] = "";
$config['primary_key_prefix'] = "FRA";

function form_settings( $values, $form, $page, $str_label ) {
	foreach ($values as $k => $v) $$k = $v;
	
	// Disable inputs for View
	$input_disabled = "";
	if ($page->get_operation("act","view")) {
		$input_disabled = "disabled";
	}

// Form Settings
$settings = 
	array(
		array(
			"field_name" => "Name",
			"field_variable" => "name",
			"field_value" => @$name,
			"input_type" => "text", 				// text/password/email/date/multidate/numeric/currency/file/masked
			"validation" => "",						// mandatory,valid_email,valid_number,valid_date
			"disabled" => $input_disabled,
		),
		array(
			"field_name" => "Output",
			"field_variable" => "output",
			"field_value" => @$output,
			"input_type" => "text",
			"custom_input_type" => $form->select_list("output","app_report_output","name","name",@$output," class=\"form-control\" $input_disabled "," order by rpo_id asc ",""),
			"validation" => "mandatory",
			"disabled" => $input_disabled,
		)
		
	);
	return $settings;
}

// ===================================================================================================================
// CRUD Setting Section: End 
// ===================================================================================================================








// Check Access Privilege
if (!$page->get_function_access("read",$config['script'])) {
	$template->box($str_label["LBL_ILLEGAL_ACCESS"], $str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"], array(array("back","Back")), "warning");
	die();
}

// Logged in user Details
$user_detail = $session->get_user_details_session($session->get_session_id());
$config['user_detail'] = $user_detail;

// Get all encrypted parameters
$page->get_parameter ( $user_detail->challenge, $str_label );

// Debug Requests
if ($page->get_operation("act","add") || $page->get_operation("act","update")) $page->debug($_GET);
if ($page->get_operation("act","do_add") || $page->get_operation("act","do_update")) $page->debug($_POST);
if ($page->get_operation("act","do_add") || $page->get_operation("act","do_update")) $page->debug($_FILES);

// Process Tabs
$tabs = $page->generate_tabs( $config );



// ===================================================
// Process Generate Report
// ===================================================
if ($page->get_operation("act","report")) {
	
	// Check Access Privileges
	if (!$page->get_function_access("read",$config['script'])) {
		$template->box($str_label["LBL_ILLEGAL_ACCESS"], $str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"], array(array("back","Back")), "warning");
		die();
	}
	
	// Prepare fields
	$values = array();
	$config["form_fields"] = form_settings( $values, $form, $page, $str_label );  
	
	// Validation
	$process->validate( $config );
	
	// Process Get List
	$data = $process->get_list( $config );
	
	// Process Generate Report
	$process->generate_report ( $config, $data );
	
}
// ===================================================
// Create Loader page
// ===================================================
else if ($page->get_operation("act","loader")) {
	
	echo "<link href=\"/assets/css/style.min.css\" rel=\"stylesheet\" />";
	echo "<link href=\"/assets/plugins/font-awesome/css/font-awesome.min.css\" rel=\"stylesheet\" />";
	echo "<i class=\"fa fa-spinner fa-pulse fa-2x\" id=\"submit-loader\"></i> ".$str_label["MSG_GENERATING_REPORT"];
	
	die();
}
// ===================================================
// Process Report Form
// ===================================================
else {

	// Check Access Privilege
	if (!$page->get_function_access("read",$config['script'])) {
		$template->box($str_label["LBL_ILLEGAL_ACCESS"], $str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"], array(array("back","Back")), "warning");
		die();
	}
	    
	$values = array();
	
	// Prepare Form Setting 
	$config["form_fields"] = form_settings( $values, $form, $page, $str_label );
	
	// Generate Form
	$main_content = $process->generate_form ( $config );
	
}

//Page Template
if (!$page->get_operation("act","report")) include $_SERVER["DOCUMENT_ROOT"]."/view/basic_report.php";
?> 