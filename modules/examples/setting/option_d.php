<?php

// Include Classes and Basic Settings
include $_SERVER['DOCUMENT_ROOT']."/s/config.php";

// Get Label
$language->get_lang("global");




// ===================================================================================================================
// CRUD Setting Section: Start 
// ===================================================================================================================

// Include Process Class
include "m/option_d.php";
$process = new option_d( $db, $page, $form, $session, $log, $date, $str_label );

// Page Settings
$process->do_debug = true;
$config['script'] = "/modules/examples/setting/option_c";
$config['page_title'] = "Option C Management";
$config['tabs'] = array(
	array("name"=>"Option B", "active"=>"0", "link"=>"/modules/examples/setting/option_b/p/0/"),
	array("name"=>"Option C", "active"=>"1")
);

// Table Settings
$config['table'] = "exp_option_c";
$config['primary_key'] = "opc_id";
$config['primary_key2'] = "opb_id";
$config['primary_key3'] = "";
$config['primary_key_prefix'] = "OPC";

// Data Tables Settings
$config['data_tables_columns'] = array("opc_id","opb_id","name","remark");
$config['data_tables_columns_label'] = array("OPC ID","Parent","Name","Remark");
$config['data_tables_default_sort'] = "opc_id";
$config['data_tables_default_order'] = "asc";
$config['data_tables_default_sort_column'] = array_search ($config['data_tables_default_sort'], $config['data_tables_columns']); // No need to edit this

function form_settings( $values, $form, $page ) {
	foreach ($values as $k => $v) $$k = $v;
	
	// Disable inputs for View
	$input_disabled = "";
	if ($page->get_operation("act","view")) {
		$input_disabled = "disabled";
	}

// Form Settings
$settings = 
	array(
		array(
			"field_name" => "Parent",
			"field_variable" => "opb_id",
			"field_value" => @$opb_id,
			"input_type" => "text", 				// text/password/email/date/multidate/numeric/currency/file
			"custom_input_type" => $form->select_list("opb_id","exp_option_b","opb_id","name",@$opb_id," class=\"form-control\" disabled ","","select opb_id, opb_id+' '+name as name from exp_option_b"),
			"validation" => "",						// mandatory,valid_email,valid_number,valid_date
			"disabled" => "",
		),
		array(
			"field_name" => "Name",
			"field_variable" => "name",
			"field_value" => @$name,
			"input_type" => "text",
			"validation" => "mandatory",
			"disabled" => $input_disabled,
		),
		array(
			"field_name" => "Remark",
			"field_variable" => "remark",
			"field_value" => @$remark,
			"input_type" => "textarea",
			"validation" => "",
			"disabled" => $input_disabled,
		)
		
	);
	return $settings;
}

// ===================================================================================================================
// CRUD Setting Section: End 
// ===================================================================================================================








// Check Access Privilege
if (!$page->get_function_access("read",$config['script'])) {
	$template->box($str_label["LBL_ILLEGAL_ACCESS"], $str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"], array(array("back","Back")), "warning");
	die();
}

// Logged in user Details
$user_detail = $session->get_user_details_session($session->get_session_id());
$config['user_detail'] = $user_detail;

// Get all encrypted parameters
$page->get_parameter ( $user_detail->challenge, $str_label );

// Debug Requests
if ($page->get_operation("act","add") || $page->get_operation("act","update")) $page->debug($_GET);
if ($page->get_operation("act","do_add") || $page->get_operation("act","do_update")) $page->debug($_POST);
if ($page->get_operation("act","do_add") || $page->get_operation("act","do_update")) $page->debug($_FILES);

// Process Tabs
$tabs = $page->generate_tabs( $config );


// ===================================================
// Process Delete Data
// ===================================================
if ($page->get_operation("act","delete")) {
	
	// Check Access Privileges
	if (!$page->get_function_access("delete",$config['script'])) {
		$template->box($str_label["LBL_ILLEGAL_ACCESS"], $str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"], array(array("back","Back")), "warning");
		die();
	}
	
	// Delete Process
	$process->delete( $config );

}
// ===================================================
// Process Do Add/Do Update Data
// ===================================================
elseif ($page->get_operation("act","do_add") || $page->get_operation("act","do_update")) {

	if(!empty($_POST[$config['primary_key']])) $act="do_update";
	
	// Check Access Privileges
	if ( 	(!$page->get_function_access("add",$config['script']) && $page->get_operation("act","do_add")) || 
			(!$page->get_function_access("edit",$config['script']) && $page->get_operation("act","do_update"))
		) {
		$template->box($str_label["LBL_ILLEGAL_ACCESS"], $str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"], "", "warning");
		die();
	}
	
	// Prepare fields
	$values = array();
	$config["form_fields"] = form_settings( $values, $form, $page );
	
	//Form Validation
	$process->validate( $config );
	
	// Save Form
	$process->set_data( $config );	
	
	die();

}
// ===================================================
// Process Get List
// ===================================================
elseif ($page->get_operation("act","list")) {
	
	// Check Access Privileges
	$edit_privilege = false;
	$delete_privilege = false;
	if ($page->get_function_access("edit",$config['script'])) $config['edit_privilege'] = true;
	if ($page->get_function_access("delete",$config['script'])) $config['delete_privilege'] = true;  
	
	// Process Get List
	$process->get_list( $config );
	
	die();

}
// ===================================================
// Process Add/Update Form
// ===================================================
else if ($page->get_operation("act","add") || $page->get_operation("act","update")  || $page->get_operation("act","view")) {

	// Check Access Privilege
	if ( 	(!$page->get_function_access("add",$config['script']) && $page->get_operation("act","add")) || 
			(!$page->get_function_access("edit",$config['script']) && $page->get_operation("act","update"))
		) {
		$template->box($str_label["LBL_ILLEGAL_ACCESS"], $str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"], array(array("back","Back")), "warning");
		die();
	}
	    
	// Process Get Data
	$result = $process->get_data( $config );
	$values = array();
	foreach ($result as $key => $value) {
		$values[$key] = $value;
	}
	
	// Additional Process to give default values on add process
	if (isset($_GET['opb_id'])) $values['opb_id'] = $_GET['opb_id'];
	
	// Prepare Form Setting 
	$config["form_fields"] = form_settings( $values, $form, $page );
	
	// Generate Form
	$main_content = $process->generate_form ( $config );
	
}
// ===================================================
// Process List View
// ===================================================
else {

	$main_content = $process->generate_list ( $config );

}

//Page Template
include $_SERVER["DOCUMENT_ROOT"]."/view/basic_page.php";
?> 