<?php

// Include Classes and Basic Settings
include $_SERVER['DOCUMENT_ROOT']."/s/config.php";

// Get Label
$language->get_lang("global");




// ===================================================================================================================
// CRUD Setting Section: Start 
// ===================================================================================================================

// Include Process Class
include "m/option_a.php";
$process = new option_a( $db, $page, $form, $session, $log, $date, $str_label );

// Page Settings
$process->do_debug = true;
$config['script'] = "/modules/examples/setting/option_a";
$config['page_title'] = "Option A Management";

// Table Settings
$config['table'] = "exp_option_a";
$config['primary_key'] = "opa_id";
$config['primary_key2'] = "";
$config['primary_key3'] = "";
$config['primary_key_prefix'] = "OPA";

// Data Tables Settings
$config['data_tables_columns'] = array("opa_id","name","parent_id","remark");
$config['data_tables_columns_label'] = array("OPA ID","Name","Parent","Remark");
$config['data_tables_default_sort'] = "opa_id";
$config['data_tables_default_order'] = "asc";
$config['data_tables_default_sort_column'] = array_search ($config['data_tables_default_sort'], $config['data_tables_columns']); // No need to edit this

function form_settings( $values, $form, $page ) {
	foreach ($values as $k => $v) $$k = $v;
	
	// Disable inputs for View
	$input_disabled = "";
	if ($page->get_operation("act","view")) {
		$input_disabled = "disabled";
	}

// Form Settings
$settings = 
	array(
		array(
			"field_name" => "Name",
			"field_variable" => "name",
			"field_value" => @$name,
			"input_type" => "text", 				// text/password/email/date/multidate/numeric/currency/file
			"validation" => "mandatory",			// mandatory,valid_email,valid_number,valid_date
			"disabled" => $input_disabled,
		),
		array(
			"field_name" => "Parent",
			"field_variable" => "parent_id",
			"field_value" => @$parent_id,
			"input_type" => "text",
			"custom_input_type" => $form->select_list("parent_id","exp_option_a","opa_id","name",@$parent_id," class=\"form-control\" $input_disabled ","","select opa_id, opa_id+' '+name as name from exp_option_a"),
			"validation" => "",
			"disabled" => $input_disabled,
		),
		array(
			"field_name" => "Remark",
			"field_variable" => "remark",
			"field_value" => @$remark,
			"input_type" => "text",
			"validation" => "",
			"disabled" => $input_disabled,
		)
		
	);
	return $settings;
}

// ===================================================================================================================
// CRUD Setting Section: End 
// ===================================================================================================================








// Check Access Privilege
if (!$page->get_function_access("read",$config['script'])) {
	$template->box($str_label["LBL_ILLEGAL_ACCESS"], $str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"], array(array("back","Back")), "warning");
	die();
}

// Logged in user Details
$user_detail = $session->get_user_details_session($session->get_session_id());
$config['user_detail'] = $user_detail;

// Get all encrypted parameters
$page->get_parameter ( $user_detail->challenge, $str_label );

// Debug Requests
if ($page->get_operation("act","add") || $page->get_operation("act","update")) $page->debug($_GET);
if ($page->get_operation("act","do_add") || $page->get_operation("act","do_update")) $page->debug($_POST);
if ($page->get_operation("act","do_add") || $page->get_operation("act","do_update")) $page->debug($_FILES);



// ===================================================
// Process Delete Data
// ===================================================
if ($page->get_operation("act","delete")) {
	
	// Check Access Privileges
	if (!$page->get_function_access("delete",$config['script'])) {
		$template->box($str_label["LBL_ILLEGAL_ACCESS"], $str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"], array(array("back","Back")), "warning");
		die();
	}
	
	// Delete Process
	$process->delete( $config );

}
// ===================================================
// Process Do Add/Do Update Data
// ===================================================
elseif ($page->get_operation("act","do_add") || $page->get_operation("act","do_update")) {

	if(!empty($_POST[$config['primary_key']])) $act="do_update";
	
	// Check Access Privileges
	if ( 	(!$page->get_function_access("add",$config['script']) && $page->get_operation("act","do_add")) || 
			(!$page->get_function_access("edit",$config['script']) && $page->get_operation("act","do_update"))
		) {
		$template->box($str_label["LBL_ILLEGAL_ACCESS"], $str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"], "", "warning");
		die();
	}
	
	// Prepare fields
	$values = array();
	$config["form_fields"] = form_settings( $values, $form, $page );
	
	//Form Validation
	$process->validate( $config );
	
	// Save Form
	$process->set_data( $config );	
	
	die();

}
// ===================================================
// Process Get List
// ===================================================
elseif ($page->get_operation("act","list")) {
	
	// Process Get List
	$process->get_list( $config );
	
	die();

}
// ===================================================
// Process Add/Update Form
// ===================================================
else if ($page->get_operation("act","add") || $page->get_operation("act","update")  || $page->get_operation("act","view")) {

	// Check Access Privilege
	if ( 	(!$page->get_function_access("add",$config['script']) && $page->get_operation("act","add")) || 
			(!$page->get_function_access("edit",$config['script']) && $page->get_operation("act","update"))
		) {
		$template->box($str_label["LBL_ILLEGAL_ACCESS"], $str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"], array(array("back","Back")), "warning");
		die();
	}
	    
	// Process Get Data
	$result = $process->get_data( $config );
	$values = array();
	foreach ($result as $key => $value) {
		$values[$key] = $value;
	}
	
	// Additional Process to give default values on add process
	if (isset($_GET['parent_id'])) $values['parent_id'] = $_GET['parent_id'];
		
	// Prepare Form Setting 
	$config["form_fields"] = form_settings( $values, $form, $page );
	
	// Generate Form
	$main_content = $process->generate_form ( $config );
	
}
// ===================================================
// Process List View
// ===================================================
else {

	$add_button = "";
	if ($page->get_function_access("add",$config['script'])) {
		$parameter_add = $page->set_parameter ( array("act"=>"add"), $config['user_detail']->challenge );
		$add_button = "<button type=\"button\" class=\"btn btn-primary btn-xs button-add\" onClick=\"window.location.href='".$config['script']."/p/".$parameter_add."/';\"><i class=\"glyphicon glyphicon-plus\"></i> ".$str_label["LBL_ADD"]."</button>";
	}

$main_content = <<<EOD
	<table id="tt" class="easyui-treegrid fixedtable"
        data-options="url:'{$config['script']}/p/0/?act=list',idField:'id',treeField:'name'" width="100%">
    <thead>
        <tr>
            <th field="name" width="50%">Name</th>
            <th field="remark" width="10%">Remark</th>
			<th field="functions" width="30%">Functions</th>
        </tr>
    </thead>
	</table>	
EOD;

}

//Page Template
include $_SERVER["DOCUMENT_ROOT"]."/view/basic_page_easyui.php";
?> 