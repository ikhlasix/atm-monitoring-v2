<?php

// Include Classes and Basic Settings
include $_SERVER['DOCUMENT_ROOT']."/s/config.php";

// Get Label
$language->get_lang("global");
$language->get_lang("examples");




// ===================================================================================================================
// CRUD Setting Section: Start 
// ===================================================================================================================

// Include Process Class
include "m/crud_with_selectlists.php";
$process = new crud_with_selectlists( $db, $page, $form, $session, $log, $date, $str_label );

// Page Settings
$process->do_debug = true;
$config['script'] = "/modules/examples/crud_with_selectlists";
$config['page_title'] = "CRUD with selectlists";
$config['tabs'] = array();

// Table Settings
$config['table'] = "exp_crud_select";
$config['primary_key'] = "frb_id";
$config['primary_key2'] = "";
$config['primary_key3'] = "";
$config['primary_key_prefix'] = "FRB";

// Data Tables Settings
$config['data_tables_columns'] = array("frb_id","option1","option2","option3");
$config['data_tables_columns_label'] = array("FRB ID","Option 1","Option 2","Option 3");
$config['data_tables_default_sort'] = "frb_id";
$config['data_tables_default_order'] = "asc";
$config['data_tables_default_sort_column'] = array_search ($config['data_tables_default_sort'], $config['data_tables_columns']); // No need to edit this

function form_settings( $values, $form, $page, $str_label ) {
	
	global $db;
	
	foreach ($values as $k => $v) $$k = $v;
	
	// Disable inputs for View
	$input_disabled = "";
	if ($page->get_operation("act","view")) {
		$input_disabled = "disabled";
	}

// Form Settings
$settings = 
	array(
		array(
			"field_name" => "Standard Selectlist",
			"field_variable" => "option1",
			"field_value" => @$option1,
			"input_type" => "text", 				// text/password/email/date/multidate/numeric/currency/file/masked
			"validation" => "mandatory",			// mandatory,valid_email,valid_number,valid_date
			"custom_input_type" => $form->select_list("option1","exp_option_b","opb_id","name",@$option1," class=\"form-control\" $input_disabled ","",""),
			"disabled" => $input_disabled,
		),
		array(
			"field_name" => "Searchable Selectlist",
			"field_variable" => "option2",
			"field_value" => @$option2,
			"input_type" => "text", 				
			"validation" => "mandatory",			
			"custom_input_type" => $form->select_list("option2","exp_option_b","opb_id","name",@$option2," class=\"select2 form-control\" $input_disabled ","",""),
			"disabled" => $input_disabled,
		),
		array(
			"field_name" => "Autocomplete",
			"field_variable" => "option3",
			"field_value" => @$option3,
			"input_type" => "autocomplete", 	
			"input_config" => array(
				"mode" => "option_b",				// parameter to pass to /modules/utilities/autocomplete.php
				"text" => $db->GetOne("select name from exp_option_b where opb_id='".@$option3."'"),
			),			
			"validation" => "mandatory",			
			"disabled" => $input_disabled,
		),
		array(
			"field_name" => "Combotree",
			"field_variable" => "option4",
			"field_value" => @$option4,
			"input_type" => "combotree", 	
			"input_config" => array(
				"mode" => "option_a",				// parameter to pass to /modules/utilities/combotree.php
				"text" => $db->GetOne("select name from exp_option_a where opa_id='".@$option4."'"),
			),			
			"validation" => "mandatory",			
			"disabled" => $input_disabled,
		),
		array(
			"field_name" => "Radio",
			"field_variable" => "option5",
			"field_value" => @$option5,
			"input_type" => "text",
			"validation" => "mandatory",
			"disabled" => $input_disabled,
			"custom_input_type" => $form->radio_list("option5","exp_option_b","opb_id","name",@$option5," ","","","yes"), 
		),
		array(
			"field_name" => "Checkbox",
			"field_variable" => "option6",
			"field_value" => @$option6,
			"input_type" => "text",
			"validation" => "mandatory",
			"disabled" => $input_disabled,
			"custom_input_type" => $form->checkbox_list("option6","exp_option_b","opb_id","name",@$option6," ","","","no"), 
		),
		array(
			"field_name" => "Dynamic Selectlist 1",
			"field_variable" => "option7",
			"field_value" => @$option7,
			"input_type" => "text", 				
			"validation" => "mandatory",			
			"custom_input_type" => $form->select_list("option7","exp_option_b","opb_id","name",@$option7," class=\"form-control\" onChange=\"dynamic_selectbox('option7', 'option8', '/modules/utilities/selectbox.php?mode=option_c')\" $input_disabled ","",""),
			"disabled" => $input_disabled,
		),
		array(
			"field_name" => "Dynamic Selectlist 2",
			"field_variable" => "option8",
			"field_value" => @$option8,
			"input_type" => "text", 				
			"validation" => "mandatory",			
			"custom_input_type" => $form->select_list("option8","exp_option_c","opc_id","name",@$option8," class=\"form-control\" $input_disabled "," where opb_id='".@$option7."' ",""),
			"disabled" => $input_disabled,
		)
		
	);
	return $settings;
}

// ===================================================================================================================
// CRUD Setting Section: End 
// ===================================================================================================================








// Check Access Privilege
if (!$page->get_function_access("read",$config['script'])) {
	$template->box($str_label["LBL_ILLEGAL_ACCESS"], $str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"], array(array("back","Back")), "warning");
	die();
}

// Logged in user Details
$user_detail = $session->get_user_details_session($session->get_session_id());
$config['user_detail'] = $user_detail;

// Get all encrypted parameters
$page->get_parameter ( $user_detail->challenge, $str_label );

// Debug Requests
if ($page->get_operation("act","add") || $page->get_operation("act","update")) $page->debug($_GET);
if ($page->get_operation("act","do_add") || $page->get_operation("act","do_update")) $page->debug($_POST);
if ($page->get_operation("act","do_add") || $page->get_operation("act","do_update")) $page->debug($_FILES);

// Process Tabs
$tabs = $page->generate_tabs( $config );


// ===================================================
// Process Delete Data
// ===================================================
if ($page->get_operation("act","delete")) {
	
	// Check Access Privileges
	if (!$page->get_function_access("delete",$config['script'])) {
		$template->box($str_label["LBL_ILLEGAL_ACCESS"], $str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"], array(array("back","Back")), "warning");
		die();
	}
	
	// Delete Process
	$process->delete( $config );

}
// ===================================================
// Process Do Add/Do Update Data
// ===================================================
elseif ($page->get_operation("act","do_add") || $page->get_operation("act","do_update")) {

	if(!empty($_POST[$config['primary_key']])) $act="do_update";
	
	// Check Access Privileges
	if ( 	(!$page->get_function_access("add",$config['script']) && $page->get_operation("act","do_add")) || 
			(!$page->get_function_access("edit",$config['script']) && $page->get_operation("act","do_update"))
		) {
		$template->box($str_label["LBL_ILLEGAL_ACCESS"], $str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"], "", "warning");
		die();
	}
	
	// Prepare fields
	$values = array();
	$config["form_fields"] = form_settings( $values, $form, $page, $str_label );
	
	//Form Validation
	$process->validate( $config );
	
	// Save Form
	$process->set_data( $config );	
	
	die();

}
// ===================================================
// Process Get List
// ===================================================
elseif ($page->get_operation("act","list")) {
	
	// Check Access Privileges
	$edit_privilege = false;
	$delete_privilege = false;
	if ($page->get_function_access("edit",$config['script'])) $config['edit_privilege'] = true;
	if ($page->get_function_access("delete",$config['script'])) $config['delete_privilege'] = true;  
	
	// Process Get List
	$process->get_list( $config );
	
	die();

}
// ===================================================
// Process Add/Update Form
// ===================================================
else if ($page->get_operation("act","add") || $page->get_operation("act","update")  || $page->get_operation("act","view")) {

	// Check Access Privilege
	if ( 	(!$page->get_function_access("add",$config['script']) && $page->get_operation("act","add")) || 
			(!$page->get_function_access("edit",$config['script']) && $page->get_operation("act","update"))
		) {
		$template->box($str_label["LBL_ILLEGAL_ACCESS"], $str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"], array(array("back","Back")), "warning");
		die();
	}
	    
	// Process Get Data
	$result = $process->get_data( $config );
	$values = array();
	foreach ($result as $key => $value) {
		$values[$key] = $value;
	}
	
	// Prepare Form Setting 
	$config["form_fields"] = form_settings( $values, $form, $page, $str_label );
	
	// Generate Form
	$main_content = $process->generate_form ( $config );
	
}
// ===================================================
// Process List View
// ===================================================
else {

	$main_content = $process->generate_list ( $config );

}

//Page Template
include $_SERVER["DOCUMENT_ROOT"]."/view/basic_page_easyui.php";
?> 