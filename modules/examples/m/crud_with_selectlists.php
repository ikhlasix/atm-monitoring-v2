<?php

class crud_with_selectlists {
    
    private $page;
    private $form;
    private $session;
    private $log;
    private $str_label;
    private $date;
    private $db;
    
    public function __construct( $db, classes\application\page $page, classes\application\form $form, classes\application\session $session, classes\application\log $log, classes\application\date $date, $str_label ) {
        $this->page = $page;
        $this->form = $form;
        $this->session = $session;
        $this->log = $log;
        $this->str_label = $str_label;
        $this->date = $date;
        $this->db = $db;
        
        // Set Debug
        $this->page->do_debug = 0;
    }
    
    public function get_list( $config ) {
        
        // Sorting Setting
        $_order = $config['data_tables_columns'][$_GET['order'][0]['column']];
        $_sort = $_GET['order'][0]['dir'];
        $result = array();
        
        // Condition Setting
        $cond = " and isnull(a.is_delete,0)!='1' ";
        if (isset($_GET[$config['primary_key2']])) $cond .= " and ".$config['primary_key2']."='".$_GET[$config['primary_key2']]."' ";
        if (isset($_GET[$config['primary_key3']])) $cond .= " and ".$config['primary_key3']."='".$_GET[$config['primary_key3']]."' ";
        
        // Search Setting
        $_query = $_GET['search']['value'];
        $cond_search = "";
        if ($_query) {
            /*foreach ($config['data_tables_columns'] as $key => $value) {
                $cond_search .= " $value like '%$_query%' or ";
            }
            $cond_search = " and (".substr($cond_search,0,-4).") ";*/
            
            $cond_search = " and (frb_id like '%$_query%' or b.name like '%$_query%' or c.name like '%$_query%' or d.name like '%$_query%') ";
        }
        
        //Get Total Records
        $sql="select count(*) from ".$config['table']." where 1=1 $cond";
        $this->page->debug($sql);
        $records_total=$this->db->GetOne($sql);

        //Get Data
        $sql="select a.frb_id, b.name as option1, c.name as option2, d.name as option3 from ".$config['table']." a 
        left join exp_option_b b on a.option1=b.opb_id 
        left join exp_option_b c on a.option2=c.opb_id 
        left join exp_option_b d on a.option3=d.opb_id 
        where 1=1 $cond $cond_search order by $_order $_sort";
        $this->page->debug($sql);
        $res_select=$this->db->Execute($sql);
        $records_filtered = $res_select->RecordCount();
        
        $res_select=$this->db->SelectLimit($sql,$_GET['length'],$_GET['start']);
        if (!$res_select) $this->page->debug_query(1);
        while($row_select=$res_select->FetchRow()) {
            if (is_array($row_select)){ foreach($row_select as $key=>$val) {
                $key=strtolower($key);
                
                // Format Date 
                if (preg_match("/^datetime_/", $key)) {
                    $val = $this->date->datetime_to_display($val);
                }
                else if (preg_match("/^date_/", $key)) {
                    $val = $this->date->datetime_to_display($val);
                }
                
                $$key=$val;
            }}
            
            $parameter_array = array();
            if ($config['primary_key']) $parameter_array[$config['primary_key']] = $$config['primary_key'];
            if ($config['primary_key2']) $parameter_array[$config['primary_key2']] = $$config['primary_key2'];
            if ($config['primary_key3']) $parameter_array[$config['primary_key3']] = $$config['primary_key3'];
            
            $parameter_update = $this->page->set_parameter ( array_merge($parameter_array,array("act"=>"update")), $config['user_detail']->challenge );
            $parameter_view = $this->page->set_parameter ( array_merge($parameter_array,array("act"=>"view")), $config['user_detail']->challenge );
            $parameter_delete = $this->page->set_parameter ( array_merge($parameter_array,array("act"=>"delete")), $config['user_detail']->challenge );
            
            $column_data = array();
            foreach ($config['data_tables_columns'] as $key2 => $value2) {
                $column_data[] = $$value2;
            }
            
            $links = "";
            // Edit or View link
            if ($this->page->get_function_access("edit",$config['script'])) $links .=  "<button type=\"button\" class=\"btn btn-primary btn-xs button-edit\" onClick=\"window.location.href='".$config['script']."/p/".$parameter_update."/';\"><i class=\"glyphicon glyphicon-edit\"></i> ".$this->str_label["LBL_EDIT"]."</button> ";
            else $links .=  "<button type=\"button\" class=\"btn btn-primary btn-xs button-view\" onClick=\"window.location.href='".$config['script']."/p/".$parameter_view."/';\"><i class=\"glyphicon glyphicon-edit\"></i> ".$this->str_label["LBL_VIEW"]."</button> ";
            
            // Delete link            
            if ($this->page->get_function_access("delete",$config['script'])) $links .=  "<button type=\"button\" class=\"btn btn-primary btn-xs button-delete\" onClick=\"if (confirm('".$this->str_label["MSG_ARE_YOU_SURE_TO_DELETE_THIS_DATA"]."')) window.location.href='".$config['script']."/p/".$parameter_delete."/';\"><i class=\"glyphicon glyphicon-remove\"></i> ".$this->str_label["LBL_DELETE"]."</button>";
            
            
            array_push($column_data, $links);
            
            $result[] = $column_data;
            
        }
        
        $output = array("draw"=>$_GET['draw'], "recordsTotal"=>$records_total, "recordsFiltered"=>$records_filtered, "data"=>$result);
        
        // Format to json
        echo json_encode($output);        
        
    }     
    
    
    
    public function get_data ( $config ) {
        
        $result = array();
        
        if(!empty($_GET[$config['primary_key']])){
		
            $cond = "";
            if (isset($_GET[$config['primary_key']])) $cond .= $config['primary_key']."='".$_GET[$config['primary_key']]."' and ";
            if (isset($_GET[$config['primary_key2']])) $cond .= $config['primary_key2']."='".$_GET[$config['primary_key2']]."' and ";
            if (isset($_GET[$config['primary_key3']])) $cond .= $config['primary_key3']."='".$_GET[$config['primary_key3']]."' and ";
            $cond = substr($cond,0,-5);
            
            $sql="select * from ".$config['table']." where $cond ";
            $this->page->debug($sql);
            $res_data=$this->db->Execute($sql);
            if(!$res_data) $this->page->debug_query(1);
            $row=$res_data->FetchRow();
            foreach($row as $key=>$val){
                $key=strtolower($key);
                
                // Format Date 
                if (preg_match("/^datetime_/", $key)) {
                    $val = $this->date->datetime_to_display($val);
                }
                else if (preg_match("/^date_/", $key)) {
                    $val = $this->date->datetime_to_display($val);
                }
                
                $result[$key]=$val;
            }
            
            // Reset password and verify variable
            $form_variable_arr = array("verify","password");
            foreach ($form_variable_arr as $key => $value) {
                $result[$value] = "";
            }
        }
        
        return $result;
        
    }
    
    
    public function delete( $config ) {
        
        $cond = "";
        if (isset($_GET[$config['primary_key']])) $cond .= $config['primary_key']."='".$_GET[$config['primary_key']]."' and ";
        if (isset($_GET[$config['primary_key2']])) $cond .= $config['primary_key2']."='".$_GET[$config['primary_key2']]."' and ";
        if (isset($_GET[$config['primary_key3']])) $cond .= $config['primary_key3']."='".$_GET[$config['primary_key3']]."' and ";
        $cond = substr($cond,0,-5);
        
        $list = "";
        $list .= "is_delete='1',";
        $list .= "d_time=".$this->date->datetime_to_db(date("Y-m-d H:i:s")).",";
        $list .= "d_user='".$config['user_detail']->username."',";
        $list = preg_replace("/,$/","",$list);
        
        $sql="update ".$config['table']." set $list where $cond";
        $this->page->debug($sql);
        $res_del=$this->db->Execute($sql);
        if (!$res_del) $this->page->debug_query(1);
        
        $this->log->insert_log("DELETE ".$config['table']." ".$config['primary_key'].": ".$_GET[$config['primary_key']]);
        
        // Redirecting page
        $parameter_array = array();
        if ($config['primary_key2']) $parameter_array[$config['primary_key2']] = $_GET[$config['primary_key2']];
        if ($config['primary_key3']) $parameter_array[$config['primary_key3']] = $_GET[$config['primary_key3']];
        
        if (count($parameter_array)>0) $parameter_delete = $this->page->set_parameter ( $parameter_array, $config['user_detail']->challenge );
        else $parameter_delete = "0";
            
        $this->page->redirect(1 ,$config['script']."/p/$parameter_delete/" ,"Data Successfully Deleted");
        
        die();
        
    }
    
    
    public function validate( $config ) {
        
        $error = "";
        $error = $this->form->validate( $config );
        
        // Additional Validation
        /*if ($this->page->get_operation("act","do_add")) {
            if ($_POST['password']!=$_POST['verify']) $error .="<li>Password is not verified";
            if (strlen($_POST['password']) < 8) $error .="<li>Password is too short";
            if (!preg_match("#[0-9]+#", $_POST['password'])) $error .="<li>Password must include at least one number";
            if (!preg_match("#[a-zA-Z]+#", $_POST['password'])) $error .="<li>Password must include at least one letter";
        }*/
        
        // Return Error
        if($error){
            echo '{ "status": "error", "message": "<b>Invalid Input</b><hr><ul>'.$error.'</ul>" }';
            die();
        }
        
    }
    
    
    public function set_data( $config ) {
        
        $columns = "";
        $values = "";
        $list = "";
        $cond = "";
        
        $act = $_POST['act'];
        
        // Prepare for exceptions
        $upload_variables = array();
        $autocomplete_variables = array();
        foreach ($config["form_fields"] as $key => $value) {
            if ($value['input_type']=="file") $upload_variables[] = $value['field_variable'];
            if ($value['input_type']=="autocomplete") $autocomplete_variables[] = $value['field_variable'];
        }
        
        
        $exceptions = "|";
        
        // Exceptions for processing POST for wysiwyg editor
        $exceptions .= "_wysihtml5_mode|";
        
        // Exceptions for autocomplete search value
        foreach ($autocomplete_variables as $key => $value) {
            $exceptions .= $value."_autocomplete|";
        }
        
        // Exceptions for processing POST for file input type
        foreach ($upload_variables as $key => $value) {
            $exceptions .= $value."_id|";
            $exceptions .= $value."_process|";
            $exceptions .= $value."_upload|";
        }
        
        $exceptions = substr($exceptions,0,-1);
        
        $this->page->debug("exceptions:$exceptions");
        
        $pk1_name = "";
        $pk1_value = "";
        $pk2_name = "";
        $pk2_value = "";
        $pk3_name = "";
        $pk3_value = "";

        foreach($_POST as $key=>$val){

            if($act=='do_add' && !preg_match("/^(act".$exceptions.")$/i",$key)){
                
                if (preg_match("/^datetime_/", $key)) {
                    $val = $this->date->display_to_datetime($val);
                    $val = $this->date->datetime_to_db($val);
                    $columns .="$key,";
                    $values .="$val,";
                }
                else if (preg_match("/^date_/", $key)) {
                    $val = $this->date->display_to_datetime($val);
                    $val = $this->date->datetime_to_db($val);
                    $columns .="$key,";
                    $values .="$val,";
                }
                else if ($key==$config['primary_key']) {
                    $this->page->debug($key."=".$val);
                    $val = $this->form->generate_row_id($config['table'],$config['primary_key'],$config['primary_key_prefix']);
                    $_POST[$config['primary_key']] = $val;
                    $columns .="$key,";
                    $values .="'$val',";
                    $pk1_name = $key;
                    $pk1_value = $val;
                }
                else if ($key==$config['primary_key2']) {
                    $columns .="$key,";
                    $values .="'$val',";
                    $pk2_name = $key;
                    $pk2_value = $val;
                }
                else if ($key==$config['primary_key3']) {
                    $columns .="$key,";
                    $values .="'$val',";
                    $pk3_name = $key;
                    $pk3_value = $val;
                }
                else if (is_array($val)) {
                    $new_val = "";
                    foreach ($val as $k => $v) {
                        $new_val .= $v."|";
                    }
                    $val = substr($new_val,0,-1);
                    $columns .="$key,";
                    $values .="'$val',";
                }
                else if (is_array($val)) {
                    $new_val = "";
                    foreach ($val as $k => $v) {
                        $new_val .= $v."|";
                    }
                    $val = substr($new_val,0,-1);
                    $columns .="$key,";
                    $values .="'$val',";
                }
                else {
                    $columns .="$key,";
                    $values .="'$val',";
                }
            }elseif($act=='do_update' && !preg_match("/^(act".$exceptions.")$/i",$key)){
                
                if (preg_match("/^datetime_/", $key)) {
                    $val = $this->date->display_to_datetime($val);
                    $val = $this->date->datetime_to_db($val);
                    $list .="$key=$val,";
                }
                else if (preg_match("/^date_/", $key)) {
                    $val = $this->date->display_to_datetime($val);
                    $val = $this->date->datetime_to_db($val);
                    $list .="$key=$val,";
                }
                else if($key==$config['primary_key']){
                    $cond .= "$key='$val' and ";
                    $pk1_name = $key;
                    $pk1_value = $val;
                }
                else if($key==$config['primary_key2']){
                    $cond .= "$key='$val' and ";
                    $pk2_name = $key;
                    $pk2_value = $val;
                }
                else if($key==$config['primary_key3']){
                    $cond .= "$key='$val' and ";
                    $pk3_name = $key;
                    $pk3_value = $val;
                }
                else if (is_array($val)) {
                    $new_val = "";
                    foreach ($val as $k => $v) {
                        $new_val .= $v."|";
                    }
                    $val = substr($new_val,0,-1);
                    $list .="$key='$val',";
                }
                else{
                    $list .="$key='$val',";
                }
            }
            
            $$key = $val;
        }

        // Additional Columns
        if ($act == 'do_add') {
            
            $columns .= "c_time,";
            $values .= $this->date->datetime_to_db(date("Y-m-d H:i:s")).",";
            
            $columns .= "c_user,";
            $values .= "'".$config['user_detail']->username."',"; 
        }
        else if ($act == 'do_update') {
            
            $list .= "m_time=".$this->date->datetime_to_db(date("Y-m-d H:i:s")).",";
            $list .= "m_user='".$config['user_detail']->username."',";
        }
        
        $columns = preg_replace("/,$/","",$columns);
        $values	 = preg_replace("/,$/","",$values);
        $list	 = preg_replace("/,$/","",$list);
        $cond 	 = substr($cond,0,-5);
        
        // Process file uploads
        $upload_config = array();
        $upload_config['table'] = $config['table'];
        $upload_config['pk1_name'] = $pk1_name;
        $upload_config['pk1_value'] = $pk1_value;
        $upload_config['pk2_name'] = $pk2_name;
        $upload_config['pk2_value'] = $pk2_value;
        $upload_config['pk3_name'] = $pk3_name;
        $upload_config['pk3_value'] = $pk3_value;
        $upload_config['user_detail'] = $config['user_detail'];
        $upload_config['form_fields'] = $config["form_fields"];
        
        $upload_error = $this->form->upload_process( $upload_config, $this->str_label );
        if ($upload_error) {
            echo '{ "status": "error", "message": "'.$upload_error.'" }';
            die();
        }
        
        if($act=="do_update"){
            $sql="update ".$config['table']." set $list where $cond";
            $this->page->debug($sql);
            $result=$this->db->Execute($sql);
            if (!$result){
                $this->page->debug_query(1);
            }
            $this->log->insert_log("UPDATE ".$config['table']." ".$config['primary_key'].": ".$_POST[$config['primary_key']]);
            
        }else{

            $sql="insert into ".$config['table']." ($columns) values ($values)";
            $this->page->debug($sql);
            $result=$this->db->Execute($sql);
            if (!$result){
                $this->page->debug_query(1);
            }	
            $this->log->insert_log("INSERT ".$config['table']." ".$config['primary_key'].": ".$_POST[$config['primary_key']]);
        }
        
        echo '{ "status": "success", "message": "Data has been successfully saved" }';
        
    }
    
    
    public function generate_form ( $config ) {
        
        $output = "";
        foreach ($config["form_fields"] as $key => $form_group) {
            
            $input_disabled = $form_group["disabled"];
            
            $mandatory_sign = "";
            if (preg_match("/mandatory/i",$form_group["validation"])) {
                $mandatory_sign = "*";
            }
            
            $input = "";
            if (isset($form_group["custom_input_type"])) {
                $input = $form_group["custom_input_type"];
            }
            else {
                $input = $this->form->generate_standard_input ( $form_group, $input_disabled, $config, $this->str_label );
            }
            
            if (isset($form_group["notification"])) {
                $input .= $form_group["notification"];
            }
            
            $output .= "
            
            <div class=\"form-group\">
                <label class=\"control-label col-sm-2\" for=\"".$form_group["field_variable"]."\">".$form_group["field_name"]." ".$mandatory_sign."</label>
                <div class=\"col-lg-6 col-md-8 col-sm-10\">
                ".$input."                
                </div>
            </div>
            
            ";
            
            // Button Label
            $button_label = (($this->page->get_operation("act","add"))?$this->str_label["LBL_ADD"]:$this->str_label["LBL_EDIT"]);
            
            $submit_button = "";
            if ($this->page->get_operation("act","add") || $this->page->get_operation("act","update"))
                $submit_button = "<input type=\"submit\" class=\"btn btn-primary button-submit\" value=\"".$button_label."\" style=\"width:100px;\" />";
            
            // Set variable inside main content
            $hidden_pk1_var = "";
            $hidden_pk2_var = "";
            $hidden_pk3_var = "";
            $notification_password = "";
            if ($config['primary_key']) $hidden_pk1_var = "<input type=\"hidden\" name=\"".$config['primary_key']."\" value=\"".@$_GET[$config['primary_key']]."\">";
            if ($config['primary_key2']) $hidden_pk2_var = "<input type=\"hidden\" name=\"".$config['primary_key2']."\" value=\"".@$_GET[$config['primary_key2']]."\">";
            if ($config['primary_key3']) $hidden_pk3_var = "<input type=\"hidden\" name=\"".$config['primary_key3']."\" value=\"".@$_GET[$config['primary_key3']]."\">"; 
            
            $_act=(!empty($_GET[$config['primary_key']]))?"do_update":"do_add";
            

$main_content = <<<EOD
            <form id="f1"  class="form-horizontal" role="form" method="post" action="{$config['script']}/p/0/" enctype="multipart/form-data">	
            <input type="hidden" name="act" value="$_act">
            {$hidden_pk1_var}
            {$hidden_pk2_var}
            {$hidden_pk3_var}
            
            {$output}
            
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                <button type="button" class="btn btn-primary button-back" onClick="window.location.href='{$config['script']}/p/0/'; return false" style="width:100px"><i class="fa fa-arrow-left" style="margin-right:10px"></i> {$this->str_label["LBL_BACK"]}</button>
                {$submit_button}
                </div>
            </div>
            </form>
            {$this->str_label["MSG_FIELDS_MANDATORY"]}
EOD;
            
        }
        
        return $main_content;
        
    }
    
    
    
    public function generate_list( $config ) {
        
        global $add_button;
        
        // Check Button Privileges
        $add_button = "";
        if ($this->page->get_function_access("add",$config['script'])) {
            
            $parameter_array = array();
            if (isset($_GET[$config['primary_key2']])) $parameter_array[$config['primary_key2']] =  $_GET[$config['primary_key2']];
            if (isset($_GET[$config['primary_key3']])) $parameter_array[$config['primary_key3']] =  $_GET[$config['primary_key3']];
            
            $parameter_add = $this->page->set_parameter ( array_merge($parameter_array,array("act"=>"add")), $config['user_detail']->challenge );
            $add_button = "<button type=\"button\" class=\"btn btn-primary btn-xs button-add\" onClick=\"window.location.href='".$config['script']."/p/".$parameter_add."/';\"><i class=\"fa fa-plus\"></i> ".$this->str_label["LBL_ADD"]."</button>";
        }           

        $column_label = "";
        foreach ($config['data_tables_columns_label'] as $key => $value) {
            $column_label .= "<th>".$value."</th>"; 
        }

$main_content = <<<EOD
	<table class="table table-striped table-hover" id="dataTables">
		<thead>
			<tr>
				{$column_label}
				<th width="200">Functions</th>
			</tr>
		</thead>
	</table>
EOD;

    return $main_content;
        
    }
   
}

?>