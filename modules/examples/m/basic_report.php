<?php

class basic_report {
    
    private $page;
    private $form;
    private $session;
    private $log;
    private $str_label;
    private $date;
    private $db;
    
    public function __construct( $db, classes\application\page $page, classes\application\form $form, classes\application\session $session, classes\application\log $log, classes\application\date $date, $str_label ) {
        $this->page = $page;
        $this->form = $form;
        $this->session = $session;
        $this->log = $log;
        $this->str_label = $str_label;
        $this->date = $date;
        $this->db = $db;
        
        // Set Debug
        $this->page->do_debug = 0;
    }
    
    public function get_list( $config ) {
        
        $cond = "";
        if ($_POST['name']) {
            $cond .= " and name like '%".$_POST['name']."%'";
        }
        
        $data = array();
        
        $sql="select a.*, ".$this->date->datetime_from_db('a.date_number')." from exp_report a where 1=1 $cond order by rep_id asc";
        $this->page->debug($sql);
        // $res_select=$this->db->Execute($sql);
        $res_select=$this->db->SelectLimit($sql,500,0);
        if (!$res_select) $this->page->debug_query(1);
        while($row_select=$res_select->FetchRow()) {
           if (is_array($row_select)){ foreach($row_select as $key=>$val) {
               $key=strtolower($key);
               $$key=$val;
           }}
           
           $data[] = array(
               "id" => $rep_id,
               "name" => $name,
               "data1" => $number1,
               "data2" => $number2,
               "data3" => $number3,
               "data4" => $number4,
               "data5" => $number5,
               "data6" => $number6,
               "data7" => $number7,
               "date" => $date_number
           );
        
        }
        
        return $data;
        
    }    
    
    
    
    public function generate_report( $config, $data ) {
        
        include $_SERVER['DOCUMENT_ROOT'].'/components/php-report/PHPReport.php';

        $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
        $rendererLibrary = 'mpdf';
        $rendererLibraryPath = $_SERVER['DOCUMENT_ROOT'].'/components/' . $rendererLibrary;

        if (!PHPExcel_Settings::setPdfRenderer($rendererName,$rendererLibraryPath)) {
            die('NOTICE: Please set the $rendererName and $rendererLibraryPath values' . EOL .
                'at the top of this script as appropriate for your directory structure');
        }

        //which template to use
        $template='adv_report.xlsx';
        // $template='basic_report.xlsx';
            
        //set absolute path to directory with template files
        $templateDir=$_SERVER['DOCUMENT_ROOT'].'/template/reports/';

        //set config for report
        $config=array(
            'template'=>$template,
            'templateDir'=>$templateDir
        );
        
        
        // Format Setting
        $format = array();
        if ($_POST['output']!='excel') $format['data1'] = array('number'=>array('decimals'=>0));
        if ($_POST['output']!='excel') $format['data2'] = array('number'=>array('decimals'=>0));
        if ($_POST['output']!='excel') $format['data3'] = array('number'=>array('decimals'=>0));
        if ($_POST['output']!='excel') $format['data4'] = array('number'=>array('decimals'=>0));
        if ($_POST['output']!='excel') $format['data5'] = array('number'=>array('decimals'=>0));
        if ($_POST['output']!='excel') $format['data6'] = array('number'=>array('decimals'=>0));
        if ($_POST['output']!='excel') $format['data7'] = array('number'=>array('decimals'=>0));
        
        if ($_POST['output']!='excel') $format['date'] = array('datetime'=>'d/m/Y');

        $R=new PHPReport($config);

        $R->objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $R->objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

        $R->load(array(
                    array(
                        'id' => 'data',
                        'config' => $config,
                        'repeat' => true,
                        'data' => $data,
                        'minRows' => 1,
                        'format' => $format
                        ),
                    )
                );
                
        //we can render html, excel, excel2003 or pdf
        echo $R->render($_POST['output']);
        exit();
        
    } 
    
    
    
    public function get_data ( $config ) {
        
        $result = array();
        
        if(!empty($_GET[$config['primary_key']])){
		
            $cond = "";
            if (isset($_GET[$config['primary_key']])) $cond .= $config['primary_key']."='".$_GET[$config['primary_key']]."' and ";
            if (isset($_GET[$config['primary_key2']])) $cond .= $config['primary_key2']."='".$_GET[$config['primary_key2']]."' and ";
            if (isset($_GET[$config['primary_key3']])) $cond .= $config['primary_key3']."='".$_GET[$config['primary_key3']]."' and ";
            $cond = substr($cond,0,-5);
            
            $sql="select * from ".$config['table']." where $cond ";
            $this->page->debug($sql);
            $res_data=$this->db->Execute($sql);
            if(!$res_data) $this->page->debug_query(1);
            $row=$res_data->FetchRow();
            foreach($row as $key=>$val){
                $key=strtolower($key);
                
                // Format Date 
                if (preg_match("/^datetime_/", $key)) {
                    $val = $this->date->datetime_to_display($val);
                }
                else if (preg_match("/^date_/", $key)) {
                    $val = $this->date->datetime_to_display($val);
                }
                
                $result[$key]=$val;
            }
            
            // Reset password and verify variable
            $form_variable_arr = array("verify","password");
            foreach ($form_variable_arr as $key => $value) {
                $result[$value] = "";
            }
        }
        
        return $result;
        
    }
    
    
    
    public function validate( $config ) {
        
        $error = "";
        $error = $this->form->validate( $config );
        
        // Additional Validation
        /*if ($this->page->get_operation("act","do_add")) {
            if ($_POST['password']!=$_POST['verify']) $error .="<li>Password is not verified";
            if (strlen($_POST['password']) < 8) $error .="<li>Password is too short";
            if (!preg_match("#[0-9]+#", $_POST['password'])) $error .="<li>Password must include at least one number";
            if (!preg_match("#[a-zA-Z]+#", $_POST['password'])) $error .="<li>Password must include at least one letter";
        }*/
        
        // Return Error
        if($error){
            echo "<link href=\"/assets/css/style.min.css\" rel=\"stylesheet\" />";
            echo $error;
            die();
        }
        
    }
    
    
    
    public function generate_form ( $config ) {
        
        $output = "";
        foreach ($config["form_fields"] as $key => $form_group) {
            
            $input_disabled = $form_group["disabled"];
            
            $mandatory_sign = "";
            if (preg_match("/mandatory/i",$form_group["validation"])) {
                $mandatory_sign = "*";
            }
            
            $input = "";
            if (isset($form_group["custom_input_type"])) {
                $input = $form_group["custom_input_type"];
            }
            else {
                $input = $this->form->generate_standard_input ( $form_group, $input_disabled, $config, $this->str_label );
            }
            
            if (isset($form_group["notification"])) {
                $input .= $form_group["notification"];
            }
            
            $output .= "
            
            <div class=\"form-group\">
                <label class=\"control-label col-sm-2\" for=\"".$form_group["field_variable"]."\">".$form_group["field_name"]." ".$mandatory_sign."</label>
                <div class=\"col-lg-6 col-md-8 col-sm-10\">
                ".$input."                
                </div>
            </div>
            
            ";
            
            $submit_button = "
            <input type=\"submit\" class=\"btn btn-primary button-submit\" value=\"Submit\" style=\"width:100px;\" onClick=\"submit_form();return false;\">
            ";
            

$main_content = <<<EOD
            <form id="f1"  class="form-horizontal" role="form" method="post" action="{$config['script']}/p/0/" target="submit-frame" enctype="multipart/form-data">	
            <input type="hidden" name="act" value="report">
            
            {$output}
            
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                {$submit_button}
                </div>
            </div>
            </form>
            {$this->str_label["MSG_FIELDS_MANDATORY"]}
EOD;
            
        }
        
        return $main_content;
        
    }
    
}

?>