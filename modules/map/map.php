<?php

// Include Classes and Basic Settings
include $_SERVER['DOCUMENT_ROOT']."/s/config.php";

// Get Label
$language->get_lang("global");
$language->get_lang("examples");

include "m/map.php";
$process = new map ( $db, $page, $form, $session, $log, $date, $str_label );

// Page Settings
$process->do_debug = true;
$config['script'] = "/modules/map/map";
$config['page_title'] = "Maps";
$config['tabs'] = array();

// Table Settings
$config['table'] = "app_alert";
$config['primary_key'] = "alt_id";
$config['primary_key2'] = "";
$config['primary_key3'] = "";
$config['primary_key_prefix'] = "ALT";

// Process Get Data
// $result = $process->get_data( $config );
// $values = array();
// foreach ($result as $key => $value) {
// 	$values[$key] = $value;
// }


include $_SERVER["DOCUMENT_ROOT"]."/view/map.php";
?>