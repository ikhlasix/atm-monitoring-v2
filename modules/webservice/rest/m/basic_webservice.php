<?php

class basic_webservice {
    
    private $page;
    private $form;
    private $session;
    private $log;
    private $str_label;
    private $date;
    private $db;
    
    public function __construct( $db, classes\application\page $page, classes\application\form $form, classes\application\session $session, classes\application\log $log, classes\application\date $date, $str_label ) {
        $this->page = $page;
        $this->form = $form;
        $this->session = $session;
        $this->log = $log;
        $this->str_label = $str_label;
        $this->date = $date;
        $this->db = $db;
        
        // Set Debug
        $this->page->do_debug = 0;
    }
    
    
    
    /**
    * Deliver HTTP Response
    *
    * @param string $format The desired HTTP response content type: [json, html, xml]
    *
    * @param string $api_response The desired HTTP response data
    *
    * @return void
    *
    **/
    
    public function deliver_response($format, $api_response, $client_name){
        
        include($_SERVER['DOCUMENT_ROOT']."/components/array2xml/array2xml.php");
        $xml = new Array2XML;
    
        // Define HTTP responses
        $http_response_code = array(
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method not allowed',
            406 => 'Not Acceptable',
        );
    
        // Set HTTP Response
        header('HTTP/1.1 '.$api_response['status'].' '.$http_response_code[ $api_response['status'] ]);
    
        // Process different content types
        if( strcasecmp($format,'json') == 0 ){
    
            // Set HTTP Response Content Type
            header('Content-Type: application/json; charset=utf-8');
    
            // Format data into a JSON response
            $response = json_encode($api_response);
    
            // Deliver formatted data
            echo $response;
    
        }elseif( strcasecmp($format,'xml') == 0 ){
            
            // Set HTTP Response Content Type
            header('Content-Type: application/xml; charset=utf-8');
            
            $xml_data = array(
                                "code"=>$api_response['code'],
                                "data"=>array("record"=>$api_response['data'])
                        );
            $xml = Array2XML::createXML('response', $xml_data);
		    $response = $xml->saveXML();
    
            // Deliver formatted data
            echo $response;
    
        }else{
    
            // Set HTTP Response Content Type (This is only good at handling string data, not arrays)
            header('Content-Type: text/html; charset=utf-8');
    
            $response = $api_response['data'];
    
            // Deliver formatted data
            echo $response;
    
        }
        
        // --- Log All Request
        $wsl_id		=$this->form->generate_row_id("WSV_LOG","wsl_id","WSL");
        $sql="insert into wsv_log values ('$wsl_id','".$_SERVER['REQUEST_METHOD']."','".$_SERVER['REQUEST_URI']."','".date('Y-m-d H:i:s')."','".$this->get_user_ip()."','$client_name','".$api_response['status']."','".$response."')";
        $this->page->debug($sql);
        $res_log=$this->db->Execute($sql);
        if (!$res_log) $this->page->debug_query(1);

    
        // End script process
        exit;
    
    }
    
    
    
    
    /**
    * Get User IP Address
    *
    * @param void
    *
    * @return string user IP address
    *
    **/
    
    public function get_user_ip() {
        if( array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER) && !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
            if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',')>0) {
                $addr = explode(",",$_SERVER['HTTP_X_FORWARDED_FOR']);
                return trim($addr[0]);
            } else {
                return $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
        }
        else {
            return $_SERVER['REMOTE_ADDR'];
        }
    }  
    
    
    
    
    public function auth_webservice_client ( $pin ) {
        
        $result = array();
        $sql="select wsc_id, client_name from wsv_client where client_pin='$pin'";
        $this->page->debug($sql);
        $row_client=$this->db->GetRow($sql);
        if (is_array($row_client)){ foreach($row_client as $key=>$val) {
            $key=strtolower($key);
            $$key=$val;
            $result[$key] = $val;
        }}
        
        return $result;
    }
    
    
    
    public function check_access_method ( $wsc_id, $client_name ) {
        
        global $api_response_code;
        
        // --- Check if access is granted for this client
        $sql="select count(*) as accessible from wsv_method_access where wsc_id='$wsc_id' and method='".$_GET['method']."'";
        $this->page->debug($sql);
        $accessible=$this->db->GetOne($sql);
        
        if ($accessible<1) { // Method not Accessible to this client
            $response['code'] = 7;
            $response['status'] = $api_response_code[ $response['code'] ]['HTTP Response'];
            $response['data'] = $api_response_code[ $response['code'] ]['Message'];

            // Return Response to browser
            $this->deliver_response($_GET['format'], $response, $client_name);
        }
    }
    
    
    private function check_access( $session_id ) {
        
        // Check if session id exist in APP_SESSION
        $sql="select username, ip, ".$this->date->datetime_from_db('last_access')." from app_session where session_id='$session_id'";
        $row=$this->db->GetRow($sql);
        if (is_array($row)){ foreach($row as $key=>$val) {
            $key=strtolower($key);
            $$key=$val;
        }}
        
        if (!@$username) {
            
            $session_data = array();
            $session_data['status'] = "fail";
            $session_data['reason'] = "Session not exist";
            return $session_data;
        }
        else {
            
            $session_data = array();
            $session_data['status'] = "success";
            return $session_data;
        }
        
    }
    
    
    
    public function authentication( $str_label ) {
        
        $login_status = true;

        // Get User Detail        
        $user_detail = $this->session->get_user_details_db($_GET['username']);
        if ($user_detail["status"] != '1' && $login_status==true) {
            $login_status = false;
            $reason = $str_label["MSG_CAN_NOT_ACCESS_STATUS_INACTIVE"];
        }
        
        // Get user hashed password
		$hashed_password = $this->session->get_app_password($_GET['username']);
        $sp = "appSpasi2016";
        
        $verify_password = $this->session->app_password_verify ( $_GET['password'], $hashed_password, $sp );
        if ($verify_password == false  && $login_status==true) {
            $login_status = false;
            $reason = $str_label["MSG_NOT_REGISTERED_OR_WRONG_USERNAME_PASSWORD"];
        }
        
        $session_id = $this->session->generate_session_id();
        $this->session->set_session_id( $session_id );
        $this->session->set_session ( $session_id, $_GET['username'], json_encode($user_detail) );
        
        if ($login_status == false) {
            
            $session_data = array();
            $session_data['status'] = "fail";
            $session_data['reason'] = $reason;
            return $session_data;
        }                
        else { 
            
            $session_data = array();
            $session_data['status'] = "success";
            $session_data['session_id'] = $session_id;
            $session_data['user_detail'] = $user_detail;
            return $session_data;
        }
        
    }
    
    
    
    public function get_sample_data() {
        
        // Always get user detail from session ID
        $session_id = @$_GET['session_id'];
        $session_data = $this->check_access( $session_id );
        if ($session_data['status'] == "fail") {
            return $session_data;
            die();
        }
        else if ($session_data['status'] == "success") {
            $user_detail = $this->session->get_user_details_session ( $session_id );    
        }
        
        // --- Method Parameters
        $cond = "";
        if (!empty($_GET['search'])) {
            $rel 	= !empty($cond)?"and":"where";
            $cond .= " $rel name like '%".$_GET['search']."%' ";
        }
        
        $return = array();
        $i = 0;
        
        $sql="select * from exp_option_a $cond";
        $this->page->debug($sql);
        $res_select=$this->db->Execute($sql);
        if (!$res_select) $this->page->debug_query(1);
        while($row_select=$res_select->FetchRow()) {
           if (is_array($row_select)){ foreach($row_select as $key=>$val) {
               $key=strtolower($key);
               $$key=$val;
               $result[$i][$key] = $val;
           }}
           $i++;
        }
        
        return $result;
    }
    
    
    
    
}

?>