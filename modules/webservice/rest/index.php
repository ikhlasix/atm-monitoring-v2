<?php

include($_SERVER['DOCUMENT_ROOT']."/s/config.php");

// Get Label
$language->get_lang("global");

// Include Process Class
include "m/basic_webservice.php";
$process = new basic_webservice( $db, $page, $form, $session, $log, $date, $str_label );

// Page Settings
$page->do_debug = 0;
$config['script'] = "/modules/webservice/rest/";



// --- Step 1: Initialize variables
 
// Define whether an HTTPS connection is required
$HTTPS_required = FALSE;
 
// Define whether user authentication is required
$authentication_required = TRUE;
 
// Define API response codes and their related HTTP response
$api_response_code = array(
    0 => array('HTTP Response' => 400, 'Message' => 'Unknown Error'),
    1 => array('HTTP Response' => 200, 'Message' => 'Success'),
    2 => array('HTTP Response' => 403, 'Message' => 'HTTPS Required'),
    3 => array('HTTP Response' => 401, 'Message' => 'Authentication Required'),
    4 => array('HTTP Response' => 401, 'Message' => 'Authentication Failed'),
    5 => array('HTTP Response' => 404, 'Message' => 'Invalid Request'),
    6 => array('HTTP Response' => 400, 'Message' => 'Invalid Response Format'),
	7 => array('HTTP Response' => 405, 'Message' => 'Method not Accessible')
);
 
// Set default HTTP response of 'ok'
$response['code'] = 1;
$response['status'] = 200;
$response['data'] = NULL;

 
// --- Step 2: Authorization
 
// Optionally require connections to be made via HTTPS
if( $HTTPS_required && $_SERVER['HTTPS'] != 'on' ){
    $response['code'] = 2;
    $response['total_all_data'] = 0;
    $response['total_show_data'] = 0;
    $response['status'] = $api_response_code[ $response['code'] ]['HTTP Response'];
    $response['data'] = $api_response_code[ $response['code'] ]['Message'];
 
    // Return Response to browser. This will exit the script.
    $process->deliver_response($_GET['format'], $response, $client_name);
}
 
// Optionally require user authentication
if( $authentication_required ){
 
    if( $_SERVER['REQUEST_METHOD'] == 'GET' )  { // For GET Method
		$pin = @$_GET['pin'];
	}
	else if( $_SERVER['REQUEST_METHOD'] == 'POST' )  { // For POST Method
		$pin = @$_POST['pin'];
	}
	else if( $_SERVER['REQUEST_METHOD'] == 'PUT' )  { // For PUT Method
		parse_str(file_get_contents("php://input"),$post_vars);
		$pin = @$post_vars['pin'];
	}
	else if( $_SERVER['REQUEST_METHOD'] == 'DELETE' )  { // For DELETE Method
		parse_str(file_get_contents("php://input"),$post_vars);
		$pin = @$post_vars['pin'];
	}
	
	$client_data = $process->auth_webservice_client( $pin );
	$client_name = @$client_data['client_name'];
	$wsc_id = @$client_data['wsc_id'];
	
	
	
	if (!$client_name) { // Wrong Pin
		$response['code'] = 3;
		$response['status'] = $api_response_code[ $response['code'] ]['HTTP Response'];
		$response['data'] = $api_response_code[ $response['code'] ]['Message'];

		// Return Response to browser
		$process->deliver_response($_GET['format'], $response, $client_name);
	}
}

 
// --- Step 3: Process Request
 
// Method Echo: Response Success Message
if( $_SERVER['REQUEST_METHOD'] == 'GET' && strcasecmp($_GET['method'],'echo') == 0){
    $response['code'] = 1;
    $response['status'] = $api_response_code[ $response['code'] ]['HTTP Response'];
    $response['data'] = 'Success';
}
// Method Authentication
// Url: /modules/webservice/rest/?method=Authentication&pin=pin&format=json&username=username&password=password
else if( $_SERVER['REQUEST_METHOD'] == 'GET' && strcasecmp($_GET['method'],'Authentication') == 0){

	$process->check_access_method( $wsc_id, $client_name );
	
	$result = $process->authentication( $str_label );	
	
    $response['code'] = 1;
    $response['status'] = $api_response_code[ $response['code'] ]['HTTP Response'];
    $response['data'] = $result;
	
}
// Method GetSampleData
// Url: /modules/webservice/rest/?method=GetSampleData&pin=pin&format=json&search=text
else if( $_SERVER['REQUEST_METHOD'] == 'GET' && strcasecmp($_GET['method'],'GetSampleData') == 0){

	$process->check_access_method( $wsc_id, $client_name );
	
	$result = $process->get_sample_data();	
	
    $response['code'] = 1;
    $response['status'] = $api_response_code[ $response['code'] ]['HTTP Response'];
    $response['data'] = $result;
	
}
//Method Unknown
else {
	$response['code'] = 0;
	$response['status'] = 405;
	$response['data'] = NULL;
}
 
// --- Step 4: Deliver Response
 
// Return Response to browser
$process->deliver_response(@$_GET['format'], $response, $client_name);
 
?>