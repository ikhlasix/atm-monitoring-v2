<?php

// Include Classes and Basic Settings
include $_SERVER['DOCUMENT_ROOT']."/s/config.php";

// Get Label
$language->get_lang("global");



// Page Settings
$config['script'] = "/modules/examples/webservice/index";
$config['page_title'] = "Web Service";
$config['tabs'] = array();




$main_content = <<<EOD
	<ul>
		<li> Authentication 
			<ul>
				<li> /modules/webservice/rest/?method=Authentication&pin=pin&format=json&username=username&password=password </li>
				<li> <a href="/modules/webservice/rest/?method=Authentication&pin=123456&format=json&username=admin&password=admin" target="_blank">json example</a> </li>
				<li> <a href="/modules/webservice/rest/?method=Authentication&pin=123456&format=xml&username=admin&password=admin" target="_blank">xml example</a> </li>
			</ul>
		</li>
		<li> Get Sample Data 
			<ul>
				<li> /modules/webservice/rest/?method=GetSampleData&pin=pin&format=json&search=text&session_id=session_id </li>
				<li> <a href="/modules/webservice/rest/?method=GetSampleData&pin=123456&format=json&search=sawi&session_id=16f25b1cd34b69a5da0c9b2684ae3e7b" target="_blank">json example</a> </li>
				<li> <a href="/modules/webservice/rest/?method=GetSampleData&pin=123456&format=xml&search=sawi&session_id=16f25b1cd34b69a5da0c9b2684ae3e7b" target="_blank">xml example</a> </li>
			</ul>
		</li>
	</ul>
EOD;


//Page Template
include $_SERVER["DOCUMENT_ROOT"]."/view/basic_page.php";
?> 