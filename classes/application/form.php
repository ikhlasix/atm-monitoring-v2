<?php

namespace classes\application
{

/**
 * Class to handle Form components
 *
 * Form Components
*/

	class form {
		
		private $page;
		private $date;

		public function __construct( $db, page $page, date $date ) {
            $this->db = $db;
			$this->page = $page;
			$this->date = $date;
		}
		
		
		
		/**
		 * Function to create Text Input
		 *
		 * Create simple text input
		 *
		 * @param $name string variable name
		 *
		 * @param $type string input type
		 *
		 * @param $value string value
		 *
		 * @param $disabled string disabled or empty
		 *
		 * @return string text input
		*/
		
		public function text_input ( $name, $type, $value, $disabled ) {
			
			return "<input type=\"".$type."\" class=\"form-control\" name=\"".$name."\" id=\"".$name."\" value=\"".$value."\" $disabled>";
			
		}
		
		
		
		/**
		 * Function to create Masked Input
		 *
		 * Create masked text input
		 *
		 * @param $name string variable name
		 *
		 * @param $type string input type
		 *
		 * @param $value string value
		 *
		 * @param $disabled string disabled or empty
		 *
		 * @return string text input
		*/
		
		public function masked_input ( $name, $value, $disabled, $config ) {
			
			global $config;
			
			foreach ($config["form_fields"] as $key => $val) {
				if ($val['input_type']=="masked") {
					$format = $val['input_config']['format'];
				}
			}
			
			$config['additional_jquery'][] = "$('#".$name."').mask(\"99.999.999.9-999.999\");";
			
			return "<input type=\"text\" class=\"form-control\" name=\"".$name."\" id=\"".$name."\" value=\"".$value."\" placeholder=\"".$format."\" $disabled>";
			
		}
		
		
		
		/**
		 * Function to create Text Area Input
		 *
		 * Create text area input
		 *
		 * @param $name string variable name
		 *
		 * @param $value string value
		 *
		 * @param $disabled string disabled or empty
		 *
		 * @return string text area input
		*/
		
		public function textarea_input ( $name, $value, $disabled ) {
			
			return "<textarea class=\"form-control\" rows=\"5\" name=\"".$name."\" id=\"".$name."\" $disabled>".$value."</textarea>";
			
		}
		
		
		
		/**
		 * Function to create WYSIWYG Input
		 *
		 * Create WYSIWYG input
		 *
		 * @param $name string variable name
		 *
		 * @param $value string value
		 *
		 * @param $disabled string disabled or empty
		 *
		 * @return string text area input
		*/
		
		public function wysiwyg_input ( $name, $value, $disabled ) {
			
			return "<textarea class=\"textarea form-control\" id=\"wysihtml5\" name=\"".$name."\" rows=\"12\" $disabled>".$value."</textarea>";
			
		}
		
		
		
		/**
		 * Function to create date input
		 *
		 * Create date input
		 *
		 * @param $name string variable name
		 *
		 * @param $value string value
		 *
		 * @param $disabled string disabled or empty
		 *
		 * @return string text input
		*/
		
		public function date_input ( $name, $value, $disabled ) {
			
			$output = "
				<div class=\"input-group date\">
				<input type=\"text\" class=\"form-control masked-input-date\" name=\"".$name."\" id=\"".$name."\" value=\"".$value."\" placeholder=\"DD/MM/YYYY\" $disabled><span class=\"input-group-addon\"><i class=\"glyphicon glyphicon-calendar\"></i></span>
				</div>
			";
			
			return $output;
			
		}
		
		
		
		/**
		 * Function to create datetime input
		 *
		 * Create datetime input
		 *
		 * @param $name string variable name
		 *
		 * @param $value string value
		 *
		 * @param $disabled string disabled or empty
		 *
		 * @return string text input
		*/
		
		public function datetime_input ( $name, $value, $disabled ) {
			
			$output = "
				<div class=\"input-group datetime\">
				<input type=\"text\" class=\"form-control masked-input-datetime\" name=\"".$name."\" id=\"".$name."\" value=\"".$value."\" placeholder=\"DD/MM/YYYY HH:mm\" $disabled><span class=\"input-group-addon\"><i class=\"glyphicon glyphicon-calendar\"></i></span>
				</div>
			";
			
			return $output;
			
		}
		
		
		
		/**
		 * Function to create time input
		 *
		 * Create time input
		 *
		 * @param $name string variable name
		 *
		 * @param $value string value
		 *
		 * @param $disabled string disabled or empty
		 *
		 * @return string time input
		*/
		
		public function time_input ( $name, $value, $disabled ) {
			
			$output = "
				<div class=\"input-group time\">
				<input type=\"text\" class=\"form-control masked-input-time\" name=\"".$name."\" id=\"".$name."\" value=\"".$value."\" placeholder=\"HH:mm\" $disabled><span class=\"input-group-addon\"><i class=\"glyphicon glyphicon-time\"></i></span>
				</div>
			";
			
			return $output;
			
		}
		
		
		
		/**
		 * Function to create multiple date input
		 *
		 * Create multiple date input
		 *
		 * @param $name string variable name
		 *
		 * @param $value string value
		 *
		 * @param $disabled string disabled or empty
		 *
		 * @return string text input
		*/
		
		public function multidate_input ( $name, $value, $disabled ) {
			
			$output = "
				<div class=\"input-group date multi\">
				<input type=\"text\" class=\"form-control\" name=\"".$name."\" id=\"".$name."\" value=\"".$value."\" $disabled><span class=\"input-group-addon\"><i class=\"glyphicon glyphicon-th\"></i></span>
				</div>
			";
			
			return $output;
			
		}
		
		
		
		/**
		 * Function to create numeric input
		 *
		 * Create numeric input
		 *
		 * @param $name string variable name
		 *
		 * @param $value string value
		 *
		 * @param $disabled string disabled or empty
		 *
		 * @return string text input
		*/
		
		public function numeric_input ( $name, $value, $disabled ) {
			
			$output = "
				<input type=\"text\" class=\"form-control numeric\" data-a-sep=\"\" data-v-max=\"999999999999999999999999999999\" data-v-min=\"0\" name=\"".$name."\" id=\"".$name."\" value=\"".$value."\" $disabled>
			";
			
			return $output;
			
		}
		
		
		/**
		 * Function to create currency input
		 *
		 * Create currency input
		 *
		 * @param $name string variable name
		 *
		 * @param $value string value
		 *
		 * @param $disabled string disabled or empty
		 *
		 * @return string text input
		*/
		
		public function currency_input ( $name, $value, $disabled ) {
			
			$output = "
				<input type=\"text\" class=\"form-control numeric\" data-a-sep=\".\" data-a-dec=\",\" data-v-max=\"999999999999999999\" data-v-min=\"0\" name=\"".$name."\" id=\"".$name."\" value=\"".$value."\" $disabled>
			";
			
			return $output;
			
		}
		
		
		
		/**
		 * Function to create autocomplete Input
		 *
		 * Create autocomplete input based on the jquery-ui one
		 *
		 * @param $name string variable name
		 *
		 * @param $value string value
		 *
		 * @param $text string text
		 *
		 * @param $mode string autocomplete mode in /modules/utilities/autocomplete.php
		 *
		 * @param $disabled string disabled or empty
		 *
		 * @return string text input
		*/
		
		public function autocomplete_input ( $name, $value, $text, $mode, $disabled ) {
			
			global $config;
			
			$output = "<input type=\"hidden\" name=\"".$name."\" id=\"".$name."\" value=\"".$value."\"> <input type=\"text\" class=\"form-control autocomplete\" name=\"".$name."_autocomplete\" id=\"".$name."_autocomplete\" value=\"".$text."\" placeholder=\"Autocomplete\" $disabled><span class=\"fa fa-spinner fa-pulse fa-2x margin-bottom loaderspan\" id=\"".$name."_loader\"></span>";
			
			// $config['additional_jquery'][] = '$("#'.$name.'_autocomplete").autocomplete({source:"/modules/utilities/autocomplete.php?mode='.$mode.'",minLength:2,select:function(event,ui){$("#'.$name.'").val(ui.item.id);}});';
			
			$config['additional_jquery'][] = '$("#'.$name.'_autocomplete").devbridgeAutocomplete({serviceUrl:"/modules/utilities/autocomplete.php?mode='.$mode.'",minChars:2,onSearchStart:function(query) {$("#'.$name.'_loader").css("visibility","visible");},onSearchComplete:function(query,suggestions) {$("#'.$name.'_loader").css("visibility","hidden");},onSelect:function(suggestion){$("#'.$name.'").val(suggestion.data);}});';
			
			return $output;
			
		}
		
		
		
		/**
		 * Function to create combotree Input
		 *
		 * Create combotree input based on the jquery easyui
		 *
		 * @param $name string variable name
		 *
		 * @param $value string value
		 *
		 * @param $text string text
		 *
		 * @param $mode string autocomplete mode in /modules/utilities/autocomplete.php
		 *
		 * @param $disabled string disabled or empty
		 *
		 * @return string text input
		*/
		
		public function combotree_input ( $name, $value, $text, $mode, $disabled ) {
			
			global $config;
			
			$output = "<input class=\"form-control combotree-input\" name=\"".$name."\" id=\"".$name."\" $disabled>";
			
			$config['additional_jquery'][] = '$("#'.$name.'").combotree({url: "/modules/utilities/combotree.php?mode='.$mode.'",height:34,width:"100%"});';
            
            if ($value) $config['additional_jquery'][] = "$('#".$name."').combotree('setValue', {id:'".$value."',text:'".$text."'});";

			return $output;
			
		}
		
		
		
		/**
		 * Function to create Map Input
		 *
		 * Create map input
		 *
		 * @param $name string variable name
		 *
		 * @param $value string value
		 *
		 * @param $disabled string disabled or empty
		 *
		 * @return string text input
		*/
		
		public function map_input ( $name, $value, $disabled ) {
			
			global $config;
			
			if ($value) $value = explode("|",$value);
			else {
				$value[1] = -6.1753924;
				$value[2] = 106.82715280000002;
			}
			
			$output = "
				<div class=\"form-horizontal\">
					<div class=\"form-group\">
						<label class=\"col-md-1 control-label\">Location</label>
						<div class=\"col-md-7\"><input type=\"text\" class=\"form-control\" name=\"".$name."[]\" id=\"".$name."_location\" value=\"".@$value[0]."\" placeholder=\"Location\"></div>
					</div>
					<div id=\"".$name."_googlemap\" style=\"width: 550px; height: 300px;\"></div>
					<div class=\"clearfix\">&nbsp;</div>
					<div class=\"form-group\" style=\"margin-top:10px\">
						<label class=\"control-label col-md-1\" for=\"".$name."_lat\">Lat</label>
						<div class=\"col-md-3\">
							<input type=\"text\" name=\"".$name."[]\" id=\"".$name."_lat\" value=\"".@$value[1]."\" class=\"form-control\" placeholder=\"Lat\">             
						</div>
						<label class=\"control-label col-md-1\" for=\"".$name."_lon\">Lon</label>
						<div class=\"col-md-3\">
							<input type=\"text\" name=\"".$name."[]\" id=\"".$name."_lon\" value=\"".@$value[2]."\" class=\"form-control\" placeholder=\"Lon\">             
						</div>
					</div>
					<div class=\"clearfix\"></div>
				</div>
			";
			
			$config['additional_jquery'][] = "
			$('#".$name."_googlemap').locationpicker({
				location: {latitude: ".@$value[1].", longitude: ".@$value[2]."},
				radius: 0,
				inputBinding: {
					latitudeInput: $('#".$name."_lat'),
					longitudeInput: $('#".$name."_lon'),
					radiusInput: null,
					locationNameInput: $('#".$name."_location')
				},
				enableAutocomplete: true
			});
			";
			
			return $output;
			
		}
		
		
		
		
		/**
		 * Function to generate icon
		 *
		 * Generate icon based on file extension
		 *
		 * @param $filetype string file type eg. jpg/gif/doc/xls
		 *
		 * @return string font awesome icon eg. <i class="fa fa-file-image-o fa-2x"></i>
		*/
		
		public function generate_icon ( $filetype ) {
			
			$icon = "";
			if (preg_match("/jpg|gif|png|jpeg|tiff|bmp/i", $filetype)) $icon = "<i class=\"fa fa-file-image-o fa-2x\"></i>";
			else if (preg_match("/xls|csv/i", $filetype)) $icon = "<i class=\"fa fa-file-excel-o fa-2x\"></i>";
			else if (preg_match("/ppt/i", $filetype)) $icon = "<i class=\"fa fa-file-powerpoint-o fa-2x\"></i>";
			else if (preg_match("/doc/i", $filetype)) $icon = "<i class=\"fa fa-file-word-o fa-2x\"></i>";
			else if (preg_match("/pdf/i", $filetype)) $icon = "<i class=\"fa fa-file-pdf-o fa-2x\"></i>";
			else if (preg_match("/zip|rar|tar|gz|bzip/i", $filetype)) $icon = "<i class=\"fa fa-file-archive-o fa-2x\"></i>";
			else if (preg_match("/mp4|mpg|mpeg|avi|mkv/i", $filetype)) $icon = "<i class=\"fa fa-file-video-o fa-2x\"></i>";
			else if (preg_match("/mp3|m4a/i", $filetype)) $icon = "<i class=\"fa fa-file-audio-o fa-2x\"></i>";
			
			return $icon;
		}
		
		
		
		/**
		 * Function to create File Input
		 *
		 * Create File Input
		 *
		 * @param $name string variable name
		 *
		 * @param $file_config array setting of files $file_config['table'],$file_config['primary_key'],$file_config['primary_key2'],$file_config['primary_key3'],$file_config['form_fields']
		 *
		 * @param $disabled string disabled or empty
		 *
		 * @return string file input
		*/
		
		public function file_input ( $name, $file_config, $disabled, $str_label ) {
			
			// Check for file upload
			$type_variables = "";
			$extension_variables = "";
			foreach ($file_config["form_fields"] as $key => $value) {
				if ($value['field_variable']==$name) {
					$type_variables = $value['input_config']['type'];
					$extension_variables = $value['input_config']['extensions'];
				}
			}
			
			$cond = "";
			if (isset($_GET[$file_config['primary_key']])) $cond = "pk1_name='".$file_config['primary_key']."' and pk1_value='".$_GET[$file_config['primary_key']]."' and ";
			if (isset($_GET[$file_config['primary_key2']])) $cond = "pk2_name='".$file_config['primary_key2']."' and pk2_value='".$_GET[$file_config['primary_key2']]."' and ";
			if (isset($_GET[$file_config['primary_key3']])) $cond = "pk3_name='".$file_config['primary_key3']."' and pk3_value='".$_GET[$file_config['primary_key3']]."' and ";
			if ($cond) $cond = "and ".substr($cond,0,-5);
			else $cond = " and 1=2 ";
			
			$output = "";
			$process_key = 0;
			$sql="select * from app_upload where isnull(is_delete,0)!='1' and filetable='".$file_config['table']."' and filefield='$name' $cond";
			$this->page->debug($sql);
			$result=$this->db->Execute($sql);
			if (!$result) echo $this->page->debug_query(1);
			while($row=$result->FetchRow()) {
			   	if (is_array($row)){ foreach($row as $key=>$val) {
				   	$key=strtolower($key);
				   	$$key=$val;
			   	}}
			   
			   	$icon = $this->generate_icon( $filetype ); 
				   
				   
				$output .= "
				<div class=\"alert alert-preview\">";
				$output .= "<p>$icon <a href=\"$filepath\" target=\"_blank\">$filename</a></p>";
				if (!$disabled) {
					$output .= "<p>
					<label class=\"radio-inline\"><input name=\"".$name."_process[$process_key]\" type=\"radio\" value=\"keep\" checked=\"checked\"> ".$str_label['LBL_KEEP']."</label>
					<label class=\"radio-inline\"><input name=\"".$name."_process[$process_key]\" type=\"radio\" value=\"remove\"> ".$str_label['LBL_REMOVE']."</label>
					<input type=\"hidden\" name=\"".$name."_id[]\" value=\"$upl_id\">
					</p>";
				}
				$output .= "
				</div>";
				
				$process_key++;
			
			}
			
			$num_file_input = 1;
			if ($disabled) $num_file_input = 0;
			
			if ($type_variables == "multiple" and !$disabled) {
				$output .= "<script> window['".$name."_container_num'] = 0; </script>";
				$output .= "<button class=\"btn btn-grey\" onClick=\"show_next('".$name."_container');return false;\"><i class=\"fa fa-plus\"></i></button>";
				$num_file_input = 5;
			}
			
			$visibility = "";
			for ($i=0; $i < $num_file_input; $i++) {
				
				if ($i>0) $visibility = "display:none"; 
				
				$output .= "<div class=\"".$name."_container".$i."\" style=\"".$visibility."\">";
				$output .= "<p><input id=\"".$name."_upload[]\" name=\"".$name."_upload[]\" type=\"file\" class=\"file\" values=\"\" data-show-upload=\"false\" $disabled>";
				$output .= "<small>only $extension_variables allowed</small></p>";
				$output .= "</div>".PHP_EOL;
			
			}
			
			return $output;
			
		}
		
		
		
		/**
		 * Function to upload files
		 *
		 * Process Upload Files
		 *
		 * @param $upload_config file upload config in array $upload_config['table'],$upload_config['pk1_name'],$upload_config['pk1_value'],$upload_config['pk2_name'],$upload_config['pk2_value'],$upload_config['pk3_name'],$upload_config['pk3_value'],$upload_config['user_detail'],$upload_config['form_fields']
		 *
		 * @return string error message
		*/
		
		public function upload_process ( $upload_config, $str_label ) {
			
			$error = "";
			
			// Check for file upload
			$upload_variables = array();
			$target_dir_variables = array();
			$extension_variables = array();
			foreach ($upload_config["form_fields"] as $key => $value) {
				if ($value['input_type']=="file") {
					$upload_variables[] = $value['field_variable'];
					$target_dir_variables[] = $value['input_config']['target_dir'];
					$extension_variables[] = $value['input_config']['extensions'];
				}
			}
			
			foreach ($upload_variables as $key => $files) {
				
				$target_dir = $target_dir_variables[$key];
				$extensions = $extension_variables[$key];
				
				if (is_array(@$_POST[$files."_id"])) {
					foreach ($_POST[$files."_id"] as $key2 => $file_id) {
						if (isset($file_id) && $_POST[$files."_process"][$key2] == 'remove') {

							// Delete uploaded file
							$sql = "update app_upload set is_delete=1, d_time=".$this->date->datetime_to_db(date("Y-m-d H:i:s")).", d_user='".$upload_config['user_detail']->username."' where upl_id='$file_id'";
							$this->page->debug($sql);
							$res_delete=$this->db->Execute($sql);
							if (!$res_delete) $this->page->debug_query(1);
						}
					}
				}
				
				if (is_array(@$_FILES)) {
				
					if (is_array(@$_FILES[$files."_upload"]['name'])) {
						
						foreach ($_FILES[$files."_upload"]['name'] as $key2 => $file) {
							
							$sec=microtime();
							mt_srand((double)microtime()*1000000);
							$sec2 = mt_rand(1000,9999);
							$fileid=md5("$sec2$sec");
							$folder1 = substr($fileid,0,1);
							$folder2 = substr($fileid,1,1);
							$folder3 = substr($fileid,2,1);
							
							if (!file_exists($_SERVER['DOCUMENT_ROOT'].$target_dir."$folder1")) mkdir($_SERVER['DOCUMENT_ROOT'].$target_dir."$folder1");
							if (!file_exists($_SERVER['DOCUMENT_ROOT'].$target_dir."$folder1/$folder2")) mkdir($_SERVER['DOCUMENT_ROOT'].$target_dir."$folder1/$folder2");
							if (!file_exists($_SERVER['DOCUMENT_ROOT'].$target_dir."$folder1/$folder2/$folder3")) mkdir($_SERVER['DOCUMENT_ROOT'].$target_dir."$folder1/$folder2/$folder3");
							
							$target_file = $fileid."_".strtolower(str_replace(" ","_",basename($file)));
							$target_path = $target_dir."$folder1/$folder2/$folder3/".$target_file;
							
							// Check file extensions
							$extension_arr = explode(",", $extensions);
							$reg_extension = "";
							foreach ($extension_arr as $key3 => $extension) {
								$reg_extension .= "$extension|";
							}
							$reg_extension = substr($reg_extension, 0, -1);
							if (!preg_match("/$reg_extension/i", pathinfo($target_path,PATHINFO_EXTENSION))) {
								$error = $str_label["MSG_EXTENSION_NOT_ALLOWED"];
							}
							else {
								if (move_uploaded_file($_FILES[$files."_upload"]["tmp_name"][$key2], $_SERVER['DOCUMENT_ROOT']."/".$target_path)) {
									
									$upl_id = $this->generate_row_id("app_upload","upl_id","UPL");
									$filename = $file;
									$filepath = $target_path;
									$filetype = pathinfo($target_path,PATHINFO_EXTENSION);
									$filesize = $_FILES[$files."_upload"]["size"][$key2];
									$filetable = $upload_config['table'];
									$pk1_name = $upload_config['pk1_name'];
									$pk1_value = $upload_config['pk1_value'];
									$pk2_name = $upload_config['pk2_name'];
									$pk2_value = $upload_config['pk2_value'];
									$pk3_name = $upload_config['pk3_name'];
									$pk3_value = $upload_config['pk3_value'];
									
									// Insert to table
									$sql = "insert into app_upload (upl_id, filename, filepath, filetype, filesize, filetable, filefield, pk1_name, pk1_value, pk2_name, pk2_value, pk3_name, pk3_value, c_time, c_user)
										values ('$upl_id', '$filename', '$filepath', '$filetype', '$filesize', '$filetable', '$files', 
										'$pk1_name', '$pk1_value', '$pk2_name', '$pk2_value', '$pk3_name', '$pk3_value', ".$this->date->datetime_to_db(date("Y-m-d H:i:s")).", '".$upload_config['user_detail']->username."')
									";
									$this->page->debug($sql);
									$res_insert=$this->db->Execute($sql);
									if (!$res_insert) {
										$error = $str_label["MSG_UPLOAD_FAILED"];
										$this->page->debug_query(1);	
									}
									
								} else {
									$error = $str_label["MSG_UPLOAD_FAILED"];
								}
							}
						}
					}	
				}	
			}
			
			return $error;
		}
		
		

		/**
		 * Function to create select list drop down
		 *
		 * Create simple drop down from db table
		 *
		 * @param $name string variable name
		 *
		 * @param $table string table name
		 *
		 * @param $option_name string option
		 *
		 * @param $value_name string value
		 *
		 * @param $curr_id string current variable value
		 *
		 * @param $script string javascript to be added
		 *
		 * @param $cond string query condition eg: where kode='a'
		 *
		 * @param $sql string custom query command
		 *
		 * @return string drop down select list
		*/

		public function select_list($name,$table,$option_name,$value_name,$curr_id,$script="",$cond="",$sql="") {
			
			$db = $this->db;
			
			$output		 = "<SELECT NAME='$name' $script id='$name' class=>\n";
			$output		.= "<option></option>\n";
			if($sql==""){
				$sql="select * from $table $cond ";
				// if (!preg_match("/order/i", $sql)) $sql = $sql ." order by $value_name ";
			}
			$result = $db->Execute("$sql");
			if (!$result){
				print $db->ErrorMsg();
			}
			while ( $row = $result->FetchRow() ) {
				foreach($row as $key=>$val){
					$key=strtolower($key);
					$$key=trim($val);
				}
				if(preg_match("#\smultiple\s#i",$script)){
						$selected=preg_match("#$curr_id#i",$$option_name)?"selected":"";
				}else{
						$selected=(trim($curr_id)==trim($$option_name))?"selected":"";
						
				}
				
				$output .= "<option value='".$$option_name."' $selected>";
				if(preg_match("/,/",$value_name)){
					$value_name_arr=split(",",$value_name);
					unset($_output);
					for($i=0;$i< count($value_name_arr);$i++){
						$key_val = $value_name_arr[$i];
						$val = $$value_name_arr[$i];
						if(eregi("tgl|tanggal|mulai|selesai",$key_val))
							$val = date("d/m/Y",strtotime($val));
						
						if($val!="01/01/1970")
							$_output .=" ".$val." |";

					}
					$output .=preg_replace("/\|$/","",$_output);
				}else{	
					$output.= $$value_name;
				}
				$output .="</option>\n";
			}
			$result->Close();

			$output .= "</SELECT>\n";
			return $output;
		}
		
		
		
		/**
		 * Function to create select list drop down from array
		 *
		 * Create simple drop down from array
		 *
		 * @param $name string variable name
		 *
		 * @param $array array key-value pair in array
		 *
		 * @param $curr_id string current variable value
		 *
		 * @param $script string javascript to be added
		 *
		 * @return string drop down select list
		*/
		
		function select_list_array($name,$array,$curr_id,$script="") {
			
			$output = "<select name=\"$name\" id=\"$name\" $script><option>\n";
			$selected = "";
			
			foreach($array as $value=>$text){
				if($curr_id!=""){
					if($value==$curr_id) $selected="selected";	
				}else{
					$selected="";
				}
				
				$output .= "<option value=\"".$value."\" ".$selected.">".$text."</option>";
				
				$selected="";
			}

			$output .= "</select>\n";

			return $output;
		}
		
		
		
		
		/**
		 * Function to create radio list
		 *
		 * Create radio list from db table
		 *
		 * @param $name string variable name
		 *
		 * @param $table string table name
		 *
		 * @param $option_name string option
		 *
		 * @param $value_name string value
		 *
		 * @param $curr_id string current variable value
		 *
		 * @param $script string javascript to be added
		 *
		 * @param $cond string query condition eg: where kode='a'
		 *
		 * @param $sql string custom query command
		 *
		 * @param $inline string checkbox type inline "yes" or "no"
		 *
		 * @return string drop down select list
		*/
		
		function radio_list($radio_name,$table,$option_name,$value_name,$curr_id,$script="",$cond="",$sql="", $inline="no") {
			
			$db = $this->db;
			
			if($sql==""){
				$sql="select $option_name, $value_name from $table $cond ";
			}
			$result = $db->Execute("$sql");
			if (!$result){
				print $db->ErrorMsg();
			}
			$i = 0;
			$output = "";
			while ( $row = $result->FetchRow() ) {
				$i++;
				if (is_array($row)){ foreach($row as $key=>$val) {
					$key=strtolower($key);
					$$key=$val;
				}}
				
				$_option_name = $$option_name;
				$_value_name = $$value_name;
				
				$radio_class = "";
				if ($inline == "no") $radio_class = "radio";
				else if ($inline == "yes") $radio_class = "radio-inline";

				$output .= "
				<div class=\"$radio_class\">
					<label>
					<input type=radio name=$radio_name id=$radio_name$i value='$_option_name' ".(($curr_id==$_option_name)?"checked":"").">
					$_value_name
					</label>
				</div>
				";
			}
			$result->Close();


			return $output;
		}
		
		
		
		/**
		 * Function to create checkbox list
		 *
		 * Create checkbox list from db table
		 *
		 * @param $name string variable name
		 *
		 * @param $table string table name
		 *
		 * @param $option_name string option
		 *
		 * @param $value_name string value
		 *
		 * @param $curr_id string current variable value
		 *
		 * @param $script string javascript to be added
		 *
		 * @param $cond string query condition eg: where kode='a'
		 *
		 * @param $sql string custom query command
		 *
		 * @param $inline string checkbox type inline "yes" or "no"
		 *
		 * @return string drop down select list
		*/
		
		function checkbox_list ($checkbox_name,$table,$option_name,$value_name,$curr_id,$script="",$cond="",$sql="",$inline="no") {

			$db = $this->db;
			$output = "";

			$curr_id_array = array();
			
			if(is_array($curr_id)) {
			
				if ($curr_id['cond']) $cond1 = " and ".$curr_id['cond'];
				
				$sql="SELECT id FROM app_multiselect WHERE jenis='".$curr_id['jenis']."' $cond1 ";   
				$result = $db->Execute("$sql");
				if (!$result){
					echo"$sql";
					print $db->ErrorMsg();
				}
				
				while ( $row = $result->FetchRow() ) {
					$curr_id_array[]=$row['id'];
				}   
			
			} else {

				if(preg_match("/\|/",$curr_id)) {
					$curr_id_array=explode("|",$curr_id);
				}
			}

			if ($sql == "") $sql="select $option_name,$value_name from ".$table." $cond";
			$result = $db->Execute("$sql");
			if (!$result){
				print $db->ErrorMsg();

			}
			while ( $row = $result->FetchRow() ) {
				$selected = "";
				$selected = (in_array($row[$option_name],$curr_id_array)?"checked ":"");
				
				$checkbox_class = "";
				if ($inline == "no") $checkbox_class = "checkbox";
				else if ($inline == "yes") $checkbox_class = "checkbox-inline";
				
				$output .= "
				<div class=\"$checkbox_class\">
					<label>
					<input type=\"checkbox\" name=\"".$checkbox_name."[]\" id=\"$checkbox_name\" value=\"".$row[$option_name]."\" $selected $script>
					".$row[$value_name]."
					</label>
				</div>
				";
				
				unset($selected);
				unset($_value);
			}
			$result->Close();

			return $output;
		}
		
		
		
		/**
		 * Function to generate row number
		 *
		 * Create incremental row number (1,2,3,4,5, ...)
		 *
		 * @param $table string table name
		 *
		 * @param $column string name of incremental column
		 *
		 * @return integer row number
		*/
		
		public function generate_row_number ( $table, $column ) {
			
			$sql="select max($column) as max_num from $table";
			$max_num=$this->db->GetOne($sql);
			if (!isset($max_num)) $max_num = 0;
			
			$next_num = $max_num + 1;
			
			return $next_num;
		}
		
		
		
		/**
		 * Function to generate row id
		 *
		 * Create incremental row id (USR-000001, USR-000002, ...)
		 *
		 * @param $table string table name
		 *
		 * @param $column string name of incremental column
		 *
		 * @param $prefix string prefix used in incremental column
		 *
		 * @param $start int starting number
		 *
		 * @return string row id
		*/
		
		public function generate_row_id($table,$column,$prefix,$start=5){
			
			$column=strtoupper($column);
			$table=strtoupper($table);
			
			$strSQL	= "select cast(substring($column,$start,6) as bigint)+1 as nomerurut
			from $table where $column like '$prefix%'
			order by cast(substring($column,$start,6) as bigint) desc";
			
			$result	= $this->db->SelectLimit($strSQL,1,0);
			if (!$result) echo $this->db->ErrorMsg();
			$row = $result->FetchRow();
			$nomor = $row["nomerurut"];
			if(empty($nomor)) $nomor="1";
			//26 aug 2010. kalau primary key-nya lebih dari 99999 ganti prefix incremental 1 letter
			if($nomor >= 500000){
				$third_char=substr($prefix,2,1);
				if(strtolower($third_char)=='z'){
					$second_char=substr($prefix,1,1);
					$second_char=$this->incremental_letter($second_char);
					$next_prefix=substr($prefix,0,1).$second_char.'A';
				}else{
					$third_char=$this->incremental_letter($third_char);
					$next_prefix=substr($prefix,0,2).$third_char;
				}
				$next_prefix=strtoupper($next_prefix);
				
				$counter_date=date("d/m/Y H:i:s");
			}
			$nomor="$prefix-".sprintf("%06d",$nomor);
			return $nomor;
		}
		
		
		
		/**
		 * Function to generate incremental leter
		 *
		 * Create incremental letter M, N, O, P, etc
		 *
		 * @param $letter string letter eg. A/B/C
		 *
		 * @return string incremented letter A->B, M->N
		*/
		
		private function incremental_letter($letter){
			$letter=ord($letter);            //Convert to an integer
			if($letter=='122') $letter='96';#z
			$letter=chr($letter+1);            //Convert back to a string, but the
			return $letter;
		}
		
		
		
		/**
		 * Function to validate form
		 *
		 * Validate form values
		 *
		 * @param $table string table name
		 *
		 * @param $column string name of incremental column
		 *
		 * @param $prefix string prefix used in incremental column
		 *
		 * @param $start int starting number
		 *
		 * @return string row id
		*/
		
		public function validate ( $config ) {
			
			if (!preg_match("/".$_SERVER['HTTP_HOST']."/i", $_SERVER['HTTP_REFERER'])) {
				$error .="<li>Invalid post, this action is logged";
				$this->page->debug("Processing page from ".$_SERVER['HTTP_REFERER']);
			}
			
			$error = "";
			
			foreach ($config["form_fields"] as $key => $form_group) {
				// Re Process data for currency_input
				if (preg_match("/currency/i",$form_group["input_type"])) {
					$_POST[$form_group["field_variable"]] = intval(str_replace(".","",$_POST[$form_group["field_variable"]]));
				}
				
				// Mandatory Field
				if (preg_match("/mandatory/i",$form_group["validation"]) && empty($_POST[$form_group["field_variable"]])) {
					$error .="<li><strong>".$form_group["field_name"]."</strong> is mandatory";
				}            
				// Valid Email
				if (preg_match("/valid_email/i",$form_group["validation"])) {
					if (!filter_var($_POST[$form_group["field_variable"]], FILTER_VALIDATE_EMAIL)) $error .="<li>".$form_group["field_name"]." is not a valid email address";
				}
				// Valid Number
				if (preg_match("/valid_number/i",$form_group["validation"])) {
					$_POST[$form_group["field_variable"]] = str_replace(".","",$_POST[$form_group["field_variable"]]);
					if(is_numeric($_POST[$form_group["field_variable"]]) && $_POST[$form_group["field_variable"]] > 0 && $_POST[$form_group["field_variable"]] == round($_POST[$form_group["field_variable"]], 0)){
					}
					else $error .="<li>".$form_group["field_name"]." is not a valid number";
				}
				// Valid Date
				if (preg_match("/valid_date/i",$form_group["validation"])) {
					$date_part = explode(",",$_POST[$form_group["field_variable"]]);
					
					foreach ($date_part as $k => $v) {
						$d = "";
						$m = "";
						$y = "";
						if (preg_match("/\//i",$v)) {
							list($d,$m,$y) = explode("/",$v);   
						}
						else if (preg_match("/-/i",$v)) {
							list($y,$m,$d) = explode("-",$v);   
						}
						if (@checkdate($m,$d,$y) == false) $error .="<li>".$v." is not a valid date";    
					}
					
				}
				
        	}
			
			return $error;
			
		}
		
		
		
		
		/**
		 * Function to generate standard input
		 *
		 * Generate standard input from settings
		 *
		 * @param $form_group array form setting
		 *
		 * @param $input_disabled string "disabled" to generate disabled input
		 *
		 * @param $config array page setting
		 *
		 * @param $str_label array multi language label
		 *
		 * @return string row id
		*/
		
		public function generate_standard_input ( $form_group, $input_disabled, $config, $str_label ) {
			
			$input = "";
			
			// Text, Password, Email
			if (preg_match("/^(text|password|email)$/i",$form_group["input_type"])) {
				$input = $this->text_input ( $form_group["field_variable"], $form_group["input_type"], $form_group["field_value"], $input_disabled );
			}
			// Text Area
			else if (preg_match("/^(textarea)$/i",$form_group["input_type"])) {
				$input = $this->textarea_input ( $form_group["field_variable"], $form_group["field_value"], $input_disabled );
			}
			// Masked input 
			else if (preg_match("/^(masked)$/i",$form_group["input_type"])) {
				$input = $this->masked_input ( $form_group["field_variable"], $form_group["field_value"], $input_disabled, $config );
			}
			// WYSIWYG
			else if (preg_match("/^(wysiwyg)$/i",$form_group["input_type"])) {
				$input = $this->wysiwyg_input ( $form_group["field_variable"], $form_group["field_value"], $input_disabled );
			}
			// Date
			else if (preg_match("/^(date)$/i",$form_group["input_type"])) {
				$input = $this->date_input ( $form_group["field_variable"], $form_group["field_value"], $input_disabled );
			}
			// Datetime
			else if (preg_match("/^(datetime)$/i",$form_group["input_type"])) {
				$input = $this->datetime_input ( $form_group["field_variable"], $form_group["field_value"], $input_disabled );
			}
			// Time
			else if (preg_match("/^(time)$/i",$form_group["input_type"])) {
				$input = $this->time_input ( $form_group["field_variable"], $form_group["field_value"], $input_disabled );
			}
			// Multi Date
			else if (preg_match("/^(multidate)$/i",$form_group["input_type"])) {
				$input = $this->multidate_input ( $form_group["field_variable"], $form_group["field_value"], $input_disabled );
			}
			// Numeric
			else if (preg_match("/^(numeric)$/i",$form_group["input_type"])) {
				$input = $this->numeric_input ( $form_group["field_variable"], $form_group["field_value"], $input_disabled );
			}
			// Currency
			else if (preg_match("/^(currency)$/i",$form_group["input_type"])) {
				$input = $this->currency_input ( $form_group["field_variable"], $form_group["field_value"], $input_disabled );
			}
			// Autocomplete
			else if (preg_match("/^(autocomplete)$/i",$form_group["input_type"])) {
				$input = $this->autocomplete_input ( $form_group["field_variable"], $form_group["field_value"], $form_group["input_config"]["text"], $form_group["input_config"]["mode"], $input_disabled );
			}
			// Combotree
			else if (preg_match("/^(combotree)$/i",$form_group["input_type"])) {
				$input = $this->combotree_input ( $form_group["field_variable"], $form_group["field_value"], $form_group["input_config"]["text"], $form_group["input_config"]["mode"], $input_disabled );
			}
			// Map
			else if (preg_match("/^(map)$/i",$form_group["input_type"])) {
				$input = $this->map_input ( $form_group["field_variable"], $form_group["field_value"], $input_disabled );
			}
			// File
			else if (preg_match("/^(file)$/i",$form_group["input_type"])) {
				
				$file_config = array();
				$file_config['table'] = $config['table'];
				$file_config['primary_key'] = $config['primary_key'];
				$file_config['primary_key2'] = $config['primary_key2'];
				$file_config['primary_key3'] = $config['primary_key3'];
				$file_config['form_fields'] = $config['form_fields'];
				
				$input = $this->file_input ( $form_group["field_variable"], $file_config, $input_disabled, $str_label );
			}
			
			return $input;
			
		}
		
		
		
		
		/**
		 * Function to generate additional jquery
		 *
		 * Generate additional jquery based on input type given, shown on the bottom of the page inside jquery ready function
		 *
		 * @param $config array page settings
		 *
		 * @return string additional jquery
		*/
		
		public function generate_additional_jquery( $config ) {
			
			$output = "";
			
			if ($this->page->get_operation("act","add") || $this->page->get_operation("act","update")  || $this->   page->get_operation("act","view")) {
				if (is_array(@$config['additional_jquery'])) {
					foreach (@$config['additional_jquery'] as $key => $value) {
						$output .= $value.PHP_EOL.PHP_EOL;
					}
				}
			}
			
			return $output;
			
		}


	}
}

?>