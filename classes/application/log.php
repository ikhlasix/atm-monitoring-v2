<?php

namespace classes\application
{

/**
 * Class Logger
 *
 * For keeping log of all operation
*/

	class log {
		
		private $session;

		public function __construct ( session $session, $db ) {
			$this->db = $db;
			$this->session = $session;
		}


		/**
		 * Function to insert log
		 *
		 * Keeping the log data to table app_log
		 *
		 * @param $activity string description of activity with format {OPERATION:INSERT/UPDATE/DELETE} {TABLE NAME} {ID=xxx} (INSERT emp_master, UPDATE emp_master EMPLOYEE_ID=123456 )
		 *
		 * @param $session object session class
		 *
		 * @param $sql string sql command
		 *
		 * @param $username custom username of leave black to get logged in user
		 *
		 * @return result boolean
		*/

		public function insert_log ( $activity, $sql="", $username="" ) {

			if(!empty($username)){
				$login_username = $username;
			}else{
				$user_detail = $this->session->get_user_details_session($this->session->get_session_id());
				$login_username = $user_detail->username;
			}
			$ctime=date("m/d/Y H:i:s");
			$sql=base64_encode($sql);
			$sql="insert into app_log (user_id,activity,ctime,ip,detail) values ('$login_username','$activity','".date('Y-m-d H:i:s')."','".$_SERVER['REMOTE_ADDR']."','$sql')";
			$result=$this->db->Execute($sql);
			if (!$result){
				return false;
			}
			else {
				return true;
			}
		}
	}
}

?>