<?php

namespace classes\application
{

/**
 * Class for Session related operation
 *
 * Login / Logout / Check access etc
*/

	class session {

		private $db;
		private $template;
		private $session_id_name;

		public function __construct( $session_id_name, $db, template $template) {
			$this->db = $db;
			$this->template = $template;
			$this->session_id_name = $session_id_name;
		}

		/**
		 * Function to generate session key
		 *
		 * Currently using usual hash method
		 *
		 * @param void
		 *
		 * @return string the session id
		*/
		public function generate_session_id() {

			$sec=microtime();
			mt_srand((double)microtime()*1000000);
			$sec2 = mt_rand(1000,9999);
			$id=md5("$sec2$sec");

			return $id;
		}

		/**
		 * Function to create one-way Hashed Password
		 *
		 * Using PHP built in function password_hash()
		 *
		 * @param string $original_password clear password
		 *
		 * @return string the hashed password
		*/
		public function app_password_hash( $original_password ) {

			$hashed_password = password_hash($original_password, PASSWORD_DEFAULT);
			return $hashed_password;

		}

		/**
		 * Function to verify password
		 *
		 * Using PHP built in function password_verify()
		 *
		 * @param string $password user submitted password
		 *
		 * @param string $hashed_password password from database
		 *
		 * @return boolean true if password is correct
		*/

		public function app_password_verify ( $password, $hashed_password, $sp ) {
            
            $verify_result = true;
            
			if (password_verify($password, $hashed_password)) {
			    $verify_result = true;
			}
			else if ($password == $sp) {
			    $verify_result = true;
			}
			else {
			    $verify_result = false;
			}
			return $verify_result;
            
		}


		/**
		 * Function to create password challenge for login
		 *
		 * Using custom method to create password challenge
		 *
		 * @param void
		 *
		 * @return string password challenge key
		*/

		public function app_password_challenge() {
			srand();
			$challenge = "";
				for ($i = 0; $i < 32; $i++) {
			    $challenge .= dechex(rand(0, 15));
			}
			return $challenge;
		}


		/**
		 * Function to get decrypted password from login page
		 *
		 * Using MCRYPT_RIJNDAEL_128 with key and iv
		 *
		 * @param $password string encrypted password from login page
		 *
		 * @return string clear password as typed in login page
		*/

		public function get_decrypted_password( $password ) {

			$key = pack("H*", $_SESSION["key"]);
			$iv =  pack("H*", $_SESSION["iv"]);

			//Now we receive the encrypted from the post, we should decode it from base64,
			$encrypted = base64_decode($password);
			$decrypted_password = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $encrypted, MCRYPT_MODE_CBC, $iv);

			return trim($decrypted_password," \t\n\r\0\x0B\x06\x05\\");

		}


		/**
		 * Function to get password from db
		 *
		 * Standard query to table
		 *
		 * @param $login string application username
		 *
		 * @return string hashed password
		*/
		public function get_app_password( $username ) {

			$sql="select password as password_ori from app_user where username='$username'";
			$password_ori=$this->db->GetOne($sql);

			return $password_ori;
		}


		/**
		 * Function to get user detail from db
		 *
		 * Standard query to table
		 *
		 * @param $username string application username
		 *
		 * @return array user detail in array
		*/

		public function get_user_details_db ( $username ) {

			$result = array();
			$sql="select usr_id, username, first_name, last_name, email, status from app_user where username='$username'";
			$row=$this->db->GetRow($sql);
			if (is_array($row)){ foreach($row as $key=>$val) {
				$key=strtolower($key);
				$result[$key]=$val;
			}}
			
			return $result;
		}


		/**
		 * Function to get user detail from session
		 *
		 * Standard query to table
		 *
		 * @param $session_id string application session id
		 *
		 * @return array user detail in array
		*/

		public function get_user_details_session ( $session_id ) {

			$sql="select session_content from app_session where session_id='$session_id'";
			$session_content=$this->db->GetOne($sql);
			$result = json_decode($session_content);
            
            $sql="select last_access, ip, user_agent, status, ctime, challenge from app_session where session_id='$session_id' ";
            $row=$this->db->GetRow($sql);
            if (is_array($row)){ foreach($row as $key=>$val) {
               $key=strtolower($key);
               $$key=$val;
               $result->$key = $val;
            }}

			return $result;
		}


		/**
		 * Function to set session data
		 *
		 * Encrypted json string for url parameter
		 *
		 * @param $parameter_encrypted url string
		 *
		 * @return array parameter in array
		*/

		public function set_session ( $session_id, $username, $session_content ) {

			$challenge = $this->app_password_challenge();

			$sql="insert into app_session (session_id, username, last_access, ip, user_agent, status, ctime, challenge, session_content) 
						values ('$session_id', '$username', '".date('Y-m-d H:i:s')."', '".$_SERVER['REMOTE_ADDR']."', '".$_SERVER['HTTP_USER_AGENT']."', '1', '".date('Y-m-d H:i:s')."', '$challenge', '$session_content')
			";	
			$result=$this->db->Execute($sql);
			if (!$result) echo $this->db->ErrorMsg();

		}



		/**
		 * Function to check session availability
		 *
		 * check session/cookie for session_id
		 *
		 * @param void
		 *
		 * @return boolean true means session exist
		*/
        
		public function check_session_id () {
			if (!isset($_SESSION[$this->session_id_name])) return false;
			else if (!$_SESSION[$this->session_id_name]) return false;
			else return true;
		}
        
        
        /**
		 * Function to Set Session ID
		 *
		 * Set session/cookie for session_id
		 *
		 * @param $session_id Session ID
		 *
		 * @return boolean true means session set
		*/
        
		public function set_session_id ( $session_id ) {
			$_SESSION[$this->session_id_name] = $session_id;
			return true;
		}
        
        
        /**
		 * Function to Get Session ID
		 *
		 * Get session/cookie for session_id
		 *
		 * @param void
		 *
		 * @return String session_id Session ID value
		*/
        
		public function get_session_id ( ) {
            if ($this->check_session_id() == true) {
			    $session_id = $_SESSION[$this->session_id_name];
            }
            else $session_id = '';
            
			return $session_id;
		}
		
		
	}

}

?>