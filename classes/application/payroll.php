<?php

namespace classes\application
{

/**
 * Class for Processing Payroll
 *
 * Payroll processing class with all included functions
*/
class payroll {

	function payroll () {
	}

	/**
	 * Function to get Payroll Key
	 *
	 * Getting payroll key from server to encrypt/decrypt numeric values
	 *
	 * @param void
	 *
	 * @return string the payroll key value
	*/
	function getkey() {
		global $_SERVER;

		#$key = @file_get_contents("http://".$_SERVER['REMOTE_ADDR'].":8188/key.php");
		$key='Z2Fk';
		return base64_decode($key);

	}
	
	
	/**
	 * Function to encrypt value
	 *
	 * Will return RC4 encrypted string
	 *
	 * @param string $var the string to encrypt
	 *
	 * @return string $new_var encrypted string
	*/
	function encrypt($var) {
		global $payroll_key;
		
		$new_var = rc4crypt::encrypt($payroll_key, (string)$var);
		
		return $new_var;
		
	}
	
	/**
	 * Function to encrypt value
	 *
	 * Will return RC4 encrypted string, for db insert
	 *
	 * @param string $var the string to encrypt
	 *
	 * @return string encrypted string
	*/
	function db_encrypt($var) {
		global $payroll_key;
		
		$new_var = rc4crypt::encrypt($payroll_key, (string)$var);
		$new_var = str_replace("'","''",$new_var);
	
		return $new_var;
	
	}
	
	
	/**
	 * Function to decrypt value
	 *
	 * Will return original value from RC4 encrypted string
	 *
	 * @param string $var the string to decrypt
	 *
	 * @return string decrypted value
	*/
	function decrypt($var) {
		global $payroll_key;
		
		return rc4crypt::decrypt($payroll_key, (string)$var);
	
	}
	
	
}

}

?>