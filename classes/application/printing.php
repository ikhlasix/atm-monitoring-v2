<?php

namespace classes\application
{

class printing {

	function printing () {
	}
    
    function parsing_data($text, $variable, $data, $type, $align='normal') {
    
        preg_match("/#{1}(_*)$variable(_*)#{1}/", $text, $matches);
        $field = $matches[0];
    
        if ($type == 'number') {
            if (!trim($data)) $data = 0;
            $data = number_format($data,0,',','.');
        }
    
        $len_field = strlen($field);
        $len_data = strlen($data);
        
        $len_gap = $len_field - $len_data;
        
        if ($align == 'normal') {
            $filler = '';
            for ($i=0; $i<$len_gap; $i++) $filler .= ' ';
        }
        else if ($align == 'center') {
            $len_gap_left = $this->mod_floor($len_gap/2);
            $len_gap_right = $len_gap - $len_gap_left;
            
            $filler_left = '';
            for ($i=0; $i<$len_gap_left; $i++) $filler_left .= ' ';
            $data = $filler_left.$data;
            
            $filler = '';
            for ($i=0; $i<$len_gap_right; $i++) $filler .= ' ';
        }
        else if ($align == 'right') {
            $filler_left = '';
            $filler = '';
            for ($i=0; $i<$len_gap; $i++) $filler_left .= ' ';
            $data = $filler_left.$data;
        }
    
        if ($type == 'string') $out = $data.$filler;
        else if ($type == 'number') $out = $filler.$data;
        
        $text_out = str_replace($field, $out, $text);
        
        return $text_out;
    
    }
    
 
    function format_date($datetext) {
    
        $bulan_arr = array(1=>"Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
    	$datetext = trim($datetext);
        $out = substr($datetext,8,2)." ".$bulan_arr[(int)substr($datetext,5,2)]." ".substr($datetext,0,4); 
        
        return $out;
    }
    
    function format_excel($datetext) {
    
        $datetext = trim($datetext);
        $out = substr($datetext,2,2)."/".substr($datetext,0,2)."/".substr($datetext,4,4); 
        
        return $out;
    }
    
    function terbilang( $num ,$dec=4){
        $stext = array(
            "Nol",
            "Satu",
            "Dua",
            "Tiga",
            "Empat",
            "Lima",
            "Enam",
            "Tujuh",
            "Delapan",
            "Sembilan",
            "Sepuluh",
            "Sebelas"
        );
        $say  = array(
            "Ribu",
            "Juta",
            "Milyar",
            "Triliun",
            "Biliun", // remember limitation of float
            "--apaan---" ///setelah biliun namanya apa?
        );
        $w = "";

        if ($num <0 ) {
            $w  = "Minus ";
            //make positive
            $num *= -1;
        }

        $snum = number_format($num,$dec,",",".");
        #die($snum);
        $strnum =  explode(".",substr($snum,0,strrpos($snum,",")));
        //parse decimalnya
        $koma = substr($snum,strrpos($snum,",")+1);

        $isone = substr($num,0,1)  ==1;
        if (count($strnum)==1) {
            $num = $strnum[0];
            switch (strlen($num)) {
                case 1:
                case 2:
                    if (!isset($stext[$strnum[0]])){
                        if($num<19){
                            $w .=$stext[substr($num,1)]." Belas";
                        }else{
                            $w .= $stext[substr($num,0,1)]." Puluh ".
                                (intval(substr($num,1))==0 ? "" : $stext[substr($num,1)]);
                        }
                    }else{
                        $w .= $stext[$strnum[0]];
                    }
                    break;
                case 3:
                    $w .=  ($isone ? "Seratus" : $this->terbilang(substr($num,0,1)) .
                        " Ratus").
                        " ".(intval(substr($num,1))==0 ? "" : $this->terbilang(substr($num,1)));
                    break;
                case 4:
                    $w .=  ($isone ? "Seribu" : $this->terbilang(substr($num,0,1)) .
                        " Ribu").
                        " ".(intval(substr($num,1))==0 ? "" : $this->terbilang(substr($num,1)));
                    break;
                default:
                    break;
            }
        }else{
            $text = $say[count($strnum)-2];
            $w = ($isone && strlen($strnum[0])==1 && count($strnum) <=3? "Se".strtolower($text) : $this->terbilang($strnum[0]).' '.$text);
            array_shift($strnum);
            $i =count($strnum)-2;
            foreach ($strnum as $k=>$v) {
                if (intval($v)) {
                    $w.= ' '.$this->terbilang($v).' '.($i >=0 ? $say[$i] : "");
                }
                $i--;
            }
        }
        $w = trim($w);
        if ($dec = intval($koma)) {
            $w .= " Koma ". $this->terbilang($koma);
        }
        
        return trim($w);
    }
    
    function format_periode($text) {
    	
    	$bulan_arr = array(1=>"Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
    	
    	$tahun = substr($text,0,4);
    	$bulan = substr($text,4,2);
    	
    	return $bulan_arr[(int)$bulan]." ".$tahun;
    }
    
    function mod_floor($number) {
        global $nip;
        
        $upper_number = ceil($number);
        $range = $upper_number - $number;
        
        if (number_format($range,3) <= 0.005) $output = $upper_number;
        else $output = floor($number);
    
        return $output;    
    }
    
    function add_blank_space($text, $digit, $position, $blank_ch) {
    
    	$text_len = strlen(strval($text));
    
    	$blank_ch_num = intval($digit)-intval($text_len);
    	$blank_filler = '';
    	for ($i=0; $i<$blank_ch_num; $i++) {
    		$blank_filler .= $blank_ch;
    	}
    
    	$output = '';
    	if ($position=='start') $output = $blank_filler.strval($text);
    	elseif ($position=='end') $output = strval($text).$blank_filler;
    
    	return $output;
    }
    
	function excel_header($size,$orientation,$header1,$header2,$freeze_from_row=0,$freeze_to_row=0) {
	
	if ($size=='A3') $size_index = "8";
	else if ($size=='A4') $size_index = "9";
	else if ($size=='Folio') $size_index = "7";
	else if ($size=='Letter') $size_index = "5";
	
	if ($size=='A3') $size_text = "size:1190pt 842pt;";
	else if ($size=='A4') $size_text = "size:842pt 595pt;";
	else if ($size=='Folio') $size_text = "size:936pt 612pt;";
	else if ($size=='Letter') $size_text = "size:792pt 612pt;";
	
	?>
		<html xmlns:v="urn:schemas-microsoft-com:vml"
		xmlns:o="urn:schemas-microsoft-com:office:office"
		xmlns:x="urn:schemas-microsoft-com:office:excel"
		xmlns="http://www.w3.org/TR/REC-html40">
		<head>
		<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
		<meta name=ProgId content=Excel.Sheet>
		<meta name=Generator content="Microsoft Excel 14">
		<style type="text/css">
		table {mso-displayed-decimal-separator:"\.";
		mso-displayed-thousand-separator:"\,";}
		col{mso-width-source:auto;}
		td{font-size:8pt;font-family:Arial;}
		th{font-size:8pt;font-family:Arial;}
		.hiddenDataField{background-color:red;}
		@page{
			mso-page-orientation:landscape;
			<?=$size_text?>
			mso-header-data:"&L&G &\0022-\,Bold\0022&12<?=addslashes($header1)?>&\0022-\,Regular\0022&11\000A&12<?=addslashes($header2)?>";
			mso-footer-data:"Page &P of &N";
			margin:.75in 1.25in .5in .6in;
			mso-header-margin:.3in;
			mso-footer-margin:.3in;
		}
		</style>
		<!--[if gte mso 9]><xml>
		<x:ExcelWorkbook>
		<x:ExcelWorksheets>
		<x:ExcelWorksheet>
		<x:Name>Report</x:Name>
		<x:WorksheetOptions>
		<x:Print>
		<x:ValidPrinterInfo/>
		<x:PaperSizeIndex><?=$size_index?></x:PaperSizeIndex>
		<x:HorizontalResolution>600</x:HorizontalResolution>
		<x:VerticalResolution>600</x:VerticalResolution>
		</x:Print>
		<x:Selected/>
		<x:ProtectContents>False</x:ProtectContents>
		<x:ProtectObjects>False</x:ProtectObjects>
		<x:ProtectScenarios>False</x:ProtectScenarios>
		</x:WorksheetOptions>
		</x:ExcelWorksheet>
		</x:ExcelWorksheets>
		<x:WindowHeight>12780</x:WindowHeight>
		<x:WindowWidth>19035</x:WindowWidth>
		<x:WindowTopX>0</x:WindowTopX>
		<x:WindowTopY>15</x:WindowTopY>
		<x:ProtectStructure>False</x:ProtectStructure>
		<x:ProtectWindows>False</x:ProtectWindows>
		</x:ExcelWorkbook>
		<x:ExcelName> 
		<x:Name>Print_Titles</x:Name> 
		<x:SheetIndex>1</x:SheetIndex> 
		<x:Formula>=Report!$<?=$freeze_from_row?>:$<?=$freeze_to_row?></x:Formula>
		</x:ExcelName>
		</xml><![endif]-->
		</head>
		<body>
	<?
	}
	
	function excel_footer() {
	?>
		</body>
		</html>
	<?
	}
	
	function word_header($size,$orientation,$header1,$header2) {
	global $HTTP_HOST,$_SERVER;
	
	if ($size=='A3') $size_text = "size:842pt 1190pt;";
	else if ($size=='A4') $size_text = "size:842pt 595pt;";
	else if ($size=='Folio') $size_text = "size:936pt 612pt;";
	else if ($size=='Letter') $size_text = "size:792pt 612pt;";
	
	$size_text = "size:14.875in 11in;";
	
	$isSecure = false;
	if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
		$isSecure = true;
	}
	elseif (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' || !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on') {
		$isSecure = true;
	}
	$request_protocol = $isSecure ? 'https' : 'http';

	?>
		<html xmlns:v="urn:schemas-microsoft-com:vml"
		xmlns:o="urn:schemas-microsoft-com:office:office"
		xmlns:w="urn:schemas-microsoft-com:office:word"
		xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
		xmlns="http://www.w3.org/TR/REC-html40">
		<head>
		<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
		<meta name=ProgId content=Word.Document>
		<meta name=Generator content="Microsoft Word 14">
		<meta name=Originator content="Microsoft Word 14">
		<style type="text/css">
		<!--
		table {mso-displayed-decimal-separator:"\.";
		mso-displayed-thousand-separator:"\,";}
		col{mso-width-source:auto;}
		td{
			font-size:8pt;
			font-family:Arial;
		}
		th{
			font-size:8pt;
			font-family:Arial;
		}
		.hiddenDataField{background-color:red;}
		table 
			{mso-style-name:"Table Grid";
			mso-tstyle-rowband-size:0;
			mso-tstyle-colband-size:0;
			mso-style-priority:59;
			mso-style-unhide:no;
			border:solid windowtext 1.0pt;
			mso-border-alt:solid windowtext .5pt;
			mso-padding-alt:0in 5.4pt 0in 5.4pt;
			mso-border-insideh:.5pt solid windowtext;
			mso-border-insidev:.5pt solid windowtext;
			mso-para-margin:0in;
			mso-para-margin-bottom:.0001pt;
			mso-pagination:widow-orphan;
			font-size:13.0pt;
			font-family:"Arial";
			mso-ascii-font-family:Calibri;
			mso-ascii-theme-font:minor-latin;
			mso-hansi-font-family:Calibri;
			mso-hansi-theme-font:minor-latin;
			border-collapse:collapse;
			border:none;
			mso-border-alt:solid windowtext .5pt;
			mso-yfti-tbllook:1184;
			mso-padding-alt:0in 5.4pt 0in 5.4pt}
		td, th 
			{border-top:none;
			border-left:none;
			border-bottom:solid windowtext 1.0pt;
			border-right:solid windowtext 1.0pt;
			mso-border-top-alt:solid windowtext .5pt;
			mso-border-left-alt:solid windowtext .5pt;
			mso-border-alt:solid windowtext .5pt;
			padding:0in 5.4pt 0in 5.4pt }
		p.MsoHeader, li.MsoHeader, div.MsoHeader
		{	
			font-size:11.0pt;
			font-family:"Arial";
			margin:0in;
		}
		p.MsoFooter, li.MsoFooter, div.MsoFooter
		{	
			font-size:11.0pt;
			font-family:"Arial";
			margin:0in;
		}
		@page WordSection1
		{
			<?=$size_text?>
			mso-page-orientation:<?=$orientation?>;
			margin:.75in 1.25in .5in .6in;
			mso-header:url("<?=$request_protocol?>://<?=$HTTP_HOST?>/payroll/template/header.php?header1=<?=$header1?>&header2=<?=$header2?>") h1;
			mso-footer:url("<?=$request_protocol?>://<?=$HTTP_HOST?>/payroll/template/header.php?header1=<?=$header1?>&header2=<?=$header2?>") f1;
			mso-header-margin:0in;
			mso-footer-margin:0in;
			mso-paper-source:0;
		}
		div.WordSection1
			{page:WordSection1;}
		-->
		</style>
		<!--[if gte mso 9]><xml>
		<w:WordDocument>
		<w:View>Print</w:View>
		<w:Zoom>90</w:Zoom>
		<w:DoNotOptimizeForBrowser/>
		</w:WordDocument>
		</xml><![endif]-->
		</head>
		<body>

		<div class=WordSection1>
	<?
	}
	
	function word_footer() {
	?>
		</div>

		</body>
		</html>
	<?
	}
}

}
?>