<?php

namespace classes\application
{
class template{

	function __construct(){
	}
	
	function basicheader ($site_title) {
	?>
	<!DOCTYPE html>
	<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
	<!--[if !IE]><!-->
	<html lang="en">
	<!--<![endif]-->

	<head>
		<meta charset="utf-8" />
		<title><?=$site_title?></title>
		<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
		<meta content="" name="description" />
		<meta content="" name="author" />
		
		<!-- ================== BEGIN BASE CSS STYLE ================== -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
		<link href="/assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet" />
		<link href="/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
		<link href="/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
		<link href="/assets/css/animate.min.css" rel="stylesheet" />
		<link href="/assets/css/style.min.css" rel="stylesheet" />
		<link href="/assets/css/style-responsive.min.css" rel="stylesheet" />
		<link href="/assets/css/theme/default.css" rel="stylesheet" id="theme" />
		<!-- ================== END BASE CSS STYLE ================== -->
		
		<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
		<link href="/assets/plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" />
		<link href="/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" />
		<link href="/assets/plugins/bootstrap-fileinput/css/fileinput.min.css" rel="stylesheet" />
		<link href="/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
		<link href="/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
		<link href="/assets/plugins/bootstrap-wysihtml5/src/bootstrap-wysihtml5.css" rel="stylesheet" />
		<!-- ================== END PAGE LEVEL STYLE ================== -->
		
	</head>
	<body class="body-frame">
	<?php	
	
	}
	
	function basicfooter() {
	
	?>
	</body>
	</html>
	<?php
	
	}

	function bootstrapcss() {
	?>
	<link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap/css/bootstrap.min.css">
	<?php
	}


	/**
		 * Function to create message box
		 *
		 * Message box for every type
		 *
		 * @param $title string Message title
		 *
		 * @param $description string Message description
		 *
		 * @param $button array button can be more than 1, example $button=array("0"=>array("$PHP_SELF","Back"),"1"=>array("$PHP_SELF","Next"))
		 *
		 * @param $type string info/success/warning/error/validation
		 *
		 * @return void
		*/

	public function box($title,$description,$button="",$type="error",$width=""){
	
		echo "<link href=\"/assets/plugins/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" />";
		echo "<link href=\"/assets/plugins/font-awesome/css/font-awesome.min.css\" rel=\"stylesheet\" />";
		echo "<link href=\"/assets/css/interface.css\" rel=\"stylesheet\">";
		echo"
		<center>
		<div class=\"$type\" style=\"width:400px;\">
		<p><strong>$title</strong></p>
		<p>$description</p>
		";

		if(is_array($button)){
			/*example
			$button=array("0"=>array("$PHP_SELF","Kembali"))
			*/
			for($i=0;$i<count($button);$i++){
				if($button[$i][0]=='back'){
					$action="javascript:history.back(-1)";
				}else{
					$action="location.href='".$button[$i][0]."'";
				}
				echo"<input type=button class=box onClick=\"$action\" value='".$button[$i][1]."'> ";
			}
		}else{
			// No Button
		}

		echo"
		</div>
		</center>
		";
		
	}
	

}

}

?>