<?php

namespace classes\application
{

class report {

	function report () {
	}
	
	function get_induk_eselon ($condition=array()) {
		global $db,$debug,$f;
		
		foreach ($condition as $key=>$val) {
			$key = strtolower($key);
			$$key=$val;
		}
		
		if(!empty($nip)){
		
			$sql="select kd_unit_org from spg_data_current where nip='$nip'";
			if ($debug) $f->pre($sql);
			$kd_unit_org_nip=$db->GetOne($sql);
			
			$sql="select kd_unit_es4 from spg_08_unit_organisasi where trim(kd_unit_org)='trim($kd_unit_org_nip)'";
			//echo $sql; 
			if ($debug) $f->pre($sql);
			$kd_unit_es4_nip=$db->GetOne($sql);
			
			$rel 	= !empty($cond_unit)?"and":"where";
			$cond_unit .= " $rel a.kd_unit_org='trim($kd_unit_es4_nip)' ";
		}
		if(!empty($cmp_id)){
			$rel 	= !empty($cond_unit)?"and":"where";
			$cond_unit .= " $rel a.cmp_id='$cmp_id' ";
		}
		if(!empty($jns_kantor)){
			$rel 	= !empty($cond_unit)?"and":"where";
			$cond_unit .= " $rel a.jns_kantor='$jns_kantor' ";
		}
		if(!empty($kd_unit_es3)){
			$rel 	= !empty($cond_unit)?"and":"where";
			$cond_unit .= " $rel a.kd_unit_es3='$kd_unit_es3' ";
		}
		if(!empty($kd_unit_es4)){
			$rel 	= !empty($cond_unit)?"and":"where";
			$cond_unit .= " $rel a.kd_unit_es4='$kd_unit_es4' ";
		}
		$rel 	= !empty($cond_unit)?"and":"where";
		$cond_unit .= " $rel (a.tgl_end is NULL or a.tgl_end='1900-01-01')";
		
		$sql="select top 1 a.kd_eselon from spg_08_unit_organisasi a $cond_unit order by kd_eselon asc, kd_unit_org asc";
		if ($debug) $f->pre($sql);
		$kd_eselon=$db->GetOne($sql);
		
		return $kd_eselon;
	}
	
	function get_all_unit_org ($condition=array()) {
		global $db,$debug,$f,$unit_org_arr;
		
		foreach ($condition as $key=>$val) {
			$key = strtolower($key);
			$$key=$val;
		}
		
		if(!empty($nip)){
		
			$sql="select kd_unit_org from spg_data_current where nip='$nip'";
			if ($debug) $f->pre($sql);
			$kd_unit_org_nip=$db->GetOne($sql);
			
			$sql="select kd_unit_es4 from spg_08_unit_organisasi where trim(kd_unit_org)='trim($kd_unit_org_nip)'";
			//echo $sql; 
			if ($debug) $f->pre($sql);
			$kd_unit_es4_nip=$db->GetOne($sql);
			
			$rel 	= !empty($cond_unit)?"and":"where";
			$cond_unit .= " $rel a.kd_unit_org='trim($kd_unit_es4_nip)' ";
		}
		if(!empty($cmp_id)){
			$rel 	= !empty($cond_unit)?"and":"where";
			$cond_unit .= " $rel a.cmp_id='$cmp_id' ";
		}
		if(!empty($jns_kantor)){
			$rel 	= !empty($cond_unit)?"and":"where";
			$cond_unit .= " $rel a.jns_kantor='$jns_kantor' ";
		}
		if(!empty($kd_unit_es3)){
			$rel 	= !empty($cond_unit)?"and":"where";
			$cond_unit .= " $rel a.kd_unit_es3='$kd_unit_es3' ";
		}
		if(!empty($kd_unit_es4)){
			$rel 	= !empty($cond_unit)?"and":"where";
			$cond_unit .= " $rel a.kd_unit_es4='$kd_unit_es4' ";
		}
		if(!empty($kd_eselon)){
			$rel 	= !empty($cond_unit)?"and":"where";
			$cond_unit .= " $rel a.kd_eselon='$kd_eselon' ";
		}
		if(!empty($kd_unit_org_iterasi)){
			unset($str_kd_unit_org);
			/* if ($kd_eselon_baru=='10') $str_kd_unit_org = " a.kd_unit_es1='$kd_unit_org_iterasi' and a.kd_eselon='".($kd_eselon_baru+10)."' ";
			else if ($kd_eselon_baru=='20') $str_kd_unit_org = " a.kd_unit_es2='$kd_unit_org_iterasi' and a.kd_eselon='".($kd_eselon_baru+10)."' ";
			else if ($kd_eselon_baru=='30') $str_kd_unit_org = " a.kd_unit_es3='$kd_unit_org_iterasi' and a.kd_eselon='".($kd_eselon_baru+10)."' ";
			else if ($kd_eselon_baru=='40') $str_kd_unit_org = " a.kd_unit_es4='$kd_unit_org_iterasi' and a.kd_eselon='".($kd_eselon_baru+10)."' ";
			else if ($kd_eselon_baru=='50') $str_kd_unit_org = " a.kd_unit_es5='$kd_unit_org_iterasi' and a.kd_eselon='".($kd_eselon_baru+10)."' ";
			else if ($kd_eselon_baru=='60') $str_kd_unit_org = " a.kd_unit_es6='$kd_unit_org_iterasi' and a.kd_eselon='".($kd_eselon_baru+10)."' ";
			else if ($kd_eselon_baru=='70') $str_kd_unit_org = " a.kd_unit_es7='$kd_unit_org_iterasi' and a.kd_eselon='".($kd_eselon_baru+10)."' "; */
			
			$str_kd_unit_org = " a.kd_unit_org_induk='$kd_unit_org_iterasi' ";
							
			$rel 	= !empty($cond_unit)?"and":"where";
			$cond_unit .= " $rel $str_kd_unit_org ";
		}
		$rel 	= !empty($cond_unit)?"and":"where";
		$cond_unit .= " $rel (a.tgl_end is NULL or a.tgl_end='1900-01-01')";
		
		$sql="select a.kd_unit_org as kd_unit_org_baru, a.nm_unit_org as nm_unit_org_baru, a.kd_eselon as kd_eselon_baru from spg_08_unit_organisasi a $cond_unit order by a.nm_unit_org asc";
		if ($debug) $f->pre($sql,0);
		$res_unit=$db->Execute($sql);
		if (!$res_unit) echo $db->ErrorMsg();
		while($row_unit=$res_unit->FetchRow()) {
			if (is_array($row_unit)){ foreach($row_unit as $key=>$val) {
				$key=strtolower($key);
				$$key=$val;
			}}
			
			$unit_org_arr[] = array($kd_unit_org_baru,$nm_unit_org_baru,$kd_eselon_baru);
			
			//Iterasi hanya sampai batas paling bawah eselon
			//if ($kd_eselon_baru<$max_kd_eselon) {
			
				//Cek apakah ada unit dibawahnya
				$cond_baru['cmp_id']=$cmp_id;
				$cond_baru['jns_kantor']=$jns_kantor;
				$cond_baru['kd_kantor']=$kd_kantor;
				$cond_baru['kd_unit_es3']=$kd_unit_es3;
				$cond_baru['kd_unit_es4']=$kd_unit_es4;
				$cond_baru['kd_status_kepeg']=$kd_status_kepeg;
				$cond_baru['level']=$level;
				$cond_baru['nip']=$nip;
				$cond_baru['max_kd_eselon']=$max_kd_eselon;
				$cond_baru['kd_eselon_baru']=$kd_eselon_baru;
				$cond_baru['kd_unit_org_iterasi']=$kd_unit_org_baru;
				
				$this->get_all_unit_org($cond_baru);
			//}
		}
	}
		
	
}

}

?>