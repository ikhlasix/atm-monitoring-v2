<?php

namespace classes\application
{

/**
 * Class for Session related operation
 *
 * Login / Logout / Check access etc
*/

	class language {

		public function __construct ( $default_lang ) {
			$this->default_lang = $default_lang;
		}

		private function check_lang() {

			/*if (!isset($_COOKIE['lang'])) $active_lang = $this->default_lang;
			else if (isset($_COOKIE['lang']) && !$_COOKIE['lang']) $active_lang = $this->default_lang;
			else $active_lang = $_COOKIE['lang'];*/
			
			$active_lang = $this->default_lang;

			return $active_lang;
		}

		public function get_lang($file) {
			global $str_label;

			$active_lang = $this->check_lang();
			include $_SERVER['DOCUMENT_ROOT']."/lang/".$active_lang."/".$file.".php";

		}



	}

}

?>