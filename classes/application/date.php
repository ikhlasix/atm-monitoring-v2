<?php

namespace classes\application
{

/**
 * Class to handle date and time
 *
 * Date and Time process
*/

	class date {
		
		function __construct(){
		}
		
		
		/**
		 * Function datetime to db
		 *
		 * Process date format for insert/update
		 *
		 * @param $datetime date datetime 
		 *
		 * @return date formated datetime
		*/
        
		public function datetime_to_db ( $datetime ) {
            
			return " convert(datetime,'$datetime',120) ";
			
        }
        
        
        /**
		 * Function datetime from db
		 *
		 * Process date format for select
		 *
		 * @param $datetime_field date datetime field name
		 *
		 * @return date formated datetime
		*/
        
        public function datetime_from_db ( $datetime_field ) {
			
			$alias_field = "";
			$datetime_field_arr = explode(".",$datetime_field);
			if (count($datetime_field_arr) > 1) {
				$alias_field = $datetime_field_arr[ (count($datetime_field_arr)-1) ]; 
			}
			else $alias_field = $datetime_field;
			
			return " convert(varchar(max),$datetime_field,120) $alias_field ";
            
        }
		
		
		/**
		 * Function datetime to display
		 *
		 * Process date format to display
		 *
		 * @param $datetime date datetime
		 *
		 * @return date formated datetime
		*/
        
        public function datetime_to_display ( $datetime, $output_type="datetime" ) {
			
			$formated_datetime1 = "";
			$formated_datetime2 = "";
			$datetime_arr1 = array();
			$datetime_arr2 = array();
			
			$datetime = str_replace(array("T","Z")," ", $datetime);
			$datetime_arr1 = explode(" ",$datetime);
			$datetime_arr2 = explode("-",$datetime_arr1[0]);
			
			$formated_datetime1 = @$datetime_arr2[2]."/".@$datetime_arr2[1]."/".@$datetime_arr2[0];
			if (@$datetime_arr1[1]) $formated_datetime2 = " ".@$datetime_arr1[1];
			
			if ($output_type == "datetime")	$formated_datetime = $formated_datetime1.$formated_datetime2;
			else if ($output_type == "date")	$formated_datetime = $formated_datetime1;
			else if ($output_type == "time")	$formated_datetime = $datetime;
			
			return $formated_datetime;
            
        }
		
		
		
		/**
		 * Function datetime to display
		 *
		 * Process date format to display
		 *
		 * @param $datetime date datetime
		 *
		 * @return date formated datetime
		*/
        
        public function display_to_datetime ( $datetime ) {
			
			
			$formated_datetime1 = "";
			$formated_datetime2 = "";
			$datetime_arr1 = array();
			$datetime_arr2 = array();
			
			$datetime_arr1 = explode(" ",$datetime);
			$datetime_arr2 = explode("/",$datetime_arr1[0]);
			
			$formated_datetime1 = $datetime_arr2[2]."-".$datetime_arr2[1]."-".$datetime_arr2[0];
			if (@$datetime_arr1[1]) $formated_datetime2 = " ".$datetime_arr1[1];
			
			$formated_datetime = $formated_datetime1.$formated_datetime2;
			
			return $formated_datetime;
            
        }

	}

}

?>