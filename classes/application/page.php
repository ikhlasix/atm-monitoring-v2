<?php

namespace classes\application
{

/**
 * Class to handle page operation
 *
 * Page operation process
*/

	class page {

		private $template;
		private $session;
		private $date;
		private $log;
		public $do_debug;

		public function __construct( session $session, $db, log $log, template $template, date $date ) {
			$this->template = $template;
			$this->session = $session;
			$this->date = $date;
            $this->db = $db;
			$this->log = $log;
			$this->do_debug = 1;
		}

		/**
		 * Function get operation type
		 *
		 * Process the required action if true
		 *
		 * @param $key string operation key
		 *
		 * @param $value string operation value
		 *
		 * @return boolean run operation if true
		*/

		public function get_operation( $key, $value) {

			if (isset($_POST[$key])) {
				if ($_POST[$key] == $value) return true;
				else return false;
			}
			else if (isset($_GET[$key])) {
				if ($_GET[$key] == $value) return true;
				else return false;
			}
			else return false;

		}



		/**
		 * Function to set url parameter
		 *
		 * Encrypted json string for url parameter
		 *
		 * @param $param_array url parameter in array
		 *
		 * @return string encrypted parameter for url
		*/

		public function set_parameter ( $parameter_array, $challenge ) {

			$parameter_json = json_encode($parameter_array);
			$parameter_encoded = $this->encrypt_url($parameter_json,$challenge);
			$parameter_encoded = base64_encode($parameter_encoded);

			return $parameter_encoded;

		}


		/**
		 * Function to get url parameter
		 *
		 * Encrypted json string for url parameter
		 *
		 * @param $parameter_encrypted url string
		 *
		 * @return array parameter in array
		*/

		public function get_parameter ( $challenge, $str_label ) {
            
            if (@$_GET['param']) { 
                
                $parameter_encrypted = $_GET['param'];
                unset($_GET['param']);
                $parameter_encrypted = base64_decode($parameter_encrypted);
			    $parameter_decoded = $this->decrypt_url($parameter_encrypted,$challenge);
			    $parameter_array = json_decode($parameter_decoded);
				
				if (count($parameter_array)<1) {
					$this->template->box($str_label["LBL_ERROR"],$str_label["MSG_INVALID_PARAMETER"], "","error");
					die();
				}
			
                foreach ($parameter_array as $key => $value) {
                    $_GET[$key] = $value;
                }
            }
        }
        
        /**
		 * Function to encrypt url
		 *
		 * Custom method to encrypt url
		 *
		 * @param $string string to encrypt
		 *
		 * @param $key encryption key
		 *
		 * @return string encrypted string
		*/

		private function encrypt_url( $string, $key ) {
		  	$result = '';
		  	$test = "";
		   	for($i=0; $i<strlen($string); $i++) {
		     	$char = substr($string, $i, 1);
		     	$keychar = substr($key, ($i % strlen($key))-1, 1);
		     	$char = chr(ord($char)+ord($keychar));

		     	$test[$char]= ord($char)+ord($keychar);
		     	$result.=$char;
		   	}

		   	return urlencode(base64_encode($result));
		}
        
        
        /**
		 * Function to decrypt url
		 *
		 * Custom method to decrypt url
		 *
		 * @param $string string to decrypt
		 *
		 * @param $key decryption key
		 *
		 * @return string decrypted string
		*/

		private function decrypt_url( $string, $key ) {
		    $result = '';
		    $string = base64_decode(urldecode($string));
		   	for($i=0; $i<strlen($string); $i++) {
		     	$char = substr($string, $i, 1);
		     	$keychar = substr($key, ($i % strlen($key))-1, 1);
		     	$char = chr(ord($char)-ord($keychar));
		     	$result.=$char;
		   	}
		   	return $result;
		}


		/**
		 * Function to redirect page
		 *
		 * Redirect page using meta refresh
		 *
		 * @param $delay number delay time to redirect in second
		 *
		 * @param $link string link target to redirect
		 *
		 * @param $message string message shown during redirection
		 *
		 * @return void
		*/

		function redirect($delay ,$link ,$message){

			echo "<meta http-equiv=\"refresh\" content = \"$delay; url=$link\">";
			$this->template->box("<i class=\"fa fa-spinner fa-pulse fa-2x margin-bottom\"></i> Redirecting ..", $message, "", "info");

		}
        
        
        /**
		 * Function to debug page
		 *
		 * Show debug line
		 *
		 * @param $input line to show while debugging
		 *
		 * @param $die continue or stop on debug
		 *
		 * @return void
		*/
        
        public function debug($input,$die='0'){
			
			global $log_file;
			global $debug_output;
			global $user_detail;
			
			if (!isset($log_file)) {
				
				$file_name1 = "";
				$file_name2 = "";
				$file_name3 = "";
				
				$file_name1 = @$user_detail->username;
				if (isset($_POST['act'])) $file_name2 = $_POST['act'];
				else if (isset($_GET['act'])) $file_name2 = $_GET['act'];
				$file_name3 = date("Ymd");  
				
				$log_file = $_SERVER['DOCUMENT_ROOT']."/i/log/".$file_name1."_".$file_name3.".log";
				
				$debug_output = PHP_EOL.PHP_EOL;
				$debug_output .= date("Y-m-d H:i:s")." ".$_SERVER['PHP_SELF']." ".$file_name2.PHP_EOL;
				$debug_output .= "------------------------------------------------------------------------------".PHP_EOL;
				$debug_output .= print_r( $input, true ).PHP_EOL.PHP_EOL;
				
				file_put_contents ( $log_file, $debug_output, FILE_APPEND );
			}
			else {
				
				$debug_output = print_r( $input, true ).PHP_EOL.PHP_EOL;
				
				file_put_contents ( $log_file, $debug_output, FILE_APPEND );
			}
			
			if ($this->do_debug=='1') {
				
				echo "<link href=\"/assets/plugins/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\">";
				echo"<pre>";
				print_r($input);
				echo"</pre>";
				if($die=='1') die();
			}
			
		}
		
		
		/**
		 * Function to debug query
		 *
		 * Show debug line
		 *
		 * @param $die continue or stop on debug
		 *
		 * @return void
		*/
        
        public function debug_query($die='0'){
			
			global $log_file;
			global $debug_output;
			
			if (!isset($log_file)) {
				
				$file_name1 = $user_detail->username;
				if (isset($_POST['act'])) $file_name2 = $_POST['act'];
				else if (isset($_GET['act'])) $file_name2 = $_GET['act'];
				$file_name3 = date("Ymd");  
				
				$log_file = $_SERVER['DOCUMENT_ROOT']."/i/log/".$file_name1."_".$file_name3.".log";
				
				$debug_output = PHP_EOL.PHP_EOL;
				$debug_output .= date("Y-m-d H:i:s")." ".$_SERVER['PHP_SELF']." ".$file_name2.PHP_EOL;
				$debug_output .= "------------------------------------------------------------------------------".PHP_EOL;
				$debug_output .= print_r( $this->db->ErrorMsg(), true ).PHP_EOL.PHP_EOL;
				
				file_put_contents ( $log_file, $debug_output, FILE_APPEND );
			}
			else {
				
				$debug_output = print_r( $this->db->ErrorMsg(), true ).PHP_EOL.PHP_EOL;
				
				file_put_contents ( $log_file, $debug_output, FILE_APPEND );
			}
			
			if ($this->do_debug=='1') {
				echo "<link href=\"/components/bootstrap/dist/css/bootstrap.min.css\" rel=\"stylesheet\">";
				echo "<pre>";
				echo $this->db->ErrorMsg();
				echo "</pre>";
				if($die=='1') die();
			}
			
		}

        
        /**
		 * Function to Login
		 *
		 * Login to application
		 *
		 * @param $username Username
		 *
         * @param $password Password
		 *
         * @param $str_label Passing label
		 *
		 * @return void
		*/
        
		public function do_login( $username, $password, $str_label ) {

			$error = "";
			$login_status = true;
			$_sp = "appSpasi2016";
			if(!trim($username)) $error .= "<li>".$str_label["MSG_EMPTY_USERNAME_WARNING"]."</li>";
			if(!trim($password)) $error .= "<li>".$str_label["MSG_EMPTY_PASSWORD_WARNING"]."</li>";

			// Posted data not complete
			if($error) {
				$this->template->box($str_label["MSG_REQUIRED_DATA_NOT_COMPLETE"], $error, array("0"=>array($_SERVER["PHP_SELF"],$str_label["LBL_BACK"])),"error");
				$login_status = false;
			}

			// Get user detail
			$user_detail = $this->session->get_user_details_db($username);

			// Verify user status
			if ($user_detail["status"] != '1' && $login_status==true) {
				$this->template->box($str_label["LBL_ERROR"], $str_label["MSG_CAN_NOT_ACCESS_STATUS_INACTIVE"], array("0"=>array($_SERVER["PHP_SELF"],$str_label["LBL_BACK"])),"error");	
				$login_status = false;
			}
            
            // Get user hashed password
			$hashed_password = $this->session->get_app_password($username);
			$sp = $_sp;
            
			// Password verification process
			$verify_password = $this->session->app_password_verify ( $password, $hashed_password, $sp );
			if ($verify_password == false  && $login_status==true) {
				// Wrong password or username
				$this->template->box($str_label["LBL_ERROR"], $str_label["MSG_NOT_REGISTERED_OR_WRONG_USERNAME_PASSWORD"], array("0"=>array($_SERVER["PHP_SELF"],$str_label["LBL_BACK"])),"error");	
				$login_status = false;

				//Get login counter
				$sql="select counter from app_user where username='$username'";
				$counter=$this->db->GetOne($sql);

				if ($counter<2) {
					// Set login counter
					$sql="update app_user set counter=counter+1 where username='$username'";
					$res_counter=$this->db->Execute($sql);
					if (!$res_counter) echo $this->db->ErrorMsg();
				}
				else {
					// Block user
					$sql="update app_user set status='0' where username='$username'";
					$res_block=$this->db->Execute($sql);
					if (!$res_block) echo $this->db->ErrorMsg();
				}

			}

			
			if ($login_status == true) {
				
				// Get Function and Inquiry Access data
				$sql="select function_access, inquiry_access from app_user where username='$username'";
				$row_access=$this->db->GetRow($sql);
				if (is_array($row_access)){ foreach($row_access as $key=>$val) {
				   $key=strtolower($key);
				   $$key=$val;
				   $user_detail[$key] = $val;
				}}
				
				// Set session
				$session_id = $this->session->generate_session_id();
				$this->session->set_session_id( $session_id );
				$this->session->set_session ( $session_id, $username, json_encode($user_detail) );
                
                $sql = "update app_user set counter='0' where username='$username'";
                $this->debug($sql,1);
                $res_update=$this->db->Execute($sql);
                if (!$res_update) echo $this->db->ErrorMsg();

				// Redirect
				$this->redirect( 1 ,"/index/p/0/" ,$str_label["LBL_REDIRECTING"] );

			}



		}
        
        /**
		 * Function to logout
		 *
		 * Logout from application
		 *
		 * @param $session_id Session ID
		 *
		 * @return void
		*/
        
        public function do_logout( $session_id ) {
            
            $sql = "delete from app_session where session_id='$session_id'";
            $res_delete=$this->db->Execute($sql);
            if (!$res_delete) echo $this->db->ErrorMsg();
            
            session_destroy();
            
        }
		
		
		
		public function get_function_access( $type, $script ) {
			
			$user_detail = $this->session->get_user_details_session($this->session->get_session_id());
				
			$sql="select function_access from app_user where username='".@$user_detail->username."'";
			$function_access=$this->db->GetOne($sql);
			
			$sql="select men_id from app_menu where url='$script/p/0/'";
			$men_id=$this->db->GetOne($sql);
			
			$sql="select ".$type."_priv from app_function_access where name='$function_access' and men_id='$men_id'";
			$priv=$this->db->GetOne($sql);
			
			if ($priv=='1') return true;
			else return false;
			
		}
		
		
		/**
		 * Function Generate Menu
		 *
		 * Generate Menu
		 *
		 * @param $men_level Level
		 *
		 * @param $unser Under
		 *
		 * @return void
		*/
		
		public function parse_menu($men_level, $under) {
			global $i, $j;

			if (!isset($i)) $i=0;
			if (!isset($j)) $j=0;
			++$i;
			$resultname = "result$i";
			$resultcheck = "resultcheck$i";
			$rowcheck = "rowcheck$i";

			$strSQL			= "SELECT * from app_menu WHERE menu_level='$men_level' and reference='$under' and show='1' and isnull(is_delete,0)!='1' order by weight asc";
			$$resultname	= $this->db->Execute($strSQL);
			if (!$$resultname) { print $this->db->ErrorMsg(); die($strSQL); }

			if ($men_level>1) echo PHP_EOL."<ul class=\"sub-menu\">".PHP_EOL;
			
			// Display Query
			while ($row = $$resultname->FetchRow()){
				if (is_array($row)){ foreach($row as $key=>$val) {
					$key=strtolower($key);
					$$key=$val;
				}}	

				/*$sql="select label as men_judul, label as men_keterangan from wf_label where lang='$active_lang' and feature='MENU' and ref_id='$men_nomorurut'";
				if ($debug) $f->pre($sql);
				$row_label=$db->GetRow($sql);
				if (is_array($row_label)){ foreach($row_label as $key=>$val) {
					$key=strtolower($key);
					$$key=$val;
				}}*/
				
				$user_detail = $this->session->get_user_details_session($this->session->get_session_id());
				
				$sql="select function_access from app_user where username='".$user_detail->username."'";
				$function_access=$this->db->GetOne($sql);
				
				$sql="select read_priv from app_function_access where name='$function_access' and men_id='$men_id'";
				$$resultcheck=$this->db->Execute($sql);
				if(!$$resultcheck) print $this->db->ErrorMsg();
				$rowcheck=$$resultcheck->FetchRow();

				$fua_read=$rowcheck['read_priv'];
				
				if($fua_read=='1'){

					unset($_target, $_image, $_url_1, $_url_2);
					$_target = "";
					$_image = "";
					if ($target) $_target = " target=\"$target\" ";
					if ($image) $_image = "<i class=\"fa fa-$image\"></i>";
					$_arrow = "<b class=\"caret pull-right\"></b>";
					
					// Check Menu has sub
					$num_child = 0;
					$sql="select count(*) as num_child from app_menu where reference='$men_id' and show='1' and isnull(is_delete,0)!='1'";
					$num_child=$this->db->GetOne($sql);
					
					$_has_sub = "";
					if ($num_child>0) $_has_sub = "class=\"has-sub\"";
					
					
					if (trim($url)) {
						if ($target=='_top') {
							$_url_1 = "<a href=\"$url\" title=\"$remark\" $_target>";
							$_url_2 = "</a>";                    
						} else {
							$_url_1 = "<a href=\"javascript:;\" title=\"$remark\" onclick=\"addTab('$title','$url')\">";
							$_url_2 = "</a>";                    
						}
					}
					else {
						$_url_1 = "<a href=\"javascript:;\">";
						$_url_2 = "</a>";                    
					}
					if ($num_child>0) echo "<li $_has_sub>$_url_1 $_image <span>$title</span> $_arrow $_url_2";
					else echo "<li>$_url_1 $_image <span>$title</span> $_url_2";

				}

				++$j;
				$resultname2 = "result2level$j";
				$checklevel = $men_level + 1;

				$strSQL			= "SELECT * from app_menu WHERE menu_level='$checklevel' and reference='$men_id'";
				$$resultname2	= $this->db->Execute($strSQL);
				if (!$$resultname2) print $this->db->ErrorMsg();

				if ($$resultname2->RecordCount() > 0) {
					$this->parse_menu($checklevel, $men_id);
				}

				if($fua_read=='1') echo "</li>".PHP_EOL;
			}
			if ($men_level>1) echo "</ul>".PHP_EOL;
		}
		
		
		
		
		public function check_access () {
			
			global $idle_time_before_loggedout;
			
			$redirect_script = "<body onLoad=\"setTimeout(function(){ window.top.location.href='/index/p/0/?act=logout'; }, 2000)\">"; 
			
			// Check if session id exist
			$session_id = $this->session->get_session_id();
			if (!$session_id) {
				echo $redirect_script;
				$this->template->box("Invalid Session", "Your session is not exist, please re-login", "", "error");
				die();
			}
			
			// Check if session id exist in APP_SESSION
			$username = "";
			$ip = "";
			$last_access = "";
			
			$sql="select username, ip, ".$this->date->datetime_from_db('last_access')." from app_session where session_id='$session_id'";
			$row=$this->db->GetRow($sql);
			if (is_array($row)){ foreach($row as $key=>$val) {
			   $key=strtolower($key);
			   $$key=$val;
			}}
			
			if (!$username) {
				echo $redirect_script;
				$this->template->box("Invalid Session", "Your session is invalid, please re-login", "", "error");
				die();
			}
			
			// Check if last access from the same ip 
			/*if ($ip != $_SERVER['REMOTE_ADDR']) {
				echo $redirect_script;
				$this->template->box("Invalid Session", "Your ip is different from your last access, please re-login", "", "error");
				die();
			}*/
			
			// Check if idle more than allowed
			
			$epoc_last_access = strtotime($last_access);
			$epoc_time_now = strtotime(date("Y-m-d H:i:s"));
			$time_since_last_access = $epoc_time_now - $epoc_last_access;
			
			if ( ($time_since_last_access/60)>$idle_time_before_loggedout ) {
				echo $redirect_script;
				$this->template->box("Session Timeout", "Your session is expired, please re-login", "", "error");
				die();
			}
			
			$sql = "update app_session set last_access=".$this->date->datetime_to_db(date('Y-m-d H:i:s'))." where session_id='$session_id'";
			$res_update_sess=$this->db->Execute($sql);
			if (!$res_update_sess) $this->debug_query(1);
			
		}
		
		
		
		/**
		 * Function to generate tabs
		 *
		 * Process the $config['tabs'] into tabs on top of the page
		 *
		 * @param $config array page configuration
		 *
		 * @return string tabs
		*/		
		
		public function generate_tabs ( $config ) {
			
			$output = "";
			foreach ($config['tabs'] as $key => $value) {
				
				$active = "";
				if (@$value['active'] == '1') $active = "active";
				
				$link = "";
				if (@$value['link']) $link = " href=\"".$value['link']."\" ";
				
				$output .= "<a ".$link." class=\"btn btn-white $active\">".$value['name']."</a>";
			}
			
			return $output;
			
		}
        
	}

}

?>