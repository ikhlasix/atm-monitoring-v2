<?php

if(!function_exists('class_auto_loader')){
    function class_auto_loader($class){
        $class=strtolower($class);
        $class_file=$_SERVER['DOCUMENT_ROOT'].'/'.$class.'.php';
        if(is_file($class_file)&&!class_exists($class)) include $class_file;
    }
}
spl_autoload_register('class_auto_loader');

$template = new classes\application\template;
$date = new classes\application\date;
$language = new classes\application\language($default_lang);
$session = new classes\application\session ($session_id_name, $db, $template);
$log = new classes\application\log($session, $db);
$page = new classes\application\page($session, $db, $log, $template, $date);
$form = new classes\application\form($db, $page, $date);


?>