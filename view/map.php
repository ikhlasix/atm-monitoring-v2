<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
  <meta charset="utf-8" />
  <title>ATM Monitoring | Dashboard</title>
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" /> 
  <meta content="" name="description" />
  <meta content="" name="author" />

  <!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<link href="/assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet" />
	<link href="/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
	<link href="/assets/css/animate.min.css" rel="stylesheet" />
	<link href="/assets/css/style.min.css" rel="stylesheet" />
	<link href="/assets/css/style-responsive.min.css" rel="stylesheet" />
	<link href="/assets/css/theme/default.css" rel="stylesheet" id="theme" />
  <!-- ================== END BASE CSS STYLE ================== -->
  
  <!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
  <link href="/assets/plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" />
  <link href="/assets/plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" />
  <link href="/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" />
  <link href="/assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" />
  <!-- ================== END PAGE LEVEL STYLE ================== -->
  
  <!-- ================== BEGIN BASE JS ================== -->
  <!-- <script src="assets/plugins/pace/pace.min.js"></script> -->
  <script src="http://maps.google.com/maps/api/js?sensor=false" 
      type="text/javascript"></script>
  <!-- ================== END BASE JS ================== -->


</head>
<body onload="loadScript();">
  <!-- begin #page-loader -->
  <!-- <div id="page-loader" class="fade in"><span class="spinner"></span></div> -->
  <!-- end #page-loader -->
  
  <!-- begin #page-container -->
  <!-- <div id="page-container" class="fade page-sidebar-fixed page-header-fixed"> -->
    <!-- begin #header -->
    
    <!-- end #header -->
    
    <!-- begin #sidebar -->
    
    <!-- end #sidebar -->
    
    <!-- begin #content --> 
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="tab-content">
            <div class="tab-pane active" id="nav-pills-tab-2">
              <!-- <div class="col-md-12 ui-sortable"> -->
                <div class="row">
                  <div class="col-md-12">
                    <div class="panel panel-default">
                      <div class="panel-body"> 
                      	<div id="map" style="width: 100%; height: 400px;"></div>
                        <div id="detail"></div>
                      </div>
                    </div>
                  </div>
                </div>
              <!-- </div> -->
            </div>
          </div>
        </div>  
      </div>
    <!-- end #content -->   
    <!-- Modal -->

    <!-- End Modal -->
    <!-- begin scroll to top btn -->
    <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
    <!-- end scroll to top btn -->
  </div>
  <!-- end page container -->
  
  <!-- ================== BEGIN BASE JS ================== -->
  <script src="/assets/plugins/jquery/jquery-1.9.1.min.js"></script>
  <script src="/assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
  <script src="/assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
  <script src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
  <!--[if lt IE 9]>
    <script src="assets/crossbrowserjs/html5shiv.js"></script>
    <script src="assets/crossbrowserjs/respond.min.js"></script>
    <script src="assets/crossbrowserjs/excanvas.min.js"></script>
  <![endif]-->
  <script src="/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
  <script src="/assets/plugins/jquery-cookie/jquery.cookie.js"></script>
  <!-- ================== END BASE JS ================== -->
  
  <!-- ================== BEGIN PAGE LEVEL JS ================== -->
  <script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script src="/assets/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
	<script src="/assets/plugins/flot/jquery.flot.min.js"></script>
	<script src="/assets/plugins/flot/jquery.flot.time.min.js"></script>
	<script src="/assets/plugins/flot/jquery.flot.resize.min.js"></script>
	<script src="/assets/plugins/flot/jquery.flot.pie.min.js"></script>
	<script src="/assets/plugins/sparkline/jquery.sparkline.js"></script>
	<script src="/assets/plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script src="/assets/plugins/jquery-jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script src="/assets/js/form-plugins.demo.min.js"></script>
	<script src="/assets/js/apps.min.js"></script>	
  <!-- ================== END PAGE LEVEL JS ================== -->


  <!-- ================== END PAGE LEVEL JS ================== -->
  
  <script>
    $(document).ready(function() {
      App.init();
      FormPlugins.init();
    });
  </script>
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','../../../../www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-53034621-1', 'auto');
    ga('send', 'pageview');     
  </script>

<script type="text/javascript">
  function get_map () {
    $.getJSON('http://103.253.107.18:31811/modules/map/data-map.php?act=get_map', {}, function(json, textStatus) {
        initialize(json);
    });
  }

  t = setInterval(get_map,60000);

  function initialize(locations) {
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 5,
      center: new google.maps.LatLng(-1.099550, 118.178278),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map,
        icon: locations[i][4]
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          get_detail(locations[i][3]);
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
}

function get_detail (id) {
  $.ajax({
    url: 'http://103.253.107.18:31811/modules/map/data-map.php',
    type: 'GET',
    dataType: 'json',
    data: {act: 'get_detail_map',atm_id:id},
    success:function(result) {
      $('#detail').html('<h4>ATM Information</h4><div class="row"><div class="col-md-8"><div class="table-responsive"><table class="table table-striped"> <tbody> <tr> <th style="width: 30%">ATM ID</th> <td>'+result[0]+'</td> </tr> <tr> <th style="width:30%;">Location</th> <td>'+result[1]+'</td> </tr> <tr> <th style="width:30%;">Last Alert</th> <td><font style="text-transform: capitalize;">'+result[2]+'</font></td> </tr> <tr> <th style="width:30%;">Date</th> <td>'+result[3]+'</td> </tr> <tr> <th style="width:30%;">Aknowledgement</th> <td>'+result[4]+'</td> </tr> </tbody> </table></div></div><div class="col-md-4"><a href="" class="thumbnail"><img src="'+result[5]+'" style="width: 75%;"></a></div></div>'); 
    }
  });
}

function loadScript() {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&' +
      'callback=get_map';
  document.body.appendChild(script);
}
</script>

</body>
</html>
