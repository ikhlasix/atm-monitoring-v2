<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
    <meta charset="utf-8" />
    <title><?=$site_title?></title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    
    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="/assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet" />
    <link href="/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="/assets/css/animate.min.css" rel="stylesheet" />
    <link href="/assets/css/style.min.css" rel="stylesheet" />
    <link href="/assets/css/style-responsive.min.css" rel="stylesheet" />
    <link href="/assets/css/theme/default.css" rel="stylesheet" id="theme" />
    <!-- ================== END BASE CSS STYLE ================== -->
    
    <!-- ================== BEGIN BASE JS ================== -->
    <script src="/assets/plugins/pace/pace.min.js"></script>
    <!-- ================== END BASE JS ================== -->
</head>
<body class="pace-top">
    <!-- begin #page-loader -->
    <div id="page-loader" class="fade in"><span class="spinner"></span></div>
    <!-- end #page-loader -->
    
    <div class="login-cover">
        <div class="login-cover-image"><img src="/assets/img/login-bg/bg-6.jpg" data-id="login-cover-image" alt="" /></div>
        <div class="login-cover-bg"></div>
    </div>
    <!-- begin #page-container -->
    <div id="page-container" class="fade">
        <!-- begin login -->
        <div class="login login-v2" data-pageload-addclass="animated fadeIn">
            <!-- begin brand -->
            <div class="login-header">
                <div class="brand">
                    <i class="fa fa-desktop" aria-hidden="true"></i>&nbsp;&nbsp;<b>ATM Monitoring</b>
                    <!--<small>Default Application v.5</small>-->
                </div>
                <!--<div class="icon">
                    <i class="fa fa-sign-in"></i>
                </div>-->
            </div>
            <!-- end brand -->
            <div class="login-content">
                <form method='post' name='f1' id='f1' action='<?=$_SERVER["PHP_SELF"]?>'>
                    <input type='hidden' name='act' value='login' />
                    <input type='hidden' name='challange' value='<?=$challenge?>'>
                    
                    <div class="form-group m-b-20">
                        <input class="form-control" placeholder="Username" name="username" autofocus required="true">
                    </div>
                    <div class="form-group m-b-20">
                        <input class="form-control" placeholder="Password" name="password" type="password" required="true">
                    </div>
                    <div class="login-buttons">
                        <button type="submit" class="btn btn-success btn-block btn-lg" onclick="login();return false;">Log in</button>
                    </div>
                    <div class="m-t-20">
                        Forget your password? Click <a href="javascript:;">here</a>.
                    </div>
                </form>
                <form method="post" name="f2" style="display:none;" id="f2">
                    <input type="hidden" name="username">
                    <input type="hidden" name="password">
                    <input type="hidden" name="act" value="login">
                </form>
            </div>
        </div>
        <!-- end login -->
        
        <ul class="login-bg-list">
            <li class="active"><a href="#" data-click="change-bg"><img src="/assets/img/login-bg/bg-1.jpg" alt="" /></a></li>
            <li><a href="#" data-click="change-bg"><img src="/assets/img/login-bg/bg-2.jpg" alt="" /></a></li>
            <li><a href="#" data-click="change-bg"><img src="/assets/img/login-bg/bg-3.jpg" alt="" /></a></li>
            <li><a href="#" data-click="change-bg"><img src="/assets/img/login-bg/bg-4.jpg" alt="" /></a></li>
            <li><a href="#" data-click="change-bg"><img src="/assets/img/login-bg/bg-5.jpg" alt="" /></a></li>
            <li><a href="#" data-click="change-bg"><img src="/assets/img/login-bg/bg-6.jpg" alt="" /></a></li>
        </ul>
        
    </div>
    <!-- end page container -->
    
    <!-- ================== BEGIN BASE JS ================== -->
    <script src="/assets/plugins/jquery/jquery-1.9.1.min.js"></script>
    <script src="/assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
    <script src="/assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
    <script src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!--[if lt IE 9]>
        <script src="/assets/crossbrowserjs/html5shiv.js"></script>
        <script src="/assets/crossbrowserjs/respond.min.js"></script>
        <script src="/assets/crossbrowserjs/excanvas.min.js"></script>
    <![endif]-->
    <script src="/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="/assets/plugins/jquery-cookie/jquery.cookie.js"></script>
    <script src="/assets/js/aes.js"></script>
    <!-- ================== END BASE JS ================== -->
    
    <!-- ================== BEGIN PAGE LEVEL JS ================== -->
    <script src="/assets/js/login-v2.demo.min.js"></script>
    <script src="/assets/js/apps.min.js"></script>
    <!-- ================== END PAGE LEVEL JS ================== -->

    <script>
        $(document).ready(function() {
            App.init();
            LoginV2.init();
        });
        
        function login() {
            if (document.f1.username.value == "") {
                $(".modal-body").html('<?=$str_label["MSG_EMPTY_USERNAME_WARNING"]?>');
                $("#myModal").modal();
                return false;
            }else if (document.f1.password.value == "") {
                $(".modal-body").html('<?=$str_label["MSG_EMPTY_PASSWORD_WARNING"]?>');
                $("#myModal").modal();
                return false;
            }else {
                var loginForm = document.getElementById("f1");

                var key = CryptoJS.enc.Hex.parse("<?=$key?>");
                var iv =  CryptoJS.enc.Hex.parse("<?=$iv?>");
            
                var submitForm = document.getElementById("f2");
                document.f2.username.value = document.f1.username.value;

                var password_hash = CryptoJS.AES.encrypt(document.f1.password.value, key, {iv:iv});
                document.f2.password.value 	= password_hash.ciphertext.toString(CryptoJS.enc.Base64);

                document.f2.submit();
            }
        }
    </script>
    
</body>

</html>