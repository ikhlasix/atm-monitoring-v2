<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" style="height:100%">
<!--<![endif]-->

<head>
	<meta charset="utf-8" />
	<title><?=$site_title?></title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	
	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
	<link href="/assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet" />
	<link href="/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="/assets/plugins/easyui/themes/bootstrap/easyui.css" rel="stylesheet">
	<link href="/assets/plugins/easyui/themes/icon.css" rel="stylesheet">
	<link href="/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
	<link href="/assets/css/animate.min.css" rel="stylesheet" />
	<link href="/assets/css/style.min.css" rel="stylesheet" />
	<link href="/assets/css/style-responsive.min.css" rel="stylesheet" />
	<link href="/assets/css/theme/default.css" rel="stylesheet" id="theme" />
	<!-- ================== END BASE CSS STYLE ================== -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="/assets/plugins/pace/pace.min.js"></script>
	<!-- ================== END BASE JS ================== -->
</head>
<body>
	<!-- begin #page-loader -->
	<div id="page-loader" class="fade in"><span class="spinner"></span></div>
	<!-- end #page-loader -->
	
	<!-- begin #page-container -->
	<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
		<!-- begin #header -->
		<div id="header" class="header navbar navbar-default navbar-fixed-top">
			<!-- begin container-fluid -->
			<div class="container-fluid">
				<!-- begin mobile sidebar expand / collapse button -->
				<div class="navbar-header">
					<a href="/index/p/0/" class="navbar-brand"><!--<span class="navbar-logo"></span>-->
						<i class="fa fa-desktop" aria-hidden="true"></i>&nbsp;&nbsp;<b>ATM Monitoring</b>
					</a>
					<button type="button" class="navbar-toggle" data-click="sidebar-toggled">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<!-- end mobile sidebar expand / collapse button -->
				
				<!-- begin header navigation right -->
				<ul class="nav navbar-nav navbar-right">
					<!-- <li class="dropdown">
						<a href="javascript:;" data-toggle="dropdown" class="dropdown-toggle f-s-14">
							<i class="fa fa-bell-o"></i>
							<span class="label">0</span>
						</a>
						<ul class="dropdown-menu media-list pull-right animated fadeInDown">
                            <li class="dropdown-header">Notifications (5)</li>
                            <li class="media">
                                <a href="javascript:;">
                                    <div class="media-left"><i class="fa fa-bug media-object bg-red"></i></div>
                                    <div class="media-body">
                                        <h6 class="media-heading">Server Error Reports</h6>
                                        <div class="text-muted f-s-11">3 minutes ago</div>
                                    </div>
                                </a>
                            </li>
                            <li class="media">
                                <a href="javascript:;">
                                    <div class="media-left"><img src="assets/img/user-1.jpg" class="media-object" alt="" /></div>
                                    <div class="media-body">
                                        <h6 class="media-heading">John Smith</h6>
                                        <p>Quisque pulvinar tellus sit amet sem scelerisque tincidunt.</p>
                                        <div class="text-muted f-s-11">25 minutes ago</div>
                                    </div>
                                </a>
                            </li>
                            <li class="media">
                                <a href="javascript:;">
                                    <div class="media-left"><img src="assets/img/user-2.jpg" class="media-object" alt="" /></div>
                                    <div class="media-body">
                                        <h6 class="media-heading">Olivia</h6>
                                        <p>Quisque pulvinar tellus sit amet sem scelerisque tincidunt.</p>
                                        <div class="text-muted f-s-11">35 minutes ago</div>
                                    </div>
                                </a>
                            </li>
                            <li class="media">
                                <a href="javascript:;">
                                    <div class="media-left"><i class="fa fa-plus media-object bg-green"></i></div>
                                    <div class="media-body">
                                        <h6 class="media-heading"> New User Registered</h6>
                                        <div class="text-muted f-s-11">1 hour ago</div>
                                    </div>
                                </a>
                            </li>
                            <li class="media">
                                <a href="javascript:;">
                                    <div class="media-left"><i class="fa fa-envelope media-object bg-blue"></i></div>
                                    <div class="media-body">
                                        <h6 class="media-heading"> New Email From John</h6>
                                        <div class="text-muted f-s-11">2 hour ago</div>
                                    </div>
                                </a>
                            </li>
                            <li class="dropdown-footer text-center">
                                <a href="javascript:;">View more</a>
                            </li>
						</ul>
					</li> -->
					<li class="dropdown navbar-user">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
							<i class="fa fa-user"></i> <b class="caret"></b>
						</a>
						<ul class="dropdown-menu animated fadeInLeft">
							<li class="arrow"></li>
							<!-- <li><a href="javascript:;">Edit Profile</a></li>
							<li><a href="javascript:;">Setting</a></li>
							<li class="divider"></li> -->
                            <?php 
                        
                            $param = $page->set_parameter ( array("act"=>"logout"), $user_detail->challenge );
        
                            ?>
							<li><a href="/index/p/<?=$param?>/">Log Out</a></li>
						</ul>
					</li>
				</ul>
				<!-- end header navigation right -->
			</div>
			<!-- end container-fluid -->
		</div>
		<!-- end #header -->
		
		<!-- begin #sidebar -->
		<div id="sidebar" class="sidebar">
			<!-- begin sidebar scrollbar -->
			<div data-scrollbar="true" data-height="100%">
				<!-- begin sidebar user -->
				<ul class="nav">
					<li class="nav-profile">
                        <div class="row">
                            <div class="image">
                                <a href="javascript:;"><img src="/assets/img/user-default-avatar.jpg" alt="" /></a>
                            </div>
                            <div class="info col-md-9">
                                <?=$user_detail->first_name?> <?=$user_detail->last_name?>
                                <small><?=$user_detail->username?></small>
                                <small><?=$user_detail->function_access?></small>
                            </div>
                        </div>
					</li>
				</ul>
				<!-- end sidebar user -->
				<!-- begin sidebar nav -->
				<ul class="nav">
					<li class="nav-header">Menu</li>
					
                    <?php $page->parse_menu(1,''); ?>
			        
                    <!-- begin sidebar minify button -->
					<li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
			        <!-- end sidebar minify button -->
				</ul>
				<!-- end sidebar nav -->
			</div>
			<!-- end sidebar scrollbar -->
		</div>
		<div class="sidebar-bg"></div>
		<!-- end #sidebar -->
		
		<!-- begin #content -->
		<div id="content" class="content easyui-tabs">
		</div>
		<!-- end #content -->
        
        <!-- begin #footer -->
		<div id="footer" class="footer pull-right">
		    &copy; <?=date('Y')?> Spasi Indonesia All Rights Reserved
		</div>
		<!-- end #footer -->
		
		
	</div>
	<!-- end page container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="/assets/plugins/jquery/jquery-1.9.1.min.js"></script>
	<script src="/assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
	<script src="/assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
	<script src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="/assets/plugins/easyui/jquery.easyui.min.js"></script>	
	<!--[if lt IE 9]>
		<script src="/assets/crossbrowserjs/html5shiv.js"></script>
		<script src="/assets/crossbrowserjs/respond.min.js"></script>
		<script src="/assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
	<script src="/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="/assets/plugins/jquery-cookie/jquery.cookie.js"></script>
	<!-- ================== END BASE JS ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="/assets/js/apps.min.js"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->
	
	<script>
		$(document).ready(function() {
			App.init();
			
			$( window ).resize(function() {
				
                if ($(window).width()<=768) {
					$('.content').height($(window).height() - 190);
				} else {
					$('.content').height($(window).height() - 115);
				}
				
				$('.tabs-header').width($('.content').width());
				$('.tabs-panels').width($('.content').width());
				$('.panel').width($('.tabs-panels').width());
				$('.panel-body').width($('.tabs-panels').width());
				
				$('.panel-body').height($('.content').height());
				$('iframe').height($('.content').height() - 55);
				$('iframe').width($('.content').width() - 18);
            });
			
			$('.content').tabs({
				onClose: function(title){
					
					$('.content').height($(window).height() - 145);
					$('.panel').height($(window).height() - 140);
					$('.panel-body').height($('.content').height());
					
					$('iframe').height($('.content').height() - 30);
					$('iframe').width($('.content').width() - 20);
					
				}
			});
            
			addTab('Home', '/modules/alert/alert/p/0/');
			
		});
		
        function addTab(title, url){  
			
			var frame_id = "";
			frame_id = url.replace(/\//g, '');
			
			if ($('#content').tabs('exists', title)){
				 
                var content = '<iframe name="main" id="' + frame_id + '" frameborder=0 src="'+url+'"></iframe>';
				$('#content').tabs('select', title);  
				var tab = $('#tt').tabs('getSelected'); 
				$('#content').tabs('update', {
					tab: tab,
					options: {
						title: title,
						content:content
					}
				});
					
			} else { 
				
				var content = '<iframe name="main" id="' + frame_id + '" frameborder=0 src="'+url+'"></iframe>';
					$('#content').tabs('add',{  
					title:title,  
					content:content,  
					closable:true,
					fit:true, 
					border:false, 
					plain:true
									
				});  
			}   
			
			if ($(window).width()<=768) {
				$('.content').height($(window).height() - 190);
			} else {
				$('.content').height($(window).height() - 115);
			}
			
			$('.tabs-header').width($('.content').width());
			$('.tabs-panels').width($('.content').width());
			$('.panel').width($('.tabs-panels').width());
			$('.panel-body').width($('.tabs-panels').width());
				
            $('.panel-body').height($('.content').height());
			$('iframe').height($('.content').height() - 55);
			$('iframe').width($('.content').width() - 18);
			
			console.log("panel:" + $('.panel').height());
		}  
		function closeTab(title){  
			if ($('#content').tabs('exists', title)){ 
				$('#content').tabs('close', title);  
			}
		}
	</script>

</body>

</html>

