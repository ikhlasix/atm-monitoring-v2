<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
	<meta charset="utf-8" />
	<title><?=$config['page_title']?></title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	
	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
	<link href="/assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet" />
	<link href="/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="/assets/plugins/easyui/themes/bootstrap/easyui.css" rel="stylesheet">
	<link href="/assets/plugins/easyui/themes/icon.css" rel="stylesheet">
	<link href="/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
	<link href="/assets/css/animate.min.css" rel="stylesheet" />
	<link href="/assets/css/style.min.css" rel="stylesheet" />
	<link href="/assets/css/style-responsive.min.css" rel="stylesheet" />
	<link href="/assets/css/theme/default.css" rel="stylesheet" id="theme" />
	<!-- ================== END BASE CSS STYLE ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
	<link href="/assets/plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" />
	<link href="/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" />
    <link href="/assets/plugins/bootstrap-fileinput/css/fileinput.min.css" rel="stylesheet" />
    <link href="/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
	<link href="/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
    <link href="/assets/plugins/bootstrap-wysihtml5/src/bootstrap-wysihtml5.css" rel="stylesheet" />
    <link href="/assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="/assets/plugins/bootstrap-eonasdan-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
	<!-- ================== END PAGE LEVEL STYLE ================== -->
	
</head>
<body class="body-frame">
	<!-- begin #page-loader -->
	<div id="page-loader" class="fade in"><span class="spinner"></span></div>
	<!-- end #page-loader -->
	
	<!-- begin #page-container -->
	<div id="page-container" class="fade">
		
		<!-- begin #content -->
		<div id="content" class="content-full">
			
            <!-- begin row -->
			<div class="row">
			    <!-- begin col-12 -->
			    <div class="col-md-12">
                    
                    <!-- begin tabs -->
                    <div class="btn-group btn-group" style="margin-bottom:10px;">
                        <?=@$tabs?>
                    </div>
                    <!-- end tabs -->
                    
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <?=@$add_button?> 
                                <button type="button" class="btn btn-primary btn-xs button-add" onClick="location.reload();"><i class="fa fa-repeat"></i> <?=$str_label["LBL_REFRESH"]?></button>
                            </div>
                            <h4 class="panel-title"><?=$config['page_title']?></h4>
                        </div>
                        <div class="panel-body">
                            
                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-body"></div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                            
                            <?=$main_content?>
                            
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
		</div>
		<!-- end #content -->
		
		<!-- begin scroll to top btn -->
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
		<!-- end scroll to top btn -->
	</div>
	<!-- end page container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="/assets/plugins/jquery/jquery-1.9.1.min.js"></script>
	<script src="/assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
	<script src="/assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
	<script src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="/assets/plugins/easyui/jquery.easyui.min.js"></script>	
	<!--[if lt IE 9]>
		<script src="/assets/crossbrowserjs/html5shiv.js"></script>
		<script src="/assets/crossbrowserjs/respond.min.js"></script>
		<script src="/assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
	<script src="/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="/assets/plugins/jquery-cookie/jquery.cookie.js"></script>
    <script src="/assets/plugins/jquery-cookie/jquery.cookie.js"></script>
	<!-- ================== END BASE JS ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script src="/assets/plugins/bootstrap-fileinput/js/fileinput.min.js"></script>
	<script src="/assets/plugins/bootstrap-wysihtml5/lib/js/wysihtml5-0.3.0.js"></script>
	<script src="/assets/plugins/bootstrap-wysihtml5/src/bootstrap-wysihtml5.js"></script>
    <script src="/assets/js/form-wysiwyg.demo.min.js"></script>
    <script src="/assets/plugins/masked-input/masked-input.min.js"></script>
	<script src="/assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
	<script src="/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
	<script src="/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
	<script src="/assets/js/table-manage-default.demo.min.js"></script>
	<script src="/assets/js/apps.min.js"></script>
    <script src="/assets/plugins/auto-numeric/autoNumeric.js"></script>
    <script src="/assets/plugins/jquery-form/jquery.form.min.js"></script>
    <script src="/assets/plugins/bootstrap-daterangepicker/moment.js"></script>
    <script src="/assets/plugins/select2/dist/js/select2.min.js"></script>
    <script src="/assets/plugins/jquery-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <script src="/assets/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->
	
	<script>
		$(document).ready(function() {
			App.init();
			TableManageDefault.init();
            FormWysihtml5.init();
		});
	</script>
    <script>
        function show_next( element ) {
            
            window[element + '_num']++;
            $('.' + element + window[element + '_num']).show();
            
        }
        
        function check_row( men_id ) {
        
            $('#read_' + men_id).prop('checked', true);
            $('#add_' + men_id).prop('checked', true);
            $('#edit_' + men_id).prop('checked', true);
            $('#delete_' + men_id).prop('checked', true);
            
        }
        
        function uncheck_row( men_id ) {
            
            $('#read_' + men_id).prop('checked', false);
            $('#add_' + men_id).prop('checked', false);
            $('#edit_' + men_id).prop('checked', false);
            $('#delete_' + men_id).prop('checked', false);
            
        }
        
        $(document).ready(function() {
            
            $('#f1').ajaxForm(function(msg) { 
                
                $("#myModal").modal('show');
                var IS_JSON = true;
                try { var response = jQuery.parseJSON(msg);
                } catch(err) { IS_JSON = false; }
                if (IS_JSON) {
                    $(".modal-body").html(response.message);
                    if (response.status=='success') {
                        setInterval(function(){ 
                            location.href="<?=$config['script']?>/p/0/";
                        }, 1000);
                    }
                }
                else {
                    $(".modal-body").html(msg)	
                }
            });
            
            $('#dataTables').dataTable( {
                "processing": true,
                "serverSide": true,
                "order": [[ <?=$config['data_tables_default_sort_column']?>, "<?=$config['data_tables_default_order']?>" ]],
                "ajax": "?act=list"
            } );
            
            $('.input-group.date.multi').datepicker({
                format: "dd/mm/yyyy",
                multidate: true,
                multidateSeparator: ","
            });
            
            $('.input-group.date').datepicker({
                format: "dd/mm/yyyy"
            });
            
            $('.input-group.datetime').datetimepicker({
                format: "DD/MM/YYYY HH:mm"
            });
            
            $('.numeric').autoNumeric('init');
            
            $('.masked-input-date').mask("99/99/9999");
            
            $('.masked-input-datetime').mask("99/99/9999 99:99");
            
            $(".select2").select2();
            
            <?php echo $form->generate_additional_jquery( $config ); ?>
            
        } );
    </script>
</body>

</html>

