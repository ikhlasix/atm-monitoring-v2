<?php

// Login Process
$str_label["MSG_EMPTY_USERNAME_WARNING"] = "Please fill in username.";
$str_label["MSG_EMPTY_PASSWORD_WARNING"] = "Please fill in password.";
$str_label["MSG_REQUIRED_DATA_NOT_COMPLETE"] = "Required data not complete";
$str_label["MSG_INVALID_PARAMETER"] = "Invalid Parameter";
$str_label["MSG_NOT_REGISTERED_OR_WRONG_USERNAME_PASSWORD"] = "You are not registered or your username/password wrong.";
$str_label["MSG_CAN_NOT_ACCESS_STATUS_INACTIVE"] = "You can not access this application, your status is inactive or blocked";
$str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"] = "you don't have privilege to access this page";
$str_label["MSG_FIELDS_MANDATORY"] = "(*) means field is mandatory";
$str_label["MSG_ARE_YOU_SURE_TO_DELETE_THIS_DATA"] = "Are you sure you want to delete this data?";
$str_label["MSG_EXTENSION_NOT_ALLOWED"] = "File extension is not allowed, please choose another file";
$str_label["MSG_UPLOAD_FAILED"] = "Upload failed, please try again";
$str_label["MSG_GENERATING_REPORT"] = "Please wait, generating report ..";

$str_label["LBL_ILLEGAL_ACCESS"] = "Illegal Access";
$str_label["LBL_ADD"] = "Add";
$str_label["LBL_REFRESH"] = "Refresh";
$str_label["LBL_EDIT"] = "Edit";
$str_label["LBL_DELETE"] = "Delete";
$str_label["LBL_VIEW"] = "View";
$str_label["LBL_BACK"] = "Back";
$str_label["LBL_ERROR"] = "Error";
$str_label["LBL_DUPLICATE"] = "Duplicate";
$str_label["LBL_REDIRECTING"] = "Please wait while the page is redirecting.";
$str_label["LBL_KEEP"] = "Keep";
$str_label["LBL_REMOVE"] = "Remove";

?>