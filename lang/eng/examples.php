<?php

// Basic CRUD
$str_label["LBL_NAME"] = "Name";
$str_label["LBL_DATE"] = "Date";
$str_label["LBL_DATETIME"] = "Date Time";
$str_label["LBL_MULTI_DATE"] = "Multi Date";
$str_label["LBL_NUMERIC"] = "Numeric";
$str_label["LBL_CURRENCY"] = "Currency";
$str_label["LBL_TEXT"] = "Text";
$str_label["LBL_HTML"] = "Html";
$str_label["LBL_FILE"] = "File";
$str_label["LBL_MULTI_FILES"] = "Multi Files";
$str_label["LBL_MASKED_INPUT"] = "Masked Input";

?>