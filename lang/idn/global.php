<?php

$str_label["MSG_EMPTY_USERNAME_WARNING"] = "Silahkan isi username.";
$str_label["MSG_EMPTY_PASSWORD_WARNING"] = "Silahkan isi password.";
$str_label["MSG_REQUIRED_DATA_NOT_COMPLETE"] = "Data tidak lengkap";
$str_label["MSG_INVALID_PARAMETER"] = "Parameter Invalid";
$str_label["MSG_NOT_REGISTERED_OR_WRONG_USERNAME_PASSWORD"] = "Anda belum registrasi atau username/password salah.";
$str_label["MSG_CAN_NOT_ACCESS_STATUS_INACTIVE"] = "Anda tidak diperbolehkan mengakses aplikasi ini, status anda tidak aktif atau di blok";
$str_label["MSG_DONT_HAVE_PRIVILEGE_TO_ACCESS_PAGE"] = "Anda tidak mempunyai hak untuk mengakses halaman ini";
$str_label["MSG_FIELDS_MANDATORY"] = "(*) berarti field wajib diisi";
$str_label["MSG_ARE_YOU_SURE_TO_DELETE_THIS_DATA"] = "Apakah anda yakin ingin menghapus data ini?";
$str_label["MSG_EXTENSION_NOT_ALLOWED"] = "Extension ini tidak diperbolehkan, silahkan coba file lain";
$str_label["MSG_UPLOAD_FAILED"] = "Upload gagal, silahkan coba lagi";
$str_label["MSG_GENERATING_REPORT"] = "Silahkan tunggu, laporan sedang disiapkan ..";

$str_label["LBL_ILLEGAL_ACCESS"] = "Akses Ilegal";
$str_label["LBL_ADD"] = "Tambah";
$str_label["LBL_REFRESH"] = "Refresh";
$str_label["LBL_EDIT"] = "Edit";
$str_label["LBL_DELETE"] = "Hapus";
$str_label["LBL_VIEW"] = "Lihat";
$str_label["LBL_BACK"] = "Kembali";
$str_label["LBL_ERROR"] = "Error";
$str_label["LBL_DUPLICATE"] = "Duplikasi";
$str_label["LBL_REDIRECTING"] = "Silahkan tunggu.";
$str_label["LBL_KEEP"] = "Simpan";
$str_label["LBL_REMOVE"] = "Hapus";

?>