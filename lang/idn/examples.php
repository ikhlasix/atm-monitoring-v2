<?php

// Basic CRUD
$str_label["LBL_NAME"] = "Nama";
$str_label["LBL_DATE"] = "Tanggal";
$str_label["LBL_DATETIME"] = "Tanggal Waktu";
$str_label["LBL_MULTI_DATE"] = "Tanggal Multi";
$str_label["LBL_NUMERIC"] = "Nomor";
$str_label["LBL_CURRENCY"] = "Nilai";
$str_label["LBL_TEXT"] = "Text";
$str_label["LBL_HTML"] = "Html";
$str_label["LBL_FILE"] = "File";
$str_label["LBL_MULTI_FILES"] = "Multi Files";
$str_label["LBL_MASKED_INPUT"] = "Masked Input";

?>