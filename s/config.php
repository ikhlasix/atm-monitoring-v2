<?php

//Define Database Connection
$dbhostname		= "localhost";
$dbusername		= "sa";
$dbpassword		= "Spasi@2016";
$dbname			= "atm_monitoring2";

//Define Variables
$site_title		="ATM Monitoring";
$default_lang	="eng";
$session_id_name = "app_spasi_session";
$idle_time_before_loggedout = 30; // in minutes

session_start();

//Adodb
include $_SERVER["DOCUMENT_ROOT"]."/components/adodb/adodb.inc.php";

//SQL Server
$db = ADONewConnection('mssqlnative');
// $db->debug = true;
$db->PConnect($dbhostname, $dbusername, $dbpassword, $dbname);
$db->SetFetchMode(ADODB_FETCH_ASSOC);

//Autoloader
include $_SERVER["DOCUMENT_ROOT"]."/classes/autoloader.php";


//check sql injection posibility
function injection_detection($array){
	if(count($array) >= 1){
		foreach($array as $key=>$val){
			if(is_array($val)) $val=@join(",",$val);
				$val=urldecode($val);
				if(!preg_match("/\/utilitas\/fm\.php|\/payroll\/setup\/setup_grup_komponen\.php/i",$_SERVER['PHP_SELF'])){ //exclude	
					if(preg_match("/(sqlmap\.org)/i",$_SERVER['HTTP_USER_AGENT'])){
						injection_logfile($val);
						die('Illegal Access');						
					}
					if(preg_match("/\'|\"/",$val) && preg_match("/select\s|alter\s|update\s|insert\s|delete\s|drop\s|truncate\s|\sand\s|\sor\s|\<script|information_schema\.|dbms_pipe\.|sysobjects\.|sysobjects\s|xp_cmdshell\(|sys_exec\(|sp_replwritetovarbin/im",$val)){
						injection_logfile($val);
						die('Illegal Access');
					}
					
				}else{
				}
		}
	}
}

function injection_logfile($parameter_injection){
		global $_SERVER;
		global $login_nip;	
		$message_injection=date("d/m/Y H:i:s")."\t".$_SERVER['REMOTE_ADDR']."\t$login_nip\t$parameter_injection\t".$_SERVER['REQUEST_URI']."\t".$_SERVER['HTTP_USER_AGENT']."\t".$_SERVER['REQUEST_METHOD']."\t".@$_SERVER['HTTP_X_FORWARDED_FOR']."\t".gethostbyaddr($_SERVER['REMOTE_ADDR']);;
		$filename=$_SERVER['DOCUMENT_ROOT']."/i/log/injection_detection_".date("Ymd").".log";
		$fp=fopen($filename, "ab+");
		if($fp){
		        fputs($fp, stripslashes("$message_injection\r\n"));
		        fclose($fp);
		}
		//return basename($filename);
}
injection_detection($_REQUEST);
	
// Access Check
if(!preg_match("/^\/index|\/webservice\/rest/i",$_SERVER['PHP_SELF'])) $page->check_access();

?>