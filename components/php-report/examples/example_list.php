<?php

include '../PHPReport.php';

$rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
$rendererLibrary = 'mpdf';
$rendererLibraryPath = $_SERVER['DOCUMENT_ROOT'].'/components/' . $rendererLibrary;

if (!PHPExcel_Settings::setPdfRenderer(
        $rendererName,
        $rendererLibraryPath
    )) {
    die(
        'NOTICE: Please set the $rendererName and $rendererLibraryPath values' .
        EOL .
        'at the top of this script as appropriate for your directory structure'
    );
}

//which template to use
$template='list.xlsx';
	
//set absolute path to directory with template files
$templateDir=$_SERVER['DOCUMENT_ROOT'].'/components/php-report/examples/';

//we get some products, e.g. from database
$data=array(
	array('id'=>'ID-000001','name'=>'Andi','data1'=>1000,'data2'=>2000,'data3'=>4000),
	array('id'=>'ID-000002','name'=>'Wati','data1'=>5000,'data2'=>5000,'data3'=>6000),
	array('id'=>'ID-000003','name'=>'Budi','data1'=>6000,'data2'=>3000,'data3'=>3000),
	array('id'=>'ID-000004','name'=>'Amir','data1'=>3000,'data2'=>7000,'data3'=>8000),
	array('id'=>'ID-000005','name'=>'Lasti','data1'=>2000,'data2'=>6000,'data3'=>9000),
	array('id'=>'ID-000001','name'=>'Andi','data1'=>1000,'data2'=>2000,'data3'=>4000),
	array('id'=>'ID-000002','name'=>'Wati','data1'=>5000,'data2'=>5000,'data3'=>6000),
	array('id'=>'ID-000003','name'=>'Budi','data1'=>6000,'data2'=>3000,'data3'=>3000),
	array('id'=>'ID-000004','name'=>'Amir','data1'=>3000,'data2'=>7000,'data3'=>8000),
	array('id'=>'ID-000005','name'=>'Lasti','data1'=>2000,'data2'=>6000,'data3'=>9000),
	array('id'=>'ID-000001','name'=>'Andi','data1'=>1000,'data2'=>2000,'data3'=>4000),
	array('id'=>'ID-000002','name'=>'Wati','data1'=>5000,'data2'=>5000,'data3'=>6000),
	array('id'=>'ID-000003','name'=>'Budi','data1'=>6000,'data2'=>3000,'data3'=>3000),
	array('id'=>'ID-000004','name'=>'Amir','data1'=>3000,'data2'=>7000,'data3'=>8000),
	array('id'=>'ID-000005','name'=>'Lasti','data1'=>2000,'data2'=>6000,'data3'=>9000),
	array('id'=>'ID-000001','name'=>'Andi','data1'=>1000,'data2'=>2000,'data3'=>4000),
	array('id'=>'ID-000002','name'=>'Wati','data1'=>5000,'data2'=>5000,'data3'=>6000),
	array('id'=>'ID-000003','name'=>'Budi','data1'=>6000,'data2'=>3000,'data3'=>3000),
	array('id'=>'ID-000004','name'=>'Amir','data1'=>3000,'data2'=>7000,'data3'=>8000),
	array('id'=>'ID-000005','name'=>'Lasti','data1'=>2000,'data2'=>6000,'data3'=>9000),
	array('id'=>'ID-000001','name'=>'Andi','data1'=>1000,'data2'=>2000,'data3'=>4000),
	array('id'=>'ID-000002','name'=>'Wati','data1'=>5000,'data2'=>5000,'data3'=>6000),
	array('id'=>'ID-000003','name'=>'Budi','data1'=>6000,'data2'=>3000,'data3'=>3000),
	array('id'=>'ID-000004','name'=>'Amir','data1'=>3000,'data2'=>7000,'data3'=>8000),
	array('id'=>'ID-000005','name'=>'Lasti','data1'=>2000,'data2'=>6000,'data3'=>9000),
	array('id'=>'ID-000001','name'=>'Andi','data1'=>1000,'data2'=>2000,'data3'=>4000),
	array('id'=>'ID-000002','name'=>'Wati','data1'=>5000,'data2'=>5000,'data3'=>6000),
	array('id'=>'ID-000003','name'=>'Budi','data1'=>6000,'data2'=>3000,'data3'=>3000),
	array('id'=>'ID-000004','name'=>'Amir','data1'=>3000,'data2'=>7000,'data3'=>8000),
	array('id'=>'ID-000005','name'=>'Lasti','data1'=>2000,'data2'=>6000,'data3'=>9000),
	array('id'=>'ID-000001','name'=>'Andi','data1'=>1000,'data2'=>2000,'data3'=>4000),
	array('id'=>'ID-000002','name'=>'Wati','data1'=>5000,'data2'=>5000,'data3'=>6000),
	array('id'=>'ID-000003','name'=>'Budi','data1'=>6000,'data2'=>3000,'data3'=>3000),
	array('id'=>'ID-000004','name'=>'Amir','data1'=>3000,'data2'=>7000,'data3'=>8000),
	array('id'=>'ID-000005','name'=>'Lasti','data1'=>2000,'data2'=>6000,'data3'=>9000),
	array('id'=>'ID-000001','name'=>'Andi','data1'=>1000,'data2'=>2000,'data3'=>4000),
	array('id'=>'ID-000002','name'=>'Wati','data1'=>5000,'data2'=>5000,'data3'=>6000),
	array('id'=>'ID-000003','name'=>'Budi','data1'=>6000,'data2'=>3000,'data3'=>3000),
	array('id'=>'ID-000004','name'=>'Amir','data1'=>3000,'data2'=>7000,'data3'=>8000),
	array('id'=>'ID-000005','name'=>'Lasti','data1'=>2000,'data2'=>6000,'data3'=>9000),
	array('id'=>'ID-000001','name'=>'Andi','data1'=>1000,'data2'=>2000,'data3'=>4000),
	array('id'=>'ID-000002','name'=>'Wati','data1'=>5000,'data2'=>5000,'data3'=>6000),
	array('id'=>'ID-000003','name'=>'Budi','data1'=>6000,'data2'=>3000,'data3'=>3000),
	array('id'=>'ID-000004','name'=>'Amir','data1'=>3000,'data2'=>7000,'data3'=>8000),
	array('id'=>'ID-000005','name'=>'Lasti','data1'=>2000,'data2'=>6000,'data3'=>9000),
	array('id'=>'ID-000001','name'=>'Andi','data1'=>1000,'data2'=>2000,'data3'=>4000),
	array('id'=>'ID-000002','name'=>'Wati','data1'=>5000,'data2'=>5000,'data3'=>6000),
	array('id'=>'ID-000003','name'=>'Budi','data1'=>6000,'data2'=>3000,'data3'=>3000),
	array('id'=>'ID-000004','name'=>'Amir','data1'=>3000,'data2'=>7000,'data3'=>8000),
	array('id'=>'ID-000005','name'=>'Lasti','data1'=>2000,'data2'=>6000,'data3'=>9000),
	array('id'=>'ID-000001','name'=>'Andi','data1'=>1000,'data2'=>2000,'data3'=>4000),
	array('id'=>'ID-000002','name'=>'Wati','data1'=>5000,'data2'=>5000,'data3'=>6000),
	array('id'=>'ID-000003','name'=>'Budi','data1'=>6000,'data2'=>3000,'data3'=>3000),
	array('id'=>'ID-000004','name'=>'Amir','data1'=>3000,'data2'=>7000,'data3'=>8000),
	array('id'=>'ID-000005','name'=>'Lasti','data1'=>2000,'data2'=>6000,'data3'=>9000),
	array('id'=>'ID-000001','name'=>'Andi','data1'=>1000,'data2'=>2000,'data3'=>4000),
	array('id'=>'ID-000002','name'=>'Wati','data1'=>5000,'data2'=>5000,'data3'=>6000),
	array('id'=>'ID-000003','name'=>'Budi','data1'=>6000,'data2'=>3000,'data3'=>3000),
	array('id'=>'ID-000004','name'=>'Amir','data1'=>3000,'data2'=>7000,'data3'=>8000),
	array('id'=>'ID-000005','name'=>'Lasti','data1'=>2000,'data2'=>6000,'data3'=>9000),
	
);

//set config for report
$config=array(
	'template'=>$template,
	'templateDir'=>$templateDir
);


$R=new PHPReport($config);

$R->objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
$R->objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);


$R->load(array(
			array(
				'id'=>'data',
				'repeat'=>true,
				'data'=>$data,
				'minRows'=>1,
				),
			array(
				'id'=>'total',
				'data'=>array('data3'=>12000),
				)
			)
        );
//we can render html, excel, excel2003 or PDF
echo $R->render('pdf');
exit();